<?php
namespace Application\Controllers;


class Settings extends Controller {

	function index(\Base $f3) {
		//code here;

        $f3 = \Base::instance();
        $settings = new \Application\Models\Setting();
        $_settings = $f3->get('POST.setting');
        if(is_array($_settings))
        foreach($_settings as $s=>$value){
           $setting = $settings->load(array('name = ?',$s));
            $setting->name = $s;
            $setting->value = $value;
            $setting->save();
        }
        \Application\Models\Setting::reload($f3);
        $f3->set('list',$settings->list);
        $f3->set('INNER.PAGE','settings/list.html');
        $f3->set('page.title','Edit Settings');

	}

}