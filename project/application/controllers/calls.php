<?php
namespace Application\Controllers;

use Application\Models\Call;
use Application\Models\Contact;

class Calls extends Controller {
    const MODULE = \Modules::BASIC;
    function __construct(){
        $this->model = new Call();
        $this->singular = 'call';
        $this->prural = 'calls';
        $f3 = \Base::instance();
        $f3->set('contacts', Contact::select_data('fullnames'));
        if($f3->get('PARAMS.motherid'))
            $f3->set('contacts', Contact::select_data('fullnames', 'id', array('lead = ?', $f3->get('PARAMS.motherid'))));
    }
}