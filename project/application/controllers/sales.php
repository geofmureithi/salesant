<?php
namespace Application\Controllers;


use Application\Models\Contact;
use Application\Models\Organisation;
use Application\Models\PaymentPlan;
use Application\Models\Product;
use Application\Models\ProductType;
use Application\Models\Sale;

class Sales extends Controller {

    /*
     * Actions
     */
    const ACTION_VIEW = "{{@class}}::Index";
    const ACTION_ADD = "{{@class}}::Add";
    const ACTION_EDIT = "{{@class}}::Edit";
    const ACTION_DELETE = "{{@class}}::Delete";


    const MODULE = \Modules::BASIC;

    function __construct(){
        $this->model = new Sale();
        $this->singular = 'sale';
        $this->prural = 'sales';

    }
    function close(\Base $f3, $params){
        $sale = Sale::id($params['id'])->orDie();
        if($sale->paid < $sale->amount){
            \Flash::instance()->addMessage('You cannot Close a sale that is not completely paid', \Flash::DANGER);
            $f3->reroute('/sales/view/'.$params['id']);
        }
        $sale->active = 0;
        $organisation = Organisation::id(\Base::instance()->get('SESSION.organisation'));
        $saleclosed = $organisation->settings['saleclosed'];
        if($saleclosed)
            foreach($sale->products as $product){
                if(is_int($product))
                    $product = Product::id($product);
                $product->status = $saleclosed;
                $product->save();
            }
        $sale->save();
        $f3->reroute('/sales/view/'.$params['id']);
    }
    function reporting(\Base $f3){
        parent::reporting($f3);

        $productTypes =(new ProductType())->load(array('organisation = ?', $f3->get('SESSION.organisation')));
        $productType = $f3->get('GET.productType')?$f3->get('GET.productType'):$productTypes->id;
        $sales = new Sale();
        $sales
            ->filter('quotation',array('organisation = ? AND sale > 0', $f3->get('SESSION.organisation')));
        $sales->has('quotation.opportunity.producttype',array('id = ?', $productType));
        $f3->set('POST.productType',$productType );
        $f3->set('form.action', '/reporting/sales');
        $f3->set('sales', $sales->find(array('datecreated >= ? AND datecreated <= ? AND organisation = ?',$this->startDate, $this->endDate , $f3->get('SESSION.organisation'))));


    }
    function receipt(\Base $f3, $params){
        $f3->set('form.action', '/'.strtolower($this->prural).'/add');
        $f3->set('INNER.PAGE',strtolower($this->prural).'/receipt.html');
        $f3->set('page.title','Add '. \Customlang::process(ucfirst($this->singular)));
    }

    function plans(\Base $f3, $params){
        $organisation = Organisation::id($f3->get('SESSION.organisation'))->orDie();
        $f3->set('UI','files/');
        $f3->set('plan',PaymentPlan::id($params['id']));
        echo \Template::instance()->render($organisation->invoiceTemplate?$organisation->invoiceTemplate:$f3->error(500,"No Invoice Template: Please upload an invoice template"));
        die();
    }

}