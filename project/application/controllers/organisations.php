<?php
namespace Application\Controllers;

use Application\Models\Language;
use Application\Models\Organisation;
use Application\Models\User;

class Organisations extends Controller {
    /*
     * Actions
     */
    const ACTION_VIEW = "{{@class}}::Index";
    const ACTION_ADD = "{{@class}}::Add";
    const ACTION_EDIT = "{{@class}}::Edit";
    const ACTION_DELETE = "{{@class}}::Delete";

    const MODULE = \Modules::BASIC;

    function __construct(){
        $this->model = new Organisation();
        $this->singular = 'organisation';
        $this->prural = 'organisations';
        $f3= \Base::instance();
        $f3->set('contacts', User::select_data('fullnames'));
        //die(var_dump(Organisation::id(1)->leadstatuses[0]));
    }
    function changeTo(\Base $f3, $params){

        $this->view->data = array('success'=>false, "message"=> "Failed to Successfully switched Organisation");
        if (User::isInOrganisation($params['id'],$f3->get('SESSION.user_id'))){
            $this->view->data = array('success'=>true, "message"=> "Successfully switched Organisation");
            $f3->set('SESSION.organisation', (int) $params['id']);
        }

    }
    function index(\Base $f3) {
        $f3->set('list',$this->model->find());
        if($_GET['filter'] == 'inactive' )
            $f3->set('list',$this->model->find(array('active = ?', 0)));
        if($_GET['filter'] == 'active')
            $f3->set('list',$this->model->find(array('active = ?', 1)));
        $f3->set('INNER.PAGE',strtolower($this->prural).'/listing.html');
        $f3->set('page.title','Listing '.ucfirst($this->prural));
        if($_GET['filter'])
            $f3->set('page.title','Listing '.ucfirst($_GET['filter']). ' ' .ucfirst($this->prural));

    }


    function view(\Base $f3, $params) {
        $this->model->reset();
        $model = $this->model->load(array('id = ?', $params['id']));
        $f3->set('org',$model);
        $f3->set('INNER.PAGE',strtolower($this->prural).'/view.html');
        $f3->set('page.title','View '. ucfirst($this->singular) . '#'.$this->model->id);

    }

    function language(\Base $f3, $params){
        $this->model->reset();
        $model = $this->model->load(array('id = ?', $params['id']));
        $f3->set('org',$model);
        $f3->set('page.title','View '. ucfirst($this->singular) . '#'.$this->model->id);
        $f3->set('INNER.PAGE',strtolower($this->prural).'/language.html');
    }

    /* method:  post */
    function customLang(\Base $f3, $params){
        $organisation = $params['id'];
        $language = new Language();
        foreach ($f3->get('POST.lang') as $key=>$lang) {
            $language->reset();
            $language= $language->load(array('`key` = ? AND organisation = ?',$key, $organisation));
            $language->key = $key;
            $language->value = $lang;
            $language->organisation = $organisation;
            $language->save();

        }
        Language::reload(Organisation::id($organisation));
        \Flash::instance()->addMessage('Successfully edited Organisations Custom Language.');

        $f3->reroute('/organisations/language/'.$organisation);
    }

}