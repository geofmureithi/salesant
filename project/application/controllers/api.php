<?php
namespace Application\Controllers;


use Application\Models\User;

class Api extends Controller {
    const MODULE = \Modules::BASIC;
    const VERSION = 0.1;
    function beforeroute($f3,$params) {
        $apiKey = $f3->get('HEADERS.Apikey');
        $apiSecret = $f3->get('HEADERS.Apisecret');
        if(!static::authenticate($apiKey, $apiSecret))
            static::failed('Authentication Error');

    }
    function __construct(){
        $this->view = new \View\JSON();
        $this->view->data['version'] = static::VERSION;
    }
    function add(\Base $f3)
    {
        if(!$f3->exists('POST'))
            Api::failed('Please include Post Data for /add');

        if(!$f3->exists('POST.organisation'))
            Api::failed('Please include Organisation in the Post Data. Organisation Id is needed');

        try{
            $module = '\Application\Models\\'.($f3->get('GET.module'));
            $model = new $module;
            $model->copyfrom($f3->get('POST'), $model->fields());
            $model->save();
            $this->view->data['message'] = "Successfully added " .$f3->get('GET.Module');
            $this->view->data['id'] = $model->_id;
            $this->view->data['data'] = $model->cast(null, 0);
        } catch(\Exception $e){
            self::failed($e->getMessage());
        }

    }
    function get(\Base $f3){
        try{
            $module = '\Application\Models\\'.($f3->get('GET.module'));
            $model = new $module;
            $this->view->data['message'] = "Successfully Fetched " .$f3->get('GET.Module') ." Fields";
            $this->view->data['fields'] = $model->fields();
            if($f3->exists('GET.id'))
                $this->view->data['data'] = $module::id($f3->get('GET.id'))->cast(null,0);

        } catch(\Exception $e){
            self::failed($e->getMessage());
        }


    }
    function index(\Base $f3)
    {
        $this->view->data['message'] = 'Hello developer! Welcome to the SalesAnt APIs! Here is a list of Accessible Modules';
        $this->view->data['modules'] = array('Activity','Call','Contact','CustomField', 'CustomValue', 'Email','Invoice', 'Lead','Leadsource','Leadstatus','Note', 'Opportunity','OpportunityStatus','Organisation','PaymentPlan','Product','ProductType','Quotation','Receipt','Setting','Task','User','UserType','Document','Language', 'Sale', 'Payment', 'Transcript','NewsletterGroup', 'NewsletterTemplate');
    }

    private static function authenticate($apiKey, $apiSecret){
        $apiKey = \UrlEncryption::instance()->decode($apiKey);
        $apiSecret = \UrlEncryption::instance()->decode($apiSecret);
        $user = (new User);
        return $user->find(array('id = ? AND password = ?', $apiKey, $apiSecret));

    }
    private static function failed($message, $doc= null){
        $self = new static;
        $self->view->data['message'] = array('error'=>$message);
        echo $self->view->render();
        die;
    }
    public function afterroute($f3, $params)
    {
        echo $this->view->render();
        die;
    }
}