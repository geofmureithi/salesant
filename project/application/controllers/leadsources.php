<?php
namespace Application\Controllers;

use Application\Models\LeadSource;

class LeadSources extends Controller {
    /*
     * Actions
     */
    const ACTION_VIEW = "{{@class}}::Index";
    const ACTION_ADD = "{{@class}}::Add";
    const ACTION_EDIT = "{{@class}}::Edit";
    const ACTION_DELETE = "{{@class}}::Delete";

    const MODULE = \Modules::LEADS;
    function __construct(){
        $this->model = new LeadSource();
        $this->singular = 'leadSource';
        $this->prural = 'leadSources';
    }



}