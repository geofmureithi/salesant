<?php
namespace Application\Controllers;

use Application\Models\NewsletterTemplate;

class NewsletterTemplates extends Controller {

    /*
     * Actions
     */
    const ACTION_VIEW = "{{@class}}::Index";
    const ACTION_ADD = "{{@class}}::Add";
    const ACTION_EDIT = "{{@class}}::Edit";
    const ACTION_DELETE = "{{@class}}::Delete";

    const MODULE = \Modules::NEWSLETTERS;
    /**
     * NewsletterTemplates constructor.
     */
    function __construct(){
        $this->model = new NewsletterTemplate();
        $this->singular = 'newslettertemplate';
        $this->prural = 'newslettertemplates';
    }

}