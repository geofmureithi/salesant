<?php
namespace Application\Controllers;

use Application\Models\Contact;
use Application\Models\Opportunity;
use Application\Models\Organisation;
use Application\Models\PaymentPlan;
use Application\Models\Product;
use Application\Models\Quotation;
use Application\Models\QuotationsProducts;
use Application\Models\Sale;
use Ddeboer\Imap\Exception\Exception;
use PhpOffice\PhpWord\Autoloader;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\TemplateProcessor;

class Quotations extends Controller {

    /*
     * Actions
     */
    const ACTION_VIEW = "{{@class}}::Index";
    const ACTION_ADD = "{{@class}}::Add";
    const ACTION_EDIT = "{{@class}}::Edit";
    const ACTION_DELETE = "{{@class}}::Delete";


    const MODULE = \Modules::SALES;

    function __construct(){
        $this->model = new Quotation();
        $this->singular = 'quotation';
        $this->prural = 'quotations';
        $f3 = \Base::instance();
        $opportunity = Opportunity::id($f3->get('PARAMS.motherid'));
        $f3->set('opportunity', $opportunity);
        $f3->set('contacts', Contact::select_data('fullnames','id',array('lead = ?', $opportunity->lead->id)));
    }

    function add(\Base $f3)
    {
        if($f3->exists('POST.contact')) {
            $this->model->reset();
            $this->model->copyfrom('POST.'.$this->singular);
            $this->model->save();
            \Flash::instance()->addMessage('New '. ucfirst($this->singular). ' Successfully added to Database','success');
            $this->view->reroute('/'.$this->prural);
        }
        $f3->set('form.action', '/opportunity/'.$f3->get('PARAMS.motherid') .'/'.strtolower($this->prural).'/create');
        $opportunity= Opportunity::id($f3->get('PARAMS.motherid'))->orDie();

        $f3->set('POST.contact', Contact::id($opportunity->contact->id));
        $f3->set('POST.quotation.contact', $opportunity->contact->id);
        $f3->set('INNER.PAGE',strtolower($this->prural).'/add.html');
        $f3->set('page.title','Add '. ucfirst($this->singular));
    }
    function reporting(\Base $f3){
        parent::reporting($f3);

        $user = (new User);
        $user
            ->filter('organisations', array('id = ?', $f3->get('SESSION.organisation')));
        $user = $user->find();
        $productTypes =(new ProductType())->load(array('organisation = ?', $f3->get('SESSION.organisation')));
        $f3->set('POST.productType', $f3->get('GET.productType')?$f3->get('GET.productType'):$productTypes->id);
        $f3->set('form.action', '/reporting/opportunities');
        $f3->set('salespersons', $user);


    }
    function edit(\Base $f3, $params)
    {
        parent::edit($f3,$params);
        $f3->set('form.action', '/opportunity/'.$f3->get('PARAMS.motherid') .'/'.strtolower($this->prural).'/update/'.$f3->get('PARAMS.id'));
        $opportunity= Opportunity::id($f3->get('PARAMS.motherid'))->orDie();

        $f3->set('POST.contact', Contact::id($opportunity->contact->id));
        $f3->set('POST.quotation.contact', $opportunity->contact->id);
        $f3->set('quotation', $this->model->load(array('id = ?', $params['id'])));
        $f3->set('INNER.PAGE',strtolower($this->prural).'/edit.html');
    }
    function create(\Base $f3, $params){
        $opportunity= Opportunity::id($f3->get('PARAMS.motherid'))->orDie();
        if($f3->exists('POST.quotation.contact') && $f3->exists('POST.contact')) { // Lets first update Contact Details
            $contact = Contact::id($opportunity->contact->id);
            $contact->copyfrom('POST.contact');
            $contact->save();
        }
        $quotation = new Quotation();
        $quotation->reset();
        $quotation->opportunity = $opportunity;
        $products = $f3->get('POST.quotation.product');
        foreach($products as $product){
            $productsquotation = new QuotationsProducts();
            $productsquotation->products = $product['state'];
            $productsquotation->quotations = $quotation->id;
            $productsquotation->quantity = $product['qty'];
            $productsquotation->save();
        }
        $quotation->save();
        if($f3->exists('POST.paymentplan') ) {
            $plans = $f3->get('POST.paymentplan.amount');
            $days = $f3->get('POST.paymentplan.datedue');
            $descriptions = $f3->get('POST.paymentplan.description');
            $flipped = array();
            $total = 0;
            foreach($plans as $key=>$plan){
                $payment = new PaymentPlan();
                $payment->reset();
                if($plan && $days[$key]){
                    $flipped = array('amount'=>$plan, 'datedue'=>$days[$key], 'description' => $descriptions[$key]);
                    $payment->amount = $flipped['amount'];
                    $payment->datedue = $flipped['datedue'];
                    $payment->description = $flipped['description'];
                    $payment->quotation = $quotation;
                    $payment->save();
                    $total = $total + $payment->amount;
                }

            }
            $quotation->amount = $total;
            $quotation->save();


        }
        \Flash::instance()->addMessage('Quotation Created Successfully!');
        $f3->reroute('/opportunity/'.$opportunity->id. '/quotations/view/'.$quotation->id);
    }
    function quick(\Base $base, $params = array()){
        $base->set('quotation.id', (new Quotation())->load()->last()->id +1);
        $products =  (new Product)->find(array('organisation = ?', $base->get('SESSION.organisation')));
        if($products)
            $base->set('products', $products->castAll(0));
        $base->set('INNER.PAGE',strtolower($this->prural).'/quick.html');
        $base->set('page.title', ucfirst($this->singular) . ' Quick Create');
    }

    /**
     * @param \Base $f3
     * @param $params
     * @throws \Exception
     */
    function push(\Base $f3, $params){
        $this->view = new \View\JSON();
        $f3->set('POST', json_decode($f3->get('BODY'),true));
        $quotation = new Quotation;
        try{
            if((bool)$f3->get('POST.quotation.id'))
                $quotation = $quotation->id($f3->get('POST.quotation.id'))->orDie();
        } catch(\Exception $e){
            $this->view->data = array('error' => $e->getMessage());
            http_response_code(404);
            echo $this->view->render();
            die;

        }

        $quotation->setData($f3->get('POST'));


        $f3->set('POST.quotation.id', rand(0,10000));


        $this->view->data = $f3->get('POST') ;
    }
    function update(\Base $f3, $params){
        $opportunity= Opportunity::id($f3->get('PARAMS.motherid'))->orDie();
        if($f3->exists('POST.quotation.contact') && $f3->exists('POST.contact')) { // Lets first update Contact Details
            $contact = Contact::id($opportunity->contact->id);
            $contact->copyfrom('POST.contact');
            $contact->save();
        }
        $quotation = new Quotation();
        $quotation = $quotation->load(array('id = ?', $f3->get('PARAMS.id') ))->orDie();
        $quotation->opportunity = $opportunity;
        $products = $f3->get('POST.quotation.product');
        QuotationsProducts::clean($quotation->id);
        foreach($products as $product){
            $productsquotation = new QuotationsProducts();
            $productsquotation->products = $product['state'];
            $productsquotation->quotations = $quotation->id;
            $productsquotation->quantity = $product['qty'];
            $productsquotation->save();
        }
        $quotation->removePlans();
        $quotation->save();

        if($f3->exists('POST.paymentplan') ) {
            $plans = $f3->get('POST.paymentplan.amount');
            $days = $f3->get('POST.paymentplan.datedue');
            $descriptions = $f3->get('POST.paymentplan.description');
            $flipped = array();
            $total = 0;
            foreach($plans as $key=>$plan){
                $payment = new PaymentPlan();
                $payment->reset();
                if($plan && $days[$key]){
                    $flipped = array('amount'=>$plan, 'datedue'=>$days[$key], 'description' => $descriptions[$key]);
                    $payment->amount = $flipped['amount'];
                    $payment->datedue = $flipped['datedue'];
                    $payment->description = $flipped['description'];
                    $payment->quotation = $quotation;
                    $payment->save();
                    $total = $total + $payment->amount;
                }

            }
            $quotation->amount = $total;
            $quotation->save();


        }
        \Flash::instance()->addMessage('Quotation Updated Successfully!');
        $f3->reroute('/opportunity/'.$opportunity->id. '/quotations/view/'.$quotation->id);
    }
    function generatePDF(\Base $f3, $params){

    }
    function generateWORD(\Base $f3, $params){
        include(ROOT.'/includes/phpoffice/phpword/Autoloader.php');
        Autoloader::register();
        $file = ROOT.'/files/'.Organisation::id($f3->get('SESSION.organisation'))->quoteTemplate;
        if(!file_exists($file))
            throw new \ErrorException('No Word Template is set. Please upload a .docx template');

        $document = new TemplateProcessor($file);
        // Variables on different parts of document
        $quotation = Quotation::id($params['id'])->orDie();
        $values = $this->getWORDValues($quotation);
        foreach($values as $key=>$value){

            if(is_array($value)){
                $document->cloneRow($key, count($value));
                foreach($value as $subvalue){
                    foreach($subvalue as $k=>$v){
                        $document->setValue($k, $v);
                    }

                }
            } else {
                $document->setValue($key, $value); // On section/content
            }

        }

        $name = Organisation::id($f3->get('SESSION.organisation'))->name . ' - Letter of Offer - '. $quotation->opportunity->contact->fullnames.'- '.date('d-m-Y'). '.docx';
        $document->saveAs(ROOT. '/files/'. $name);
        \Web::instance()->send(ROOT. '/files/'. $name);
        die;

    }
    function approve(\Base $f3, $params){
        $this->model->reset();
        $model = $this->model->load(array('id = ?', $params['id']));
        if($f3->exists('POST.approveKey') && $f3->get('POST.approveKey') == $f3->get('SESSION.approveKey')){
            $sale = null;
            if(!$this->model->sale){
                $sale = Sale::addFrom($model);
            }
            $model = $this->model->load(array('id = ?', $params['id']));
            $sale = $this->model->sale;

            $f3->reroute('/sales/view/'.$sale->id);
        }
        $f3->set($this->singular,$model);
        $this->view->data[$this->singular] = $f3->get($this->singular);
        $f3->set('INNER.PAGE',strtolower($this->prural).'/approve.html');
        $f3->set('page.title','Approve '. \Customlang::process(ucfirst($this->singular)) . '#'.$this->model->id);
        $f3->set('POST.approveKey', md5($this->model->id));
        $f3->set('SESSION.approveKey', md5($this->model->id));

    }
    function reject(\Base $f3, $params){
        $this->model->reset();
        $model = $this->model->load(array('id = ?', $params['id']));
        $f3->set($this->singular,$model);
        $this->view->data[$this->singular] = $f3->get($this->singular);
        $f3->set('INNER.PAGE',strtolower($this->prural).'/reject.html');
        $f3->set('page.title','Reject'. \Customlang::process(ucfirst($this->singular)) . '#'.$this->model->id);
    }

    private function getWORDValues(Quotation $quotation){
        $values =
            array(
                'refno' => rand(100,1000),
                'date'=> date('l, F dS, Y'),
                'contactname' => $quotation->opportunity->contact->fullnames,
                'contactaddress' => $quotation->opportunity->contact->postalAddress,
                'contactemail' => $quotation->opportunity->contact->email,
                'contacttel' => $quotation->opportunity->contact->telephone,
                'producttype' => strtoupper($quotation->opportunity->producttype->title),
                'amountnumber' => number_format($quotation->amount,2),
                'amountwords' =>  ucwords(\Number::instance()->toWords($quotation->amount))
            );
            $payments = $quotation->paymentPlan;
            $y= 1;
            foreach($payments as $payment){
                $values['paymentnumber'][] = array(
                    "paymentnumber#{$y}"=> number_format($payment->amount,2),
                    "paymentwords#{$y}"=> ucwords(\Number::instance()->toWords($payment->amount,2)),
                    "datedue#{$y}"=>  (new \DateTime($payment->datedue))->format('F dS, Y'),
                    "description#{$y}"=> $payment->description,
                );
                $y++;
            }
            $products = $quotation->products;
            $x = 1;
            foreach($products as $product){
                foreach($product->custom as $key=>$value)
                    $values["product{$x}custom{$key}"] = $value;
                $x++;
                }
        return $values;
    }



}