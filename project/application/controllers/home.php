<?php
namespace Application\Controllers;
use Application\Models\Lead;
use Application\Models\LeadStatus;
use Application\Models\Opportunity;
use Application\Models\OpportunityStatus;
use Application\Models\Product;
use Application\Models\ProductStatus;
use Application\Models\User;

class Home extends Controller{
    const MODULE = \Modules::BASIC;

    function index(\Base $f3)
    {
        $user = $f3->get('USER');
        if(strpos(strtolower($user->type->title), 'admin')){
            $this->admin($f3);
        }
        else {
            $this->user($f3);
        }

    }
    private function admin(\Base $f3){
        $this->reporting($f3);
        $f3->set('INNER.PAGE', 'dashboard/admin.html');
        $f3->set('page.title', 'Admin Dashboard');
        $f3->set('module', 'Dashboard');
        $leadstatus = new LeadStatus();
        $leadstatus->filter('leads',array('datecreated >= ? AND datecreated <= ? AND organisation = ?',$this->startDate, $this->endDate , $f3->get('SESSION.organisation')));
        $f3->set('leadStatuses', $leadstatus->find(array('organisation = ?', $f3->get('SESSION.organisation'))));
        $productStatus = new ProductStatus();
        if($f3->get('GET.productType'))
            $productStatus->filter('product.type', array('id => ?', $f3->get('GET.productType')));
        $productStatus->filter('product',array('datecreated >= ? AND datecreated <= ? AND organisation = ?',$this->startDate, $this->endDate , $f3->get('SESSION.organisation')));
        $f3->set('productStatuses', $productStatus->find(array('organisation = ?', $f3->get('SESSION.organisation'))));
    }
    private function user(\Base $f3){
        $f3->set('INNER.PAGE', 'dashboard/index.html');
        $f3->set('page.title', 'Dashboard');
        $f3->set('module', 'Dashboard');
        $emptyleads= (new Lead());
        $emptyleads->countRel('opportunities');
        $f3->set('leads', $emptyleads->find(array('organisation = ?', $f3->get('SESSION.organisation')),array('limit'=> 5, 'order'=>'count_opportunities ASC')));
        $f3->set('opportunities', (new Opportunity())->find(array('organisation = ? AND confidence > 70', $f3->get('SESSION.organisation')),array('limit'=> 5)));

    }
}