<?php
namespace Application\Controllers;


use Application\Models\Transcript;

class Transcripts extends Controller {

    const MODULE = \Modules::SALES;


    function __construct(){
        $this->model = new Transcript();
        $this->singular = 'transcript';
        $this->prural = 'transcripts';

    }
}