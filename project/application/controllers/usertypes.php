<?php
namespace Application\Controllers;

use Application\Models\Permission;
use Application\Models\UserType;

class UserTypes extends Controller {
    /*
     * Actions
     */
    const ACTION_VIEW = "{{@class}}::Index";
    const ACTION_ADD = "{{@class}}::Add";
    const ACTION_EDIT = "{{@class}}::Edit";
    const ACTION_DELETE = "{{@class}}::Delete";

    const MODULE = \Modules::BASIC;

    function __construct(){
        $this->model = new Usertype();
        $this->singular = 'usertype';
        $this->prural = 'usertypes';
    }
    function index(\Base $f3)
    {
        $f3->set('list',$this->model->find());
        if($_GET['filter'] == 'inactive' )
            $f3->set('list',$this->model->find(array('active = ?', 0)));
        if($_GET['filter'] == 'active')
            $f3->set('list',$this->model->find(array('active = ?', 1)));
        $f3->set('INNER.PAGE',strtolower($this->prural).'/listing.html');
        $f3->set('page.title','Listing '.\Customlang::process(ucfirst($this->prural)));
        if($_GET['filter'])
            $f3->set('page.title','Listing '.ucfirst($_GET['filter']). ' ' .ucfirst($this->prural));

    }

    function permissions(\Base $f3 , $params){
        $usertype = UserType::id($params['id'])->orDie();
        if($f3->get('POST.permission')){
            $permissions = $f3->get('POST.permission');
            Permission::dissolve($usertype->id);// Get rid of past permissions
            foreach($permissions as $module=>$actions){
                foreach($actions as $action=>$value){
                    $permission = new Permission();
                    $permission->usertype = $usertype;
                    $permission->module = $module;
                    $permission->action = $action;
                    if($value)
                        $permission->save();
                }
            }
        }
        $f3->set('permissions', $this->perm());
        $f3->set('usertype', $usertype);
        $f3->set('page.title','Permissions for '. $usertype->title);
        $f3->set('form.action','/usertypes/permissions/' . $usertype->id);
        $f3->set('INNER.PAGE',strtolower($this->prural).'/permissions.html');
    }

    /**
     * @return array
     */
    final private function perm(){
        $classes = array();
        if ($handle = opendir('./project/application/controllers/')) {

            while (false !== ($entry = readdir($handle))) {

                if ($entry != "." && $entry != "..") {
                    $entry = str_replace('.php', '', $entry);
                    $refl = new \ReflectionClass('\Application\Controllers\\'. ucwords($entry));
                    //$classes[$entry] = $refl->getConstants();
                    $constants = $refl->getConstants();
                    if($constants)
                        foreach($constants as $key=>$constant)
                            //echo $key;
                            if(strpos($key,'ACTION')=== 0){
                                $classes[$entry][$key] = \Template::instance()->resolve($constant, array('class' => ucwords($refl->getName())));
                            }


                }
            }

            closedir($handle);
        }
        return $classes;
    }


}