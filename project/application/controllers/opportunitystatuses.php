<?php
namespace Application\Controllers;

use Application\Models\OpportunityStatus;

class OpportunityStatuses extends Controller {

    /*
     * Actions
     */
    const ACTION_VIEW = "{{@class}}::Index";
    const ACTION_ADD = "{{@class}}::Add";
    const ACTION_EDIT = "{{@class}}::Edit";
    const ACTION_DELETE = "{{@class}}::Delete";

    const MODULE = \Modules::OPPORTUNITIES;

    function __construct(){
        $this->model = new OpportunityStatus();
        $this->singular = 'opportunityStatus';
        $this->prural = 'opportunityStatuses';
    }



}