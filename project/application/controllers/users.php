<?php
namespace Application\Controllers;

use Application\Models\Organisation;
use Application\Models\User;
class Users extends Controller {
    /*
     * Actions
     */
    const ACTION_VIEW = "{{@class}}::Index";
    const ACTION_ADD = "{{@class}}::Add";
    const ACTION_EDIT = "{{@class}}::Edit";
    const ACTION_DELETE = "{{@class}}::Delete";

    const MODULE = \Modules::BASIC;

    function __construct(){
        $this->model = new User();
        $this->singular = 'user';
        $this->prural = 'users';
        $f3 = \Base::instance();
        $f3->set('organisations', (new Organisation)->find());
    }
    function index(\Base $f3) {
        $f3->set('list',$this->model->find());
        if($_GET['filter'] == 'inactive' )
            $f3->set('list',$this->model->find(array('active = ?', 0)));
        if($_GET['filter'] == 'active')
            $f3->set('list',$this->model->find(array('active = ?', 1)));
        $f3->set('INNER.PAGE',strtolower($this->prural).'/listing.html');
        $f3->set('page.title','Listing '.ucfirst($this->prural));
        if($_GET['filter'])
            $f3->set('page.title','Listing '.ucfirst($_GET['filter']). ' ' .ucfirst($this->prural));

    }


}