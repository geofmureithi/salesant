<?php
namespace Application\Controllers;
use Application\Models\Product;
use Application\Models\ProductStatus;
use Application\Models\ProductType;

class Products extends Controller {

    /*
     * Actions
     */
    const ACTION_VIEW = "{{@class}}::Index";
    const ACTION_ADD = "{{@class}}::Add";
    const ACTION_EDIT = "{{@class}}::Edit";
    const ACTION_DELETE = "{{@class}}::Delete";

    const MODULE = \Modules::PRODUCTS;


    function __construct(){
        $this->model = new Product();
        $this->singular = 'product';
        $this->prural = 'products';
        $f3 = \Base::instance();
        $productTypes = ProductType::select_data('title','id',array('organisation = ?', $f3->get('SESSION.organisation')));
        $f3->set('types', $productTypes);// All Active
        $f3->set('product', $this->model);
        $f3->set('productStatuses', (new ProductStatus)->find(array('organisation = ?', $f3->get('SESSION.organisation'))));

    }

    function add(\Base $f3) {

        if($f3->exists('POST.'.$this->singular)) {
            $this->model->reset();
            $this->model->copyfrom('POST.'.$this->singular);
            $this->model->save();
            $this->model->set_custom($f3->get('POST.custom'));
            \Flash::instance()->addMessage('New '. \Customlang::process(ucfirst($this->singular)). ' Successfully added to Database','success');
            $this->view->reroute('/'.$this->prural);
        }
        $f3->set('form.action', '/'.strtolower($this->prural).'/add');
        $f3->set('INNER.PAGE',strtolower($this->prural).'/add.html');
        $f3->set('page.title','Add '. \Customlang::process(ucfirst($this->singular)));

    }
    function edit(\Base $f3, $params) {

        $this->model->reset();
        $this->model->load(array('id = ?', $params['id']));
        if($f3->exists('POST.'.$this->singular)) {
            $this->model->copyfrom('POST.'.$this->singular);
            $this->model->save();
            $this->model->set_custom($f3->get('POST.custom'));
            \Flash::instance()->addMessage(\Customlang::process(ucfirst($this->singular)). ' #'.$this->model->id .' Successfully Edited','success');
            $this->view->reroute('/'.$this->prural);
        }
        $f3->set('form.action', '/'.strtolower($this->prural).'/edit/'. $params['id']);
        $this->model->copyto('POST.'.$this->singular);
        $f3->set('INNER.PAGE',strtolower($this->prural).'/add.html');
        $f3->set('page.title','Edit '. \Customlang::process(ucfirst($this->singular)) . '#'.$this->model->id);

    }
    function index(\Base $f3) {
        $f3->set('list',$this->model->find(array('organisation = ?', $f3->get('SESSION.organisation'))));
        if($_GET['filter'] == 'inactive' )
            $f3->set('list',$this->model->find(array('active = ?', 0)));
        if($_GET['filter'] == 'active')
            $f3->set('list',$this->model->find(array('active = ?', 1)));
        $f3->set('INNER.PAGE',strtolower($this->prural).'/listing.html');
        $f3->set('page.title', \Customlang::process(ucfirst($this->prural)) . " Catalogue");
        if($_GET['filter'])
            $f3->set('page.title' , ucfirst($_GET['filter']). ' ' .ucfirst($this->prural). " Catalogue");

    }
    function filter(\Base $f3){
        $products = $this->model;
        $filter = $f3->get('POST.filter');
        $custom = $f3->get('POST.custom');
        $custom_filters = array();
        if($filter){

            if($filter['status'])
                $products->has('status', array('id IN ?', $filter['status']));
            if((int) $filter['type'])
                $products->has('type', array('id = ?', $filter['type']));
            if($custom)
            {
                $query = array();
                foreach($custom as $key=>$custom){
                    if(!empty($custom))
                        $query[]= array("field = ? AND value LIKE ?", $key, '%'.$custom.'%');

                }
                if(!empty($query))
                $products->has('customdata', $products->mergeFilter($query));
            }
            if($filter['isService'])
                $custom_filters[]=array('isService = ?', 1);
            if($filter['isResale'])
                $custom_filters[]=array('isResale = ?', 1);

            if($filter['priceStart']>0)
                $custom_filters[]=array('price > ?', floatval($filter['priceStart']));
            if($filter['priceEnd']> 0)
                $custom_filters[]=array('price < ?', $filter['priceEnd']);




        }
        $custom_filters[]= array('organisation = ?', $f3->get('SESSION.organisation'));
        //die(var_dump($custom_filters));

        $f3->set('list',$products->find($products->mergeFilter($custom_filters)));
        $f3->set('INNER.PAGE',strtolower($this->prural).'/listing.html');
        $f3->set('page.title', \Customlang::process(ucfirst($this->prural)) . " Catalogue");
    }




}