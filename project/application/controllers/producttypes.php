<?php
namespace Application\Controllers;



use Application\Models\CustomField;
use Application\Models\Product;
use Application\Models\ProductType;

class ProductTypes extends Controller {

    /*
     * Actions
     */
    const ACTION_VIEW = "{{@class}}::Index";
    const ACTION_ADD = "{{@class}}::Add";
    const ACTION_EDIT = "{{@class}}::Edit";
    const ACTION_DELETE = "{{@class}}::Delete";

    const MODULE = \Modules::PRODUCTS;

    function __construct(){
        $this->model = new ProductType();
        $this->singular = 'productType';
        $this->prural = 'productTypes';
    }
    function customFields(\Base $f3, $params){
        $product = (int) $f3->get('GET.product');
        $this->view->data['fields'] = array('success'=> true, 'template'=>
            $this->buildFields($params['id'],$product?$product:null));
    }
    function buildFields($productType, $product=null){
        $f3 = \Base::instance();
        $customFields = new CustomField();
        $f3->set('prod',$product);
        $f3->set('colSize',$f3->get('GET.full') == 'true'?'col-md-12':'col-md-4');
        $f3->set('handler',$f3->get('GET.handler')?$f3->get('GET.handler'):'product');
        $f3->set('fields',$customFields->find(array('producttype = ?', $productType)));
        $tpl = \Template::instance();
        return $tpl->render('customfields/field.html');
    }



}