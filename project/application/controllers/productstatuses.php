<?php
namespace Application\Controllers;

use Application\Models\ProductStatus;

class ProductStatuses extends Controller {
    /*
     * Actions
     */
    const ACTION_VIEW = "{{@class}}::Index";
    const ACTION_ADD = "{{@class}}::Add";
    const ACTION_EDIT = "{{@class}}::Edit";
    const ACTION_DELETE = "{{@class}}::Delete";

    const MODULE = \Modules::BASIC;

    function __construct(){
        $this->model = new ProductStatus();
        $this->singular = 'productStatus';
        $this->prural = 'productStatuses';
    }



}