<?php
namespace Application\Controllers;


use Application\Models\Contact;
use Application\Models\Opportunity;
use Application\Models\OpportunityStatus;
use Application\Models\Organisation;
use Application\Models\Product;
use Application\Models\ProductType;
use Application\Models\User;

class Opportunities extends Controller {

    const MODULE = \Modules::OPPORTUNITIES;

    function __construct(){
        $this->model = new Opportunity();
        $this->singular = 'opportunity';
        $this->prural = 'opportunities';
        $f3= \Base::instance();
        if($f3->get('PARAMS.motherid'))
            $f3->set('contacts', Contact::select_data('fullnames', 'id', array('lead = ?', $f3->get('PARAMS.motherid'))));
        if($f3->get('PARAMS.id'))
            $f3->set('contacts', Contact::select_data('fullnames', 'id', array('lead = ?', Opportunity::id($f3->get('PARAMS.id'))->lead->id)));
        $f3->set('assignees', Organisation::selectableUsers());
        $f3->set('statuses', OpportunityStatus::select_data('title','id', array('organisation = ?', $f3->get('SESSION.organisation'))));
        $f3->set('producttypes', ProductType::select_data('title', 'id', array('organisation = ?', $f3->get('SESSION.organisation'))));
        $f3->set('products', (new Product())->find(array('organisation = ?', $f3->get('SESSION.organisation'))));
    }
    function add(\Base $f3)
    {
        parent::add($f3);
        //Inject Current user as Assignee
        $f3->set('POST.opportunity.assignee',$f3->get('SESSION.user_id'));
    }
    function edit(\Base $f3, $params)
    {
        $this->model->reset();
        $this->model->load(array('id = ?', $params['id']))->orDie();
        if($f3->exists('POST.'.$this->singular)) {
            $this->model->products = array();
            $this->model->copyfrom('POST.'.$this->singular);
            $this->model->save();
            \Flash::instance()->addMessage(\Customlang::process(ucfirst($this->singular)). ' #'.$this->model->id .' Successfully Edited','success');
            $this->view->reroute('/'.$this->prural);
        }
        $f3->set('form.action', '/'.strtolower($this->prural).'/edit/'. $params['id']);
        $this->model->copyto('POST.'.$this->singular);
        $f3->set($this->singular,$this->model);
        $f3->set('INNER.PAGE',strtolower($this->prural).'/add.html');
        $f3->set('page.title','Edit '. \Customlang::process(ucfirst($this->singular)). '#'.$this->model->id);
    }

    function reporting(\Base $f3){
        parent::reporting($f3);

        $user = (new User);
        $user
            ->has('organisations', array('id = ?', $f3->get('SESSION.organisation')));
        $user = $user->find();
        $productTypes =(new ProductType())->load(array('organisation = ?', $f3->get('SESSION.organisation')));
        $f3->set('POST.productType', $f3->get('GET.productType')?$f3->get('GET.productType'):$productTypes->id);
        $f3->set('form.action', '/reporting/opportunities');
        $f3->set('salespersons', $user);


    }
}