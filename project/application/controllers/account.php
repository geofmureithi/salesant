<?php
namespace Application\Controllers;

use Application\Models\User;

class Account extends Controller {


    const MODULE = \Modules::BASIC;

    function __construct(){
        $this->singular = 'account';
        $this->prural = 'account';
    }

    function profile(\Base $base){
        $user = User::id($base->get('SESSION.user_id'))->orDie();
        $base->set('page.title', 'Profile Settings');

        if($base->exists('GET.change') && $base->exists('POST') && $base->get('POST') != null ){
            switch($base->get('GET.change')){
                case "profile":
                    $user->firstname = $base->get('POST.user.firstname');
                    $user->lastname = $base->get('POST.user.lastname');
                    $user->telephone = $base->get('POST.user.telephone');
                    $user->save();
                    \Flash::instance()->addMessage('Profile Successfully Updated');
                    break;
                case "email":
                    $hash_engine = $base->get('password_hash_engine');
                    if ($hash_engine == 'bcrypt') {
                        $valid = \Bcrypt::instance()->verify($base->get('POST.password'), $user->password);
                    } elseif ($hash_engine == 'md5') {
                        $valid = (md5($base->get('POST.password') . $base->get('password_md5_salt')) == $user->password);
                    }
                    !$valid? \Flash::instance()->addMessage('Email not Updated','warning'):'';
                    if($valid){
                        $user->email = $base->get('POST.user.email');
                        $user->save();
                        \Flash::instance()->addMessage('Email Successfully Updated');
                    }
                    break;
                case "smtp":
                    $settings = $base->get('POST.smtp');
                    $user->smtp = $settings;
                    $user->save();
                    \Flash::instance()->addMessage('SMTP Details Successfully Updated');
                    break;
                case "password":
                    $passwords = $base->get('POST');
                    if($passwords['new'] <> $passwords['new_confirm'])
                        \Flash::instance()->addMessage('Password Not Updated since the new ones don\'t match', 'danger' );
                    $hash_engine = $base->get('password_hash_engine');
                    if ($hash_engine == 'bcrypt') {
                        $valid = \Bcrypt::instance()->verify($passwords['old'], $user->password);
                    } elseif ($hash_engine == 'md5') {
                        $valid = (md5($passwords['old'] . $base->get('password_md5_salt')) == $user->password);
                    }
                    !$valid? \Flash::instance()->addMessage('Password not Updated, Old Password is Wrong','warning'):'';
                    if($valid && $passwords['new'] == $passwords['new_confirm']){
                        $base->set('POST.user.password_repeat', $passwords['new_confirm']);
                        $user->password = $passwords['new'];
                        $user->save();
                        \Flash::instance()->addMessage('Password was Updated',\Flash::SUCCESS);
                    }

                    break;
                case "imap":
                    $settings = $base->get('POST.imap');
                    $user->imap = $settings;
                    $user->save();
                    \Flash::instance()->addMessage('IMAP Details Successfully Updated');
                    break;
            }
        }
        $base->set('user', $user);
        $base->set('POST.user', $user );
        $base->set('POST.imap', $user->imap );
        $base->set('POST.smtp', $user->smtp );

        $base->set('INNER.PAGE',strtolower($this->prural).'/profile.html');;
    }

    function notifications(\Base $base){

    }
}