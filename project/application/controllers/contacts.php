<?php
namespace Application\Controllers;


use Application\Models\Contact;

class Contacts extends Controller {
    /*
     * Actions
     */
    const ACTION_VIEW = "{{@class}}::Index";
    const ACTION_ADD = "{{@class}}::Add";
    const ACTION_EDIT = "{{@class}}::Edit";
    const ACTION_DELETE = "{{@class}}::Delete";

    const MODULE = \Modules::LEADS;

    function __construct(){
        $this->model = new Contact();
        $this->singular = 'contact';
        $this->prural = 'contacts';
    }

    function fetch(\Base $f3, array $params){
        $contacts = new Contact();
        $contacts->filter('lead', array('id = ?', $params['id']));
        $contacts = $contacts->find(array('organisation = ?', $f3->get('SESSION.organisation')));
        $this->view->data = $contacts?$contacts->castAll(0):array();

    }


}