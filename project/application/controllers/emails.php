<?php
namespace Application\Controllers;


use Ddeboer\Imap\Search\Date\After;
use Ddeboer\Imap\Search\Email\FromAddress;
use Ddeboer\Imap\Search\State\NewMessage;
use Ddeboer\Imap\SearchExpression;
use Application\Models\Activity;
use Application\Models\Email;
use Application\Models\Lead;
use Application\Models\User;
use Application\Threads\FetchEmails;

class Emails extends Controller {

    const MODULE = \Modules::EMAILS;

    function __construct(){
        $this->model = new Email();
        $this->singular = 'email';
        $this->prural = 'emails';
    }

    /**
     * @param \Base $base
     * @param array $params
     */
    function attachment(\Base $base, array $params){
        \Web::instance()->send(
            'files/'.\UrlEncryption::instance()->decode($params['id'])
        );
    }

    /**
     * @param \Base $f3
     */
    function add(\Base $f3)
    {
        if($f3->exists('POST.'.$this->singular)) {
            $this->model->reset();
            $this->model->copyfrom('POST.'.$this->singular);
            $this->model->save();
            \Flash::instance()->addMessage('New '. \Customlang::process(ucfirst($this->singular)). ' Successfully added to Database','success');
            $this->view->reroute('/leads/view/'. $f3->get('PARAMS.motherid'));
        }
        $f3->set('form.action', '/'.strtolower($this->prural).'/add');
        $f3->set('INNER.PAGE',strtolower($this->prural).'/add.html');
        $f3->set('page.title','Add '. \Customlang::process(ucfirst($this->singular)));
    }

    /**
     * @param \Base $f3
     * @param $params
     * @throws \Exception
     */
    function sync(\Base $f3, $params){
        $user = User::id($f3->get('SESSION.user_id'))->orDie();
        if($user->imap == null)
            throw new \Exception('IMAP settings are not set. Please do so to allow SYNC');

        $imap = $user->imap;
        $fetcher = new FetchEmails($imap['server'], $imap['port'], $imap['username'], $imap['password']);
        $messages = null;
        $lead = Lead::id($params['id'])->orDie();
        if($fetcher->connection){

            $contacts = $lead->contacts;
            $search = new SearchExpression();
            foreach($contacts as $contact){
                if($contact->email)
                    $search = $search->addCondition(new FromAddress($contact->email));
            }
            # To prevent So much back tracking start date while empty == 2016-01-01
            $date = $lead->sync?$lead->sync:'2016-03-03';
            $search->addCondition(new After(new \DateTime($date)));
            //$search->addCondition(new NewMessage());
            if($contacts[0]->email){ // If we have any emails
                $messages = $fetcher->fetch($search);
            }
        }
        if($messages){
            foreach($messages as $message){
                $email= new Email();
                if($email->load(array('uniqueKey = ?', $message->getId()))->dry()){
                    $message = $message->keepUnseen();
                    $email->uniqueKey = $message->getId();
                    $email->subject = $message->getSubject();
                    $email->datecreated = $message->getDate()->format('Y-m-d H:i:s');
                    $email->from = $message->getFrom();
                    $email->lead = $lead;
                    $email->to = implode(',',$message->getTo());
                    $email->cc = implode(',',$message->getCc());
                    $email->description = $message->getBodyText();
                    $attachments = $message->getAttachments();
                    $att= array();
                    foreach ($attachments as $attachment) {
                        $f3->write('files/'. $attachment->getFilename(), $attachment->getDecodedContent());
                        $att[] = $attachment->getFilename();
                    }
                    $email->attachments = $att;
                    $email->save();
                }

            }
        }
        $lead->sync = (new \DateTime())->format('Y-m-d');
        $lead->save();


    }

}