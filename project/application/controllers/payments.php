<?php
namespace Application\Controllers;


use Application\Models\Payment;

class Payments extends Controller {

    const MODULE = \Modules::SALES;

    function __construct(){
        $this->model = new Payment();
        $this->singular = 'payment';
        $this->prural = 'payments';
        \Template\FooForms::init();

    }
}