<?php
namespace Application\Controllers;


use Application\Models\LeadStatus;
use Application\Models\ProductType;

class Controller extends \BaseController {

    protected $limit = 10;
    function beforeroute($f3,$params) {
        //code here;

        if(!\Application\Controllers\Auth::isLoggedIn())
            $f3->reroute('/auth/login');
        $f3 = \Base::instance();
        $f3->set('UI','project/application/views/');
        $f3->set('module',\Customlang::process(ucfirst($this->prural)));

        $this->page = \Pagination::findCurrentPage();
        $this->option = array('order' => 'datecreated DESC');
        $this->filter = null;
        $f3->set('USER',\Application\Models\User::id($f3->get('SESSION.user_id')));
        $f3->set('ORGANISATION',\Application\Models\Organisation::id($f3->get('SESSION.organisation')));
        $leadstatuses = new LeadStatus();
        $leadstatuses->countRel('leads');
        $f3->set('leadstatuses', $leadstatuses->find(array('organisation = ?', $f3->get('SESSION.organisation')),array('limit'=> 3, 'order'=>'count_leads DESC')));

        parent::beforeroute($f3,$params);

    }
    function index(\Base $f3) {
        $f3->set('list',$this->model->find(array('organisation = ?', $f3->get('SESSION.organisation'))));
        if($_GET['filter'] == 'inactive' )
            $f3->set('list',$this->model->find(array('active = ?', 0)));
        if($_GET['filter'] == 'active')
            $f3->set('list',$this->model->find(array('active = ?', 1)));
        $f3->set('INNER.PAGE',strtolower($this->prural).'/listing.html');
        $f3->set('page.title','Listing '.\Customlang::process(ucfirst($this->prural)));
        if($_GET['filter'])
            $f3->set('page.title','Listing '.ucfirst($_GET['filter']). ' ' .ucfirst($this->prural));

    }
    function add(\Base $f3) {

        if($f3->exists('POST.'.$this->singular)) {
            $this->model->reset();
            $this->model->copyfrom('POST.'.$this->singular);
            $this->model->save();
            \Flash::instance()->addMessage('New '. \Customlang::process(ucfirst($this->singular)). ' Successfully added to Database','success');
            $this->view->reroute('/'.$this->prural);
        }
        $f3->set('form.action', '/'.strtolower($this->prural).'/add');
        $f3->set('INNER.PAGE',strtolower($this->prural).'/add.html');
        $f3->set('page.title','Add '. \Customlang::process(ucfirst($this->singular)));

    }
    function edit(\Base $f3, $params) {

        $this->model->reset();
        $this->model->load(array('id = ?', $params['id']))->orDie();
        if($f3->exists('POST.'.$this->singular)) {
            $this->model->copyfrom('POST.'.$this->singular);
            $this->model->save();
            \Flash::instance()->addMessage(\Customlang::process(ucfirst($this->singular)). ' #'.$this->model->id .' Successfully Edited','success');
            $this->view->reroute('/'.$this->prural);
        }
        $f3->set('form.action', '/'.strtolower($this->prural).'/edit/'. $params['id']);
        $this->model->copyto('POST.'.$this->singular);
        $f3->set($this->singular,$this->model);
        $f3->set('INNER.PAGE',strtolower($this->prural).'/add.html');
        $f3->set('page.title','Edit '. \Customlang::process(ucfirst($this->singular)). '#'.$this->model->id);

    }
    function view(\Base $f3, $params) {
        $this->model->reset();
        $model = $this->model->load(array('id = ?', $params['id']));
        $f3->set($this->singular,$model);
        $this->view->data[$this->singular] = $f3->get($this->singular);
        $f3->set('INNER.PAGE',strtolower($this->prural).'/view.html');
        $f3->set('page.title','View '. \Customlang::process(ucfirst($this->singular)) . '#'.$this->model->id);

    }
    function upload(\Base $f3,$params){
        $type = $f3->get('POST.fileType');
        switch($type){
            case "image":
                $f3->set('UPLOADS', 'files/'.strtolower($this->prural).'/');
                break;
        }
        $overwrite = false; // set to true, to overwrite an existing file; Default: false
        $slug = true; // rename file to filesystem-friendly version
        $files = \Web::instance()->receive(function($file,$formFieldName){

                return true;
            },
            $overwrite,
            function($fileBaseName, $formFieldName){
                // build new file name from base name or input field name
                return time(). '_' .$fileBaseName;
            }
        );
        foreach($files as $key=>$val){
            if($val===true)
                $this->view->data =  array('success'=>true, 'fileName'=>str_replace('files/','',$key));
        }

    }
    function reporting(\Base $f3){
        $this->startDate = $f3->get('GET.startDate')?(new \DateTime($f3->get('GET.startDate')))->format('Y-m-d'):(new \DateTime('first day of this month'))->format('Y-m-d');
        $this->endDate = $f3->get('GET.endDate')?(new \DateTime($f3->get('GET.endDate')))->format('Y-m-d'):(new \DateTime('first day of this month'))->format('Y-m-t');
        $f3->set('POST.startDate',$this->startDate);
        $f3->set('POST.endDate',$this->endDate);
        $f3->set('INNER.PAGE',strtolower($this->prural).'/report.html');
        $f3->set('producttypes',ProductType::select_data('title', 'id',array('organisation = ?', $f3->get('SESSION.organisation'))));
        $f3->set('page.title','View '. \Customlang::process(ucfirst($this->singular)) . ' Reports');
        $f3->set('module',\Customlang::process(ucfirst($this->prural)).' Reporting');

    }
    function delete(\Base $f3, $params){

        $this->model->reset();
        $this->model
            ->load(array('id = ?',$params['id']));

        try{
            if(!$this->model->erase()){
                $f3->error(500, "Record Cannot be deleted ");
            }
            \Flash::instance()->addMessage(\Customlang::process(ucfirst($this->singular)). " #".$params['id']." Successfully Deleted",\Flash::DANGER);
        }
        catch (\Exception $e){
            \Flash::instance()->addMessage($e->getMessage(),'error');
        }
        if(!$f3->get('AJAX'))
            $f3->reroute('/' . $f3->get('PARAMS.app'). '/' . strtolower($this->prural));
        $this->view->data['message'] = "Delete was Successful";
    }
    final static function fileDetails($filename){
        $myFS = new \FAL\LocalFS('files/');
        $details = new \stdClass();
        if($myFS->exists($filename)){
            $details->name= basename($filename);
            $details->path = '/files/'.$filename;
            $details->size = $myFS->size($filename);
        }
        return $details;
    }


    function afterroute($f3,$params) {
        //code here;
        $f3->set('fileDetails', function($file){
            return static::fileDetails($file);
        });
        if($this->prural)
            $f3->set('crumb', $this->prural);

        parent::afterroute($f3,$params);

    }

}