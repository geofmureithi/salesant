<?php
namespace Application\Controllers;


use Application\Models\Contact;
use Application\Models\LeadSource;
use Application\Models\LeadStatus;
use Application\Models\NewsletterGroup;
use Application\Models\NewsletterTemplate;
use Application\Models\OpportunityStatus;
use Application\Models\ProductType;
use PHPExcel;

class Newsletters extends Controller {
    /*
     * Actions
     */
    const ACTION_VIEW = "{{@class}}::Index";
    const ACTION_ADD = "{{@class}}::Add";
    const ACTION_SEND = "{{@class}}::Send";
    const ACTION_EDIT = "{{@class}}::Edit";
    const ACTION_DELETE = "{{@class}}::Delete";

    const MODULE = \Modules::NEWSLETTERS;

    function __construct(){
        $this->model = new NewsletterGroup();
        $this->singular = 'newsletter';
        $this->prural = 'newsletters';
        $f3 = \Base::instance();
        $f3->set('leads.statuses',LeadStatus::select_data('title','id', array('organisation = ?', $f3->get('SESSION.organisation')),null,true));
        $f3->set('opportunity.statuses',LeadStatus::select_data('title','id', array('organisation = ?', $f3->get('SESSION.organisation')),null,true));
        $f3->set('producttypes',ProductType::select_data('title','id', array('organisation = ?', $f3->get('SESSION.organisation')),null,true));
        $f3->set('leads.sources',LeadSource::select_data('title','id', array('organisation = ?', $f3->get('SESSION.organisation')),null,true));

    }

    /**
     * @param \Base $f3
     */
    function add(\Base $f3)
    {
        if($f3->exists('POST.'.$this->singular)) {
            $type = $f3->get('POST.'.$this->singular.'.type');
            $algorithm = $f3->get('POST.algorithm');
            $althm = $this->build($type,$algorithm);
            if($althm)
            $f3->set('POST.'.$this->singular.'.algorithm',$althm);
            $f3->set('POST.'.$this->singular.'.type', null);
            $this->model->reset();
            $this->model->copyfrom('POST.'.$this->singular);
            if($althm)
                if($this->model->save())
                    \Flash::instance()->addMessage('New Newsletter Group Successfully added to Database',\Flash::SUCCESS);

            $this->view->reroute('/'.$this->prural);
        }
        $f3->set('form.action', '/'.strtolower($this->prural).'/add');
        $f3->set('INNER.PAGE',strtolower($this->prural).'/add.html');
        $f3->set('page.title','Add '. \Customlang::process(ucfirst($this->singular)). ' Group');
    }

    /**
     * @param \Base $f3
     * @param $params
     */
    function send(\Base $f3, $params){
        $f3->set('groups',NewsletterGroup::select_data('title','id', array('organisation = ?', $f3->get('SESSION.organisation')),null));
        $f3->set('templates',NewsletterTemplate::select_data('title','id', array('organisation = ?', $f3->get('SESSION.organisation')),null));
        $templates = (new NewsletterTemplate)->find(array('organisation = ?', $f3->get('SESSION.organisation')));
        $f3->set('templatetext', $templates[0]);
        $f3->set('INNER.PAGE',strtolower($this->prural).'/send.html');
        $f3->set('page.title','Send Newsletter');
    }

    /**
     * @param $type
     * @param $algorithms
     * @return array
     */
    private function build($type, $algorithms){
        var_dump($type);
        if($type == 'algorithm'){
            if(!is_array($algorithms))
                return array();
            foreach($algorithms as $key=>$algorithm)
                if(!$algorithm)
                    unset($algorithms[$key]);
            return $algorithms;
        }
        if($type == 'file'){
            if(!\Base::instance()->get('POST.newsletter.attachment'))
                return array();
            $file = \Base::instance()->get('POST.newsletter.attachment');
            $randomNewsletterId = microtime();
            $phpExcel = new PHPExcel();
            $inputFileType = 'CSV';
            $inputFileName = 'files/'. $file;
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
            $worksheet = $objPHPExcel->getActiveSheet();
            $contacts= $worksheet->toArray();
            $failedcontacts = 0;
            foreach($contacts as $contact){
                if(\Audit::instance()->email($contact[1],false)){
                    $c = new Contact();
                    $c->name = $contact[0];
                    $c->email = $contact[1];
                    $c->telephone = $contact[2];
                    $c->newsletterGroup = $randomNewsletterId;
                    $c->save();
                } else{
                    $failedcontacts++;
                    (new \Log('logs/import.log'))->write("{{$contact[0]} | {$contact[1]} | {$contact[2]} : Is not a valid contact");
                }
            }
            \Flash::instance()->addMessage('Import was Accomplished.');
            if($failedcontacts)
                \Flash::instance()->addMessage("{$failedcontacts} Contact(s) were not imported", \Flash::DANGER);

            //$phpExcel->garbageCollect();

            return array('newsletterGroup' => $randomNewsletterId);

        }
        return array();
    }

}