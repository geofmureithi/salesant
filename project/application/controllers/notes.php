<?php
namespace Application\Controllers;


use Application\Models\Note;
use Application\Models\Contact;

class Notes extends Controller {

    const MODULE = \Modules::LEADS;

    function __construct(){
        $this->model = new Note();
        $this->singular = 'note';
        $this->prural = 'notes';

    }
}