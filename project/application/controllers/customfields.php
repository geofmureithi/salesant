<?php
namespace Application\Controllers;

use Application\Models\CustomField;
use Application\Models\CustomValue;
use Application\Models\ProductType;

class CustomFields extends Controller {

    const MODULE = \Modules::PRODUCTS;
    function __construct(){
        $this->model = new CustomField();
        $this->singular = 'customfield';
        $this->prural = 'customFields';
        $f3= \Base::instance();
        $f3->set('producttypes', ProductType::select_data('title','id', array('organisation = ?', $f3->get('SESSION.organisation'))));
    }




}