<?php

namespace Application\Controllers;


use Application\Models\User;
use View\Base;

class Auth extends Controller {

    const MODULE = \Modules::BASIC;
    /**
     * init the View
     */
    public function beforeroute($f3,$params) {
        $ajax = \Base::instance()->get('AJAX');
        $f3->set('UI','project/application/views/');
        \Template::instance()->extend('pagebrowser','\Pagination::renderTag');
        if(!$ajax) {
            $this->view = new \View\Html();
        } else {
            $this->view = new \View\JSON();
        }
    }
    public function checkLogin(){
        $this->view->data = array('loggedin'=>static::isLoggedIn());
        echo $this->view->render();
        die();
    }

    /**
     * check login state
     * @return bool
     */
    static public function isLoggedIn() {
        /** @var Base $f3 */
        $f3 = \Base::instance();
        if( $f3->get('SESSION.last_activity') < time() - $f3->get('SESSION.expire_time') ) {
            return false;
        } else{
            $f3->set('SESSION.last_activity', time());
        }
        if ($f3->exists('SESSION.user_id')) {
            $user = new \Application\Models\User();
            $user->load(array('_id = ?',$f3->get('SESSION.user_id')));
            if(!$user->dry()) {
                $f3->set('BACKEND_USER',$user);
                return true;
            }
        }
        return false;
    }

    static public function isAdmin() {
        /** @var Base $f3 */
        $f3 = \Base::instance();
        if ($f3->exists('SESSION.user_id')) {
            $user = new \Application\Models\User();
            $user->load(array('_id = ?',$f3->get('SESSION.user_id')));
            if(!$user->dry()) {
                if($user->type->id === 1)
                    return true;
            }
        }

        return false;
    }

    /**
     * login procedure
     */
    const ACTION_LOGIN = "Auth::Login";
    public function login(\Base $f3) {

        $f3->set('page.title', 'Login');
        if ($f3->exists('POST.email') && $f3->exists('POST.password')) {
            sleep(3); // login should take a while to kick-ass brute force attacks
            $user = new \Application\Models\User();
            $user->load(array('email = ?',$f3->get('POST.email')));
            if (!$user->dry()) {
                if($user->active == 0){
                    \Flash::instance()->addMessage('Account not Active! Please consult your Admin', 'danger');
                } else {
                    // check hash engine
                    $hash_engine = $f3->get('password_hash_engine');
                    $valid = false;
                    if ($hash_engine == 'bcrypt') {
                        $valid = \Bcrypt::instance()->verify($f3->get('POST.password'), $user->password);
                    } elseif ($hash_engine == 'md5') {
                        $valid = (md5($f3->get('POST.password') . $f3->get('password_md5_salt')) == $user->password);
                    }

                    if ($valid) {
                        if(!$user->organisations[0]){

                            \Flash::instance()->addMessage('You are currently not allocated to any Organisation. Sorry Login Failed. Please consult your admin','danger');

                        } else{
                            $f3->set('SESSION.user_id', $user->_id);
                            $f3->set('SESSION.organisation', $user->organisations[0]->id);
                            $f3->set('SESSION.organisation_title', $user->organisations[0]->title);
                            $f3->set('SESSION.logged_in', true); //set you've logged in
                            $f3->set('SESSION.last_activity', time()); //your last activity was now, having logged in.
                            $remember_me = $f3->get('POST.remember_me');
                            $f3->set('SESSION.expire_time', 2*60*60);// 2 Hours
                            if($remember_me == 'on'){
                                $f3->set('SESSION.expire_time', 24*60*60*5);// 5 Days
                            }
                            $f3->reroute('/');
                        }


                    }
                    \Flash::instance()->addMessage('Wrong Email/Password Combination', 'danger');
                }
            }else{
                \Flash::instance()->addMessage('Wrong Email/Password Combination', 'danger');
            }

        }
        if($f3->get('HEADERS.X-Pjax'))
            sleep(100);
        $this->view->setTemplate('auth/login.html');
    }
    private function register($f3,$params) { //make impossible
        if ($f3->exists('POST.email') && $f3->exists('POST.password')) {
            $email = $f3->get('POST.email');
            $user = new \Application\Models\User();
            $user->load(array('email = ?',$f3->get('POST.email')));
            if(empty($email)){
                \Flash::instance()->addMessage('Email cannot be empty', 'danger');
            }elseif ($user->dry()) {

                $repeat_password = $f3->get('POST.password_repeat');
                if (isset($repeat_password) && ($f3->get('POST.password') == $f3->get('POST.password_repeat'))) {
                    $user->copyfrom('POST');
                    $user->active = 0;
                    $user->usertype = $f3->get('default.userType');
                    $user->activation_token = md5(time() . microtime());
                    $this->register_mail($user);
                    $user->save();
                    \Flash::instance()->addMessage('Account Registered Check your mail to actvate.', 'success');
                    $f3->reroute('/account');
                }

                \Flash::instance()->addMessage('Passwords Don\'t Match', 'danger');

            } else {
                \Flash::instance()->addMessage('Email already registered. Please try to login', 'danger');
            }


        }
        $this->view->setTemplate('register.html');
    }
    static function logged_in_user($field = "fullname"){
        $user = new \Application\Models\User();
        $user->load(array('id = ?', \Base::instance()->get('SESSION.user_id')));
        return $user->{$field};
    }
    public function activate($f3,$params){
        $token = $f3->get('GET.token');

        $user = (new User())->load(array('activation_token = ?', $token));
        if($user->dry() OR empty($token)){
            \Flash::instance()->addMessage('Activation token already used or not applicable', 'danger');
        } else {
            $user->active = 1;
            $user->activation_token = md5(time().microtime());
            $user->save();
            \Flash::instance()->addMessage('Account activated! Please login', 'success');
        }
        $f3->reroute('/auth/login');
    }
    function recover(\Base $f3){
        $token = $f3->get('GET.token');
        $user = new User();
        if(isset($token)){
            $user->load(array('rememberme_token = ?',$token));
            if(!$user->dry()){
                if($user->active == 1 ){
                    $user->rememberme_token = md5(time().rand(1,100));
                    $user->save();
                    $f3->set('SESSION.user_id',$user->id);

                    if(self::isLoggedIn()){
                        \Flash::instance()->addMessage('Please set a new password', 'success');
                        $f3->reroute('/admin/profile/edit');
                    }

                }else{
                    \Flash::instance()->addMessage('Account inactive! Please Check consult your Admin', 'danger');
                }

            }else{
                \Flash::instance()->addMessage('Token Already used or not recognized.', 'danger');
                $f3->reroute('/auth/forgotpassword');
            }
        } else{
            \Flash::instance()->addMessage('Token Missing, please enter your email again', 'danger');
            $f3->reroute('/auth/forgotpassword');
        }


    }
    function forgotPassword(\Base $f3){
        $email = $f3->get('POST.email');
        if(isset($email)){
            $user = (new User())->load(array('email = ?', $email));
            if(!$user->dry()){
                $user->rememberme_token = md5(time().microtime().'12');
                $user->save();
                $to = $user->email;
                $subject = $f3->get('site.title'). ': Account Recovery';
                \Base::instance()->set('user',$user);
                $message = \Template::instance()->render('mails/recover.html');
                \Mailer::instance()->send($subject,$to,$message);
                \Flash::instance()->addMessage('Email Sent. Please get directions from your email', 'success');
            }else{
                \Flash::instance()->addMessage('Email does not exist in our database. Please sig', 'success');
            }
        }
        $this->view->setTemplate('auth/recover.html');
    }

    public function logout(\Base $f3) {
        $f3->clear('SESSION');
        $f3->reroute('http://'.$f3->get('HOST').$f3->get('BASE').'/');
    }


}