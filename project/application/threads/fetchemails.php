<?php
/**
 * Created by PhpStorm.
 * User: geoff
 * Date: 3/18/16
 * Time: 4:07 PM
 */
namespace Application\Threads;

use Ddeboer\Imap\Exception\AuthenticationFailedException;
use Ddeboer\Imap\SearchExpression;
use Ddeboer\Imap\Server;
use Ddeboer\Imap\Search;

class FetchEmails{
    var $connection, $mailbox;
    const MAILBOX = 'INBOX';

    /**
     * FetchEmails constructor.
     * @param $host
     * @param int $port
     * @param $username
     * @param $password
     */
    function __construct($host, $port = 993, $username, $password){

        $server = new Server($host, $port);
        try{
            $connection = $server->authenticate($username, $password);
            $this->connection = $connection;
            if($this->connection)
            $this->mailbox = $this->connection->getMailbox(self::MAILBOX);
        } catch(AuthenticationFailedException $e){
            echo $e->getMessage();
            $logger = new \Log('logs/imap_'.date('Y_m_d').'.log');
            $logger->write($e->getFile() .': '. $e->getLine() . ': ' . $e->getMessage());
        }


    }

    /**
     * @param SearchExpression $search
     * @return \Ddeboer\Imap\Message[]|\Ddeboer\Imap\MessageIterator
     */
    function fetch(SearchExpression $search){
        return  $this->mailbox->getMessages($search);
    }
}