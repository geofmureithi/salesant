<?php
namespace Application\Models;

class Language extends \BaseModel {
    protected $table ="configs_language";
    protected $fieldConf = array(
        'key'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
        ),
        'value'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );
    static function reload(Organisation $organisation){
        $cache  = \Cache::instance() ;
        $cache->clear('customlang.'.$organisation->id);
        $self = new static;
        $values = $self->find(array('organisation = ?', $organisation->id));
        $langkeys= array();
        foreach ($values as $lang) {
           $langkeys[ 'customlang.'.$organisation->id.'.'. $lang->key] =  $lang->value;
        }
        $cache->set('customlang.'.$organisation->id,$langkeys);
    }

}