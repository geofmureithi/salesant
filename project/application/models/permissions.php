<?php
namespace Application\Models;

class Permissions extends \BaseModel {
    protected $table ="permissions";
    public $permissions = [
        'users'=>['index','add','edit','delete'],
        'books'=>['index','add', 'edit','delete','upload'],
        'categories'=>['index','add', 'edit','delete','upload'],
        'home'=>['index','exportBooks','exportSoldBooks'],
        'orders'=> ['index'],
        'usertypes'=>['index','add','edit','delete'],
    ];
    protected $fieldConf = [
        'usertype' => array(
            'belongs-to-one' => array('\Model\Usertype','user_type'),
        ),
    ];

}