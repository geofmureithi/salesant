<?php
namespace Application\Models;

class Transcript extends \BaseModel {
    protected $table ="transcripts";
    protected $_icon_pending = 'fa fa-fire';
    protected $_class_pending = 'bg-danger';
    protected $_icon_note = 'fa fa-tags';
    protected $_class_note = 'bg-dark light';
    protected $_icon_payment = 'fa fa-usd';
    protected $_class_payment = 'bg-success';

    protected $fieldConf = array(
        'sale' => array(
            'belongs-to-one' => '\Application\Models\Sale',
        ),
        'description'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        'type' => array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
            'default'=> 'note'
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );
    function get_icon(){
        $icon = '_icon_'. strtolower($this->type);
        if($this->$icon)
            return $this->$icon;
        return $this->_icon_note;
    }
    function get_bgClass(){
        $bg = '_class_'. strtolower($this->type);
        if($this->$bg)
            return $this->$bg;
        return $this->_class_note;
    }

}