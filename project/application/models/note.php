<?php
namespace Application\Models;

class Note extends \BaseModel {
    protected $table ="notes";
    protected $_icon = 'glyphicons glyphicons-comments';
    protected $fieldConf = array(
        'lead' => array(
            'belongs-to-one' => '\Application\Models\Lead',
        ),
        'description'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );
    function __construct(){
        parent::__construct();
        $this->afterinsert(function($self){
            Activity::push($self,$self->description,$this->_icon);
        });
    }

}