<?php
namespace Application\Models;

use \Application\Controllers\Organisations;

class Organisation extends \BaseModel {
    protected $table ="organisations";
    protected $fieldConf = array(
        'name' => array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'physicalAddress'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'contactPerson'=>array(
            'belongs-to-one'=> '\Application\Models\User'
        ),
        'postalAddress'=> array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'logo'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
		'telephone'=> array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
		'mobile'=> array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
		'email'=> array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'leads' => array(
            'has-many' => array('\Application\Models\Lead', 'organisation')
        ),
        'leadstatuses' => array(
            'has-many' => array('\Application\Models\LeadStatus', 'organisation')
        ),
        'opportunitystatuses' => array(
            'has-many' => array('\Application\Models\OpportunityStatus', 'organisation')
        ),
        'producttypes' => array(
            'has-many' => array('\Application\Models\ProductType', 'organisation')
        ),
        'leadsources' => array(
            'has-many' => array('\Application\Models\LeadSource', 'organisation')
        ),
        'opportunities' => array(
            'has-many' => array('\Application\Models\Opportunity', 'organisation')
        ),
		'website'=> array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'quoteTemplate' => array(
            'type' => \DB\SQL\Schema::DT_TEXT
        ),
        'invoiceTemplate' => array(
            'type' => \DB\SQL\Schema::DT_TEXT
        ),
        'receiptTemplate' => array(
            'type' => \DB\SQL\Schema::DT_TEXT
        ),
        'settings' => array(
            'type' => self::DT_JSON
        ),
        'users' => array(
            'has-many' => array('\Application\Models\User','organisations', 'users_organisations')
            ),
		'description' => array(
            'type' => \DB\SQL\Schema::DT_TEXT
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 0
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );

     static function selectableUsers($show="fullnames",$key="id"){
        $data = Organisation::id(\Base::instance()->get('SESSION.organisation'))->users;
        $final_data = [];
        if($data)
            foreach($data as $k=>$d)
                $final_data[$d->id] =  $d->$show;
        return $final_data;
    }
    function __construct(){
        parent::__construct();
        $this->afterinsert(function($self){
            $this->preloader($self);
        });
    }
    private function preloader(Organisation $self){
        $opportunity_statuses = array('Active','Won','Lost');
        foreach($opportunity_statuses as $opportunity_status){
            $status = new OpportunityStatus();
            $status->title = $opportunity_status;
            $status->description = $opportunity_status;
            $status->organisation = $self->id;
            $status->save();
        }
        $lead_sources = array('Referal', 'Website', 'Direct Marketing','Call','Events','Other');
        foreach($lead_sources as $lead_source){
            $source = new LeadSource();
            $source->title = $lead_source;
            $source->description = $lead_source;
            $source->organisation = $self->id;
            $source->save();
        }
        $lead_statuses = array('Hot','Warm','Cold');
        foreach($lead_statuses as $lead_status){
            $status = new LeadStatus();
            $status->title = $lead_status;
            $status->description = $lead_status;
            $status->organisation = $self->id;
            $status->save();
        }
    }


}