<?php
namespace Application\Models;

class Sale extends \BaseModel {
    protected $table ="sales";
    protected $fieldConf = array(
        'quotation' => array(
            'belongs-to-one' => '\Application\Models\Quotation',
        ),
        'contact' => array(
            'belongs-to-one' => '\Application\Models\Contact',
        ),
        'transcripts' => array(
            'has-many' => array('\Application\Models\Transcript','sale')
        ),
        'payments' => array(
            'has-many' => array('\Application\Models\Payment','sale')
        ),
        'products' => array(
            'has-many' => array('\Application\Models\Product','sales','sales_products')
        ),

        'amount'=>array(
            'type' => \DB\SQL\Schema::DT_FLOAT,
        ),
        'description'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );
    function __construct()
    {
        parent::__construct();
        $this->beforeerase(function($sale){
            $organisation = Organisation::id(\Base::instance()->get('SESSION.organisation'));
            $salecanceled = $organisation->settings['salecanceled'];
            if($salecanceled)
                foreach($sale->products as $product){
                    if(is_int($product))
                        $product = Product::id($product);
                    $product->status = $salecanceled;
                    $product->save();
                }
        });
        $this->afterupdate(function($sale){
            $organisation = Organisation::id(\Base::instance()->get('SESSION.organisation'));
            $salecompleted = $organisation->settings['salecompleted'];
            if($salecompleted && ($sale->amount <= $sale->paid))
                foreach($sale->products as $product){
                    if(is_int($product))
                        $product = Product::id($product);
                    $product->status = $salecompleted;
                    $product->save();
                }
        });
    }


    function removePayments(){
        $plans = $this->payments;
        if(!$plans)
            return false;
        foreach ($plans as $plan)
            $plan->erase();
        return true;
    }
    static function addFrom(Quotation $quotation){
        $sale = new static;
        $sale->quotation = $quotation;
        $sale->contact = $quotation->opportunity->contact;
        $sale->products = $quotation->products;
        $organisation = Organisation::id(\Base::instance()->get('SESSION.organisation'));
        $quotetosale = $organisation->settings['quotetosale'];
        if($quotetosale)
        foreach($sale->products as $product){
            $product = Product::id($product);
            $product->status = $quotetosale;
            $product->save();
        }
        $sale->amount = $quotation->amount;
        $sale->organisation = $quotation->organisation;
        $sale->description = "New <b>Sale</b> added. Total Amount: <b>{$quotation->amount}</b>";
        $sale->save();
        $note = new Transcript;
        $note->type = 'pending';
        $note->description = "<b> System: </b> " . \Customlang::process('Quotation') . " was successfully Approved";
        $note->sale = $sale;
        $note->save();
        $status = new OpportunityStatus;
        $status->load(array('title = ?', 'Won'));
        if($status->dry())
            $status->load();
        $quotation->status = $status->id;
        $quotation->save();
        return $sale;

    }
    function get_allactivities(){
        $transcript = new Transcript();
        return $transcript->find(array('sale = ?',$this->_id),array('order'=>'datecreated DESC'));
    }
    function get_paid(){
        $total = 0;
        if($this->payments)
        foreach($this->payments as $payment)
            $total += $payment->amount;
        return $total;

    }
    function expected($startDate, $endDate){
        $sale = new static;
        $sale = $sale->load(array('id = ?', $this->_id));
        $quotation = Quotation::id($sale->quotation->id)->orDie();
        $paymentPlans = new PaymentPlan();
        $plans =$paymentPlans->find(array('quotation = ? AND datecreated >= ? AND datecreated <= ?', $quotation->id, $startDate, $endDate));
        $total = 0;
        if($plans)
            foreach($plans as $plan)
                $total += $plan->amount;
        return $total;
    }
    function paid($startDate, $endDate){
        $sale = new static;
        $sale->filter('payments', array('datecreated >= ? AND datecreated <= ?', $startDate, $endDate));
        $sale = $sale->load(array('id = ?', $this->_id));

        $total = 0;
        if($sale->payments)
            foreach($sale->payments as $payment)
                $total += $payment->amount;
        return $total;
    }
    function late($startDate, $endDate){
        return  $this->expected($startDate,$endDate) - $this->paid($startDate,$endDate);
    }

}