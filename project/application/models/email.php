<?php
namespace Application\Models;

class Email extends \BaseModel {
    protected $table ="emails";
    protected $_icon = 'glyphicon glyphicons-envelope';
    protected $fieldConf = array(
        'lead' => array(
            'belongs-to-one' => '\Application\Models\Lead',
        ),
        'uniqueKey'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'from'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'to'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'cc'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
        'bcc'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
        'subject'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'attachments'=>array(
            'type' => self::DT_SERIALIZED,
        ),

        'description'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        #Alias for Draft/Sent
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 0
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );
    function __construct(){
        parent::__construct();
        $this->afterinsert(function($self){
            $attachment_html = "";
            if($self->attachments)
                foreach($self->attachments as $attachment)
                    $attachment_html.= "<a href='/emails/attachment/". \UrlEncryption::instance()->encode($attachment) . "'>". preg_replace("/^\d+\s+\w{1,2}\s+/", "",$attachment).'</a></br>';
            Activity::push($self,"<b>{$self->subject}</b><br/>From: {$self->from} - To: {$self->to} <br/><br/>" . $self->description . ($attachment_html?"<br/><b>Attachments</b><br/>{$attachment_html}":'' ),$this->_icon);

        });
        $this->beforeinsert(function($self){
            if(!$self->lead)
                $self->lead = \Base::instance()->get('PARAMS.motherid');
            if(!$self->from)
                $self->from = User::id(\Base::instance()->get('SESSION.user_id'))->email;
        });
    }

}