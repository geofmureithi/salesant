<?php
namespace Application\Models;

class Activity extends \BaseModel {
    protected $table ="activities";
    protected $_icon = 'glyphicons glyphicons-nameplate';
    protected $fieldConf = array(
        'lead' => array(
            'belongs-to-one' => '\Application\Models\Lead',
        ),
        'description'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
        'icon'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );
    static function push(\BaseModel $item, $text, $icon= null){
        $icon =$icon?$icon:$item->_icon;
        $activity = new static;
        $activity->lead = $item->lead;
        $activity->creator = $item->lead->creator;
        $activity->description = $text;
        $activity->datecreated = $item->datecreated;
        $activity->icon = $icon?$icon:$activity->_icon;
        $activity->save();
    }
    function get_icon($value){
        if(!$value)
            $value = $this->_icon;
        return $value;
    }
    static function happening(){
        $self = new static;
        $f3 = \Base::instance();
        $self->has('lead.tasks',array('creator = ? ', $f3->get('USER')->id));
        return $self->find(array('creator != ? AND organisation = ? AND icon != "glyphicon glyphicons-envelope"', $f3->get('USER')->id, $f3->get('SESSION.organisation')),array('limit'=>15, 'order'=> 'datecreated DESC'));
    }

}