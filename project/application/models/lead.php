<?php
namespace Application\Models;

class Lead extends \BaseModel {
    protected $table ="leads";
    protected $fieldConf = array(
        'title' => array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'status' => array(
            'belongs-to-one' => '\Application\Models\LeadStatus',
        ),
        'source' => array(
            'belongs-to-one' => '\Application\Models\LeadSource',
        ),
        'contacts' => array(
            'has-many' => array('\Application\Models\Contact','lead')
        ),
        'activities' => array(
            'has-many' => array('\Application\Models\Activity','lead')
        ),
        'notes' => array(
            'has-many' => array('\Application\Models\Note','lead')
        ),
        'opportunities' => array(
            'has-many' => array('\Application\Models\Opportunity','lead')
        ),
        'emails' => array(
            'has-many' => array('\Application\Models\Email','lead')
        ),
        'calls' => array(
            'has-many' => array('\Application\Models\Call','lead')
        ),
        'sync' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
        ),

        'newsletter' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),

		'description' => array(
            'type' => \DB\SQL\Schema::DT_TEXT
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );
    function get_completedtasks(){
        $tasks = new Task();
        return $tasks->find(array('status = ? AND lead = ?', 1,$this->_id));
    }

    function get_pendingtasks(){
        $tasks = new Task();
        return $tasks->find(array('status = ? AND lead = ?', 0,$this->_id),array('order'=>'datedue ASC  '));
    }
    function get_allactivities(){
        $activity = new Activity();
        return $activity->find(array('lead = ?',$this->_id),array('order'=>'datecreated DESC'));
    }
    function __construct()
    {
        parent::__construct();
        $this->afterinsert(function($lead){
            $organisation = Organisation::id(\Base::instance()->get('SESSION.organisation'));
            $createlead = $organisation->settings['createlead'];
            $lead->status = $createlead?$createlead:1;
            $lead->save();
        });
    }


}