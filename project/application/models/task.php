<?php
namespace Application\Models;

class Task extends \BaseModel {
    protected $table ="tasks";
    protected $_icon = 'glyphicons glyphicons-nameplate';
    protected $fieldConf = array(
        'lead' => array(
            'belongs-to-one' => '\Application\Models\Lead',
        ),
        'title'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
        ),
        'description'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
        'contact' => array(
            'belongs-to-one' => '\Application\Models\Contact',
        ),
        'datedue' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        'status' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 0
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'assignee' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );
    function    __construct(){
        parent::__construct();
        $this->afterinsert(function($self){
            Activity::push($self,"<small>New Task Created</small></br>".$self->title,$this->_icon);
        });
    }


}