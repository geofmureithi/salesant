<?php
namespace Application\Models;

use DB\CortexCollection;

class NewsletterGroup extends \BaseModel {
    protected $table ="newsletter_groups";
    protected $fieldConf = array(
        'title' => array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
        ),
        'algorithm'=>array(
            'type' => self::DT_SERIALIZED,
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );

    /**
     * Function get_contacts:
     * decodes an algorithm so as to fetch the right
     * sample algorithms
     * ----------['newsletterGroup'=> 17] searches contacts in newsletter group 17
     * ----------['leadStatus'=> 1, 'leadSource'=> 3] searches contacts with leads and leadStatus = 1 & leadsource= 3
     * Acceptable Codes:
     *  newsletterGroup
     *  leadStatus
     *  leadSource
     *  hasSale
     *  opportunityStatus
     *  productType
     *  hasEmail
     *  hasOpportunity
     *  hasCall
     * @return CortexCollection | null
     */
    function get_contacts(){
        $algorithms = $this->algorithm;
        $contacts = new Contact();
        $keys = array('newsletterGroup','leadStatus','leadSource','hasSale','opportunityStatus', 'productType','hasEmail','hasOpportunity' ,'hasCall');
        if(!is_array($algorithms))
            return null;
        foreach($algorithms as $key=>$algorithm){
                if(!in_array($key, $keys))
                    continue;
                switch($key){
                    case 'newsletterGroup':
                        //$contacts->reset();
                        return $contacts->find(array('newsletterGroup = ?', $algorithm));
                        break;
                    case 'leadStatus':
                        $contacts->has('lead', array('status = ?', $algorithm));
                        break;
                    case 'leadSource':
                        $contacts->has('lead', array('source = ?', $algorithm));
                        break;
                    case 'hasSale':
                        $contacts->has('sales', null);
                        break;
                    case 'hasOpporyunity':
                        $contacts->has('opportunities', null);
                        break;
                    case 'opportunityStatus':
                        $contacts->has('opportunities',array('status = ?', $algorithm));
                        break;
                    case 'productType':
                        $contacts->has('opportunities.producttype',array('id = ?', $algorithm));
                        break;
                    case 'hasEmail':
                        $contacts->has('lead.emails', null);
                        break;
                    case 'hasCall':
                        $contacts->has('lead.calls', null);
                        break;
                }
            }
        if($contacts)
        return $contacts->find(array('contacts.`organisation` = ? AND `email` IS NOT NULL', \Base::instance()->get('SESSION.organisation')));
    }

}