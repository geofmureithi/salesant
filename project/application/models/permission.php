<?php
namespace Application\Models;

class Permission extends \BaseModel {
    protected
        $table ="permissions",
        $fieldConf = array(
        'module'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'action' => array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'usertype' => array(
            'belongs-to-one' => '\Application\Models\UserType',
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );

    /**
     * Delete all permissions for a usertype
     * @param $usertype
     * @return bool
     */
    public static function dissolve($usertype){
        $model = new static;
        $permissions = $model->find(array('usertype = ?', (int) $usertype));
        if ($permissions)
        foreach($permissions as $permission)
            $permission->erase();
        return true;
    }

}