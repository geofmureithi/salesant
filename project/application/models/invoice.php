<?php
namespace Application\Models;

class Invoice extends \BaseModel {
    protected $table ="invoices";
    protected $fieldConf = array(
        'paymentPlan' => array(
            'belongs-to-one' => '\Application\Models\PaymentPlan',
        ),
        'receipt' => array(
            'has-one' => array('\Application\Models\Receipt','invoice')
        ),
        'amount'=>array(
            'type' => \DB\SQL\Schema::DT_FLOAT,
        ),
        'description'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );

}