<?php
namespace Application\Models;

class PaymentPlan extends \BaseModel {
    protected $table ="payment_plans";
    protected $fieldConf = array(
             'quotation' => array(
                 'belongs-to-one' => '\Application\Models\Quotation',
             ),
             'datedue' => array(
                 'type' => \DB\SQL\Schema::DT_DATE,
             ),
             'amount' => array(
                 'type' => \DB\SQL\Schema::DT_FLOAT
             ),
             'description' => array(
                 'type' => \DB\SQL\Schema::DT_TEXT
             ),
             'organisation' => array(
                 'belongs-to-one' => '\Application\Models\Organisation',
             ),
             'active' => array(
                 'type' => \DB\SQL\Schema::DT_BOOL,
                 'default'=> 1
             ),
             'creator' => array(
                 'belongs-to-one' => '\Application\Models\User',
             ),
             'datecreated' => array(
                 'type' => \DB\SQL\Schema::DT_TIMESTAMP,
                 'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
             ),
    );
    function set_amount($amount){
        return preg_replace('/[^0-9-.]/s','',$amount);

    }
    



}