<?php
namespace Application\Models;

class User extends \BaseModel {
    protected $table ="users";
	protected $fieldConf = array(
        'firstname'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'lastname'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'telephone'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'email'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'password'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'tasks'=>array(
            'has-many' => array('\Application\Models\Task','assignee')
        ),
        'leads'=>array(
            'has-many' => array('\Application\Models\Lead','creator')
        ),
        'opportunities'=>array(
            'has-many' => array('\Application\Models\Opportunity','creator')
        ),
        'quotations'=>array(
            'has-many' => array('\Application\Models\Quotation','creator')
        ),
        'products'=>array(
            'has-many' => array('\Application\Models\Product','creator')
        ),
        'organisations' => array(
            'has-many' => array('\Application\Models\Organisation','users', 'users_organisations')
        ),
        'smtp' => array(
            'type' => self::DT_SERIALIZED
        ),
        'imap' => array(
            'type' => self::DT_SERIALIZED
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'type' => array(
            'belongs-to-one' => '\Application\Models\UserType',
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
	);
    function __construct(){
        parent::__construct();
        $this->beforeerase(function($self){
            if(isset($self->orders) && $self->orders->count()> 0){
                \Flash::instance()->addMessage('User Acount cannot be deleted since it has orders tagged to it','warning');
                return false;
            }
        });
    }
    function set_password($value){
        $repeat = \Base::instance()->get('POST.password_repeat');
        if(!empty($value) && $value === $repeat){
            \Flash::instance()->addMessage('Password For User #' .$this->id .' Changed', 'warning');
            return \Bcrypt::instance()->hash($value,\Base::instance()->get('HASH'));
        } elseif(!empty($repeat)) {
            \Flash::instance()->addMessage('Password Change Failed. Please try again', 'warning');
        }
        return $this->password;
    }

    /**
     * Getter For Contact::fullname
     * @return string
     */
    function get_fullnames(){
        return ucwords($this->firstname . ' ' . $this->lastname);
    }
    function get_gravatar(){
        $userMail = $this->email;

        $imageWidth = '150'; //The image size

        $imgUrl = '//www.gravatar.com/avatar/'.md5($userMail).'fs='.$imageWidth;
        return $imgUrl;
    }
    static function isInOrganisation($organisation_id, $user_id){
        if (!$user_id) return false;
        $user= User::id($user_id);
        if($user->dry()) return false;
        foreach($user->organisations?$user->organisations:array() as $organisation){
            if($organisation->id == $organisation_id)
                return true;
        }
        return false;
    }
    function get_organisation(){
        $f3 = \Base::instance();
        if(!$f3->get('SESSION.organisation') && $this->organisations)
            $f3->set('SESSION.organisation', $this->organisations[0]->id); //default organisation

        if($f3->get('SESSION.organisation'))
            return $f3->get('SESSION.organisation');
        return false;
    }
    function get_pendingtasks(){
        $tasks = new Task();
        return $tasks->find(array('status = ? AND assignee = ? AND datecreated <= CURDATE() + INTERVAL 1 DAY AND organisation = ?', 0,$this->_id, \Base::instance()->get('SESSION.organisation')),array('order'=>'datedue ASC  '));
    }
    function get_myopportunities(){
        $opportunity = new Opportunity();
        return $opportunity->find(array('creator = ? and organisation = ?', $this->_id, \Base::instance()->get('SESSION.organisation')));
    }
    function get_myleads(){
        $lead = new Lead();
        return $lead->find(array('creator = ? and organisation = ?', $this->_id, \Base::instance()->get('SESSION.organisation')));
    }
    function get_myproducts(){
        $product = new Product();
        return $product->find(array('creator = ? and organisation = ?', $this->_id, \Base::instance()->get('SESSION.organisation')));
    }
    function get_myopportunitiesvalue(){
        $opportunity = new Opportunity();
        $opportunity->sum_opportunities = 'SUM(amount)';
        $opportunity= $opportunity->find(array('creator = ? and organisation = ?', $this->_id, \Base::instance()->get('SESSION.organisation')));
        $total = 0;
        if($opportunity)
        foreach($opportunity->castAll(0) as $opp)
            $total += $opp['amount'];
        return $total;
    }
    function get_opportunitiesvalue(){
        $opportunity = new Opportunity();
        $opportunity->sum_opportunities = 'SUM(amount)';
        $opportunity= $opportunity->find(array('organisation = ?', \Base::instance()->get('SESSION.organisation')));
        $total = 0;
        if($opportunity)
        foreach($opportunity->castAll(0) as $opp)
            $total += $opp['amount'];
        return $total;
    }
    function get_reportmyopportunities(){
        $opportunity = new Opportunity();
        return $opportunity->find(array('creator = ? and organisation = ? AND datecreated > ? AND datecreated < ? AND producttype = ?', $this->_id, \Base::instance()->get('SESSION.organisation'), \Base::instance()->get('POST.startDate'),\Base::instance()->get('POST.endDate'), \Base::instance()->get('POST.productType')));
    }
    function get_reportmyquotations(){
        $quotations = new Quotation();
        $quotations->has('products.type', array('id = ?', \Base::instance()->get('POST.productType')));
        return $quotations->find(array('creator = ? and organisation = ? AND datecreated > ? AND datecreated < ?', $this->_id, \Base::instance()->get('SESSION.organisation'), \Base::instance()->get('POST.startDate'),\Base::instance()->get('POST.endDate')));
    }
    function get_reportmyquotationsvalue(){
        $quotations = new Quotation();
        $quotations->has('products.type', array('id = ?', \Base::instance()->get('POST.productType')));
        $quotes = $quotations->find(array('creator = ? and organisation = ? AND datecreated > ? AND datecreated < ?', $this->_id, \Base::instance()->get('SESSION.organisation'), \Base::instance()->get('POST.startDate'),\Base::instance()->get('POST.endDate')));
        $total = 0;
        foreach($quotes?$quotes->castAll(0):array() as $quote)
            $total += $quote['amount'];
        return $total;
    }
    function get_reportmyleads(){
        $lead = new Lead();
        $lead->has('opportunities.producttype', array('id = ?', \Base::instance()->get('POST.productType')));
        return $lead->find(array('creator = ? and organisation = ? AND datecreated > ? AND datecreated < ?', $this->_id, \Base::instance()->get('SESSION.organisation'), \Base::instance()->get('POST.startDate'),\Base::instance()->get('POST.endDate')));
    }
    function get_reportmyproducts(){
        $product = new Product();
        return $product->find(array('creator = ? and organisation = ? AND datecreated > ? AND datecreated < ?', $this->_id, \Base::instance()->get('SESSION.organisation'), \Base::instance()->get('POST.startDate'),\Base::instance()->get('POST.endDate')));
    }
    function get_reportmyopportunitiesvalue(){
        $opportunity = new Opportunity();
        $opportunity= $opportunity->find(array('creator = ? and organisation = ? AND datecreated > ? AND datecreated < ? AND producttype = ?', $this->_id, \Base::instance()->get('SESSION.organisation'), \Base::instance()->get('POST.startDate'),\Base::instance()->get('POST.endDate'), \Base::instance()->get('POST.productType')));
        $total = 0;
        foreach($opportunity?$opportunity->castAll(0):array() as $opp)
            $total += $opp['amount'];
        return $total;
    }
    function get_reportopportunitiesvalue(){
        $opportunity = new Opportunity();
        $opportunity= $opportunity->find(array('organisation = ? AND datecreated > ? AND datecreated < ?', \Base::instance()->get('SESSION.organisation'), \Base::instance()->get('POST.startDate'),\Base::instance()->get('POST.endDate')));
        $total = 0;
        foreach($opportunity?$opportunity->castAll(0):array() as $opp)
            $total += $opp['amount'];
        return $total;
    }




}