<?php
namespace Application\Models;

class ProductStatus extends \BaseModel {
    protected $table ="configs_product_statuses";
    protected $fieldConf = array(
        'title'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'badge'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'description'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        'products'=>array(
            'has-many' => array('\Application\Models\Product', 'status')
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );

}