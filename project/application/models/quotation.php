<?php
namespace Application\Models;

class Quotation extends \BaseModel {
    protected $table ="quotations";
    protected $fieldConf = array(
        'opportunity' => array(
            'belongs-to-one' => '\Application\Models\Opportunity',
        ),
        'contact' => array(
            'belongs-to-one' => '\Application\Models\Contact',
        ),
        'sale' => array(
            'has-one' => array('\Application\Models\Sale','quotation')
        ),
        'paymentPlan' => array(
            'has-many' => array('\Application\Models\PaymentPlan','quotation')
        ),
        'products' => array(
            'has-many' => array('\Application\Models\Product','quotations','quotations_products')
        ),

        'amount'=>array(
            'type' => \DB\SQL\Schema::DT_FLOAT,
        ),
        'data'=>array(
            'type' => self::DT_JSON,
        ),
        'description'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );
    function removePlans(){
        $plans = $this->paymentPlan;
        if(!$plans)
            return false;
        foreach ($plans as $plan)
            $plan->erase();
        return true;
    }
    function setData($data = array()){
        $id = $this->_id;
        $this->data = array($data); //Lets First Save the Raw Data For another Day.
        $contact = new Contact();
        $contact = $contact->load(array('email = ?', $data['client']['email']));
        if($data['quotation']['opportunity']){
            $opportunity = Opportunity::id($data['quotation']['opportunity']);
            if($opportunity->dry()){
                // Add $opportunity->
            }
            $this->opportunity = $opportunity;


        }




    }
    function __construct(){
        parent::__construct();
        $this->beforeerase(function($self){
            if($self->sale){
                \Flash::instance()->addMessage(\Customlang::process('Quotation') . ' cannot be deleted because it has a Sale tied to it. Please delete the Sale first.', \Flash::DANGER);
                return false;
            }
            $self->removePlans();

        });
    }


}