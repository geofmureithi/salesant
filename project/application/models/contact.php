<?php
namespace Application\Models;

class Contact extends \BaseModel {
    protected $table ="contacts";
    protected $fieldConf = array(
        'firstname' => array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'lastname' => array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
		'telephone'=> array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
		'email'=> array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'pin'=> array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'position'=> array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'postalAddress'=> array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'physicalAddress'=> array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'idNo'=> array(
            'type' => \DB\SQL\Schema::DT_INT8,
        ),
        'dateOfBirth' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
        ),

        'assignee' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'lead' => array(
            'belongs-to-one' => '\Application\Models\Lead',
        ),
        'calls' => array(
            'has-many' => array('\Application\Models\Call','contact')
        ),
        'sales' => array(
            'has-many' => array('\Application\Models\Sale','contact')
        ),
        'opportunities' => array(
            'has-many' => array('\Application\Models\Sale','contact')
        ),
        'newsletter' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'newsletterGroup'=>array(
            'type' => \DB\SQL\Schema::DT_INT8,
        ),
		'description' => array(
            'type' => \DB\SQL\Schema::DT_TEXT
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );

    /**
     * Getter For Contact::fullname
     * @return string
     */
    function get_fullnames(){
        return ucwords($this->firstname . ' ' . $this->lastname);
    }
    function set_name($name) {
        $parts = array();

        while ( strlen( trim($name)) > 0 ) {
            $name = trim($name);
            $string = preg_replace('#.*\s([\w-]*)$#', '$1', $name);
            $parts[] = $string;
            $name = trim( preg_replace('#'.$string.'#', '', $name ) );
        }

        if (empty($parts)) {
            return false;
        }

        $parts = array_reverse($parts);
        //$name = array();
        $this->firstname = $parts[0];
        //$name['middle_name'] = (isset($parts[2])) ? $parts[1] : '';
        $this->lastname = (isset($parts[2])) ? $parts[2] : ( isset($parts[1]) ? $parts[1] : '');
        unset($this->name);
        return;
    }
    function get_gravatar(){
        $userMail = $this->email;

        $imageWidth = '150'; //The image size

        $imgUrl = '//www.gravatar.com/avatar/'.md5($userMail).'fs='.$imageWidth;
        return $imgUrl;
    }
    



}