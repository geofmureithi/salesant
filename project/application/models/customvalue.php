<?php
namespace Application\Models;

class CustomValue extends \BaseModel {
    protected $table ="custom_fields_value";
    protected $fieldConf = array(
        'field' => array(
            'belongs-to-one' => '\Application\Models\CustomField',
        ),
        'value'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
        ),
        'product'=> array(
            'belongs-to-one' => '\Application\Models\Product',
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );

}