<?php
namespace Application\Models;

class Call extends \BaseModel {
    protected $table ="calls";
    protected $_icon = 'glyphicon glyphicon-earphone';
    protected $fieldConf = array(
        'lead' => array(
            'belongs-to-one' => '\Application\Models\Lead',
        ),
        'description'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
        'contact' => array(
            'belongs-to-one' => '\Application\Models\Contact',
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );
    function __construct(){
        parent::__construct();
        $this->afterinsert(function($self){
            Activity::push($self,'Call to '.$self->contact->fullnames. ': '.$self->description,$this->_icon);
        });
    }

}