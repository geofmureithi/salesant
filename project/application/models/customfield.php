<?php
namespace Application\Models;

class CustomField extends \BaseModel {
    protected $table ="custom_fields";
    protected $fieldConf = array(
        'producttype' => array(
            'belongs-to-one' => '\Application\Models\ProductType',
        ),
        'name'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
        ),
        'slug'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
        ),
        'default'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        'description' => array(
            'type' => \DB\SQL\Schema::DT_TEXT
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );
    public function set_name($value){
        $this->slug = str_replace('-','',\Web::instance()->slug($value));
        return $value;
    }
    function customValue($product){
        if(\Base::instance()->get('GET.handler')== 'filter')
            return \Base::instance()->get('POST.'. $this->_id);
        $custom = new CustomValue();
        return $custom->load(array('field = ? AND product = ?', $this->_id, $product))->value;
    }

    public function __toString()
    {
        return $this->name?ucwords($this->name):'Undefined';
    }
}