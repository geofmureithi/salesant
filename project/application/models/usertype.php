<?php
namespace Application\Models;

class UserType extends \BaseModel {
    protected
        $table ="user_types",
        $fieldConf = array(
        'code'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'title' => array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'users' => array(
            'has-many'=> array('\Application\Models\User','type')
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 0
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );
    function __construct(){
        parent::__construct();
        $this->beforeerase(function($self){
            if(!count($self->users)){
                \Flash::instance()->addMessage('Usertype cannot be deleted since it has users tagged to it','warning');
                return false;
            }
        });
    }

    function hasPermission($module, $action){
        return !(new Permission)->load(array('usertype = ? AND module = ? AND action = ?', $this->id, $module, $action ))->dry();


    }

}