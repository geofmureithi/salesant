<?php
namespace Application\Models;

class LeadSource extends \BaseModel {
    protected $table ="configs_lead_sources";
    protected $fieldConf = array(
        'title'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'description'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );

}