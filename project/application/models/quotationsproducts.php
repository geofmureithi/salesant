<?php
namespace Application\Models;

class QuotationsProducts extends \BaseModel {
    protected $table ="quotations_products";
    protected $fieldConf = array(
        'products' => array(
            'type' => \DB\SQL\Schema::DT_INT8,
        ),
        'quotations' => array(
            'type' => \DB\SQL\Schema::DT_INT8,
        ),
        'quantity' => array(
            'type' => \DB\SQL\Schema::DT_INT8,
        ),

    );
    function __construct(){
        parent::__construct();
        $this->beforeinsert(function(){});
    }
    static function clean($quotation){
        $self = new static;
        $qps = $self->find(array('quotations = ?', $quotation));
        if(!$qps)
            return false;
        foreach($qps as $qp)
            $qp->erase();
        return true;
    }
    static function getQuantity($quotation, $product){
        $self = new static;
        $self->load(array('quotations = ? AND products = ?', $quotation, $product));
        return $self->quantity;
    }
}