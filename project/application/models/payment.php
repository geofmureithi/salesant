<?php
namespace Application\Models;

class Payment extends \BaseModel {
    protected $table ="payments";
    protected $fieldConf = array(
             'sale' => array(
                 'belongs-to-one' => '\Application\Models\Sale',
             ),
             'datepaid' => array(
                 'type' => \DB\SQL\Schema::DT_DATE,
             ),
             'amount' => array(
                 'type' => \DB\SQL\Schema::DT_FLOAT
             ),
             'receiptNo' => array(
                 'type' => \DB\SQL\Schema::DT_VARCHAR128
             ),
             'chequeNo' => array(
                 'type' => \DB\SQL\Schema::DT_VARCHAR128
             ),
             'bank' => array(
                 'type' => \DB\SQL\Schema::DT_VARCHAR256
             ),

             'description' => array(
                 'type' => \DB\SQL\Schema::DT_TEXT
             ),
             'organisation' => array(
                 'belongs-to-one' => '\Application\Models\Organisation',
             ),
             'active' => array(
                 'type' => \DB\SQL\Schema::DT_BOOL,
                 'default'=> 1
             ),
             'creator' => array(
                 'belongs-to-one' => '\Application\Models\User',
             ),
             'datecreated' => array(
                 'type' => \DB\SQL\Schema::DT_TIMESTAMP,
                 'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
             ),
    );
    function set_amount($amount){
        return preg_replace('/[^0-9-.]/s','',$amount);

    }
    function __construct(){
        parent::__construct();
        $this->afterinsert(function($self){
            $note = new Transcript;
            $note->type = 'payment';
            $amount =\Currency::format($self->amount);
            $note->description = "<b> New Payment: </b> {$amount} Paid Via {$self->bank}";
            $note->datecreated = $self->datepaid;
            $note->sale = $self->sale;
            $note->save();
            $sale = $self->sale;
                $sale->active = 1;
            $sale->save();
        });
        $this->afterupdate(function($self){
            $note = new Transcript;
            $note->type = 'payment';
            $amount =\Currency::format($self->amount);
            $note->description = "<b> Payment #({$self->id}) Updated: </b> {$amount} Paid Via {$self->bank}";
            $note->datecreated = $self->datepaid;
            $note->sale = $self->sale;
            $note->save();
            $sale = $self->sale;
            $sale->active = $sale->active;
            $sale->save();
        });
    }


}