<?php
/**
 * Created by PhpStorm.
 * User: geoff
 * Date: 5/18/16
 * Time: 9:51 AM
 */

namespace Application\Models;


class Notification extends \BaseModel
{
    protected $table ="notifications";
    protected $fieldConf = array(
        'title' => array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
        ),
        'link' => array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
        ),
        'description'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );

    /**
     * @return bool
     */
    function opened(){
        $this->active = 0;
        return $this->save();
    }

}