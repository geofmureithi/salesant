<?php
namespace Application\Models;

class Product extends \BaseModel {
    protected $table ="products";
    protected $fieldConf = array(
        'title' => array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
            'index'=>true,
            'unique'=>true,
        ),
        'type' => array(
            'belongs-to-one' => '\Application\Models\ProductType',
        ),
		'price' => array(
            'type' => \DB\SQL\Schema::DT_FLOAT
        ),
        'unit' => array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128
        ),
        'opportunities' => array(
            'has-many' => array('\Application\Models\Opportunity','products','opportunity_products')
        ),
        'quotations' => array(
            'has-many' => array('\Application\Models\Quotation','products','quotations_products')
        ),
        'sales' => array(
            'has-many' => array('\Application\Models\Sale','products','sales_products')
        ),
        'status' => array(
            'belongs-to-one' => '\Application\Models\ProductStatus',
            'default'=> 1
        ),
        'photo' => array(
            'type' => \DB\SQL\Schema::DT_TEXT
        ),
        'customdata' => array(
            'has-many' => array('\Application\Models\CustomValue','product')
        ),
        'description' => array(
            'type' => \DB\SQL\Schema::DT_TEXT
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'isService' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 0
        ),
        'isResale' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 0
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );

    function set_custom($values){
        foreach($values as $key=>$value){
            $model = new CustomValue();
            $model->load(array('product = ? AND field = ?', $this->_id, $key));
            if($model->dry())
                $model->reset();
            $model->field = $key;
            $model->product = $this->_id;
            $model->value = $value;
            $model->save();
        }
    }
    function get_custom(){
        $data =  $this->customdata;
        $values = array();
        if($data)
        foreach($data as $value){
            $values[$value->field->slug] = $value->value;
        }
        return $values;
    }
    static function hot($count = 10){
        $products = new static;
        $products->countRel('sales');
        $products->countRel('quotations');
        $products->countRel('opportunities');
        return $products->find(array('organisation = ? ', \Base::instance()->get('SESSION.organisation')), array('order'=>'count_sales DESC, count_opportunities DESC, title','limit'=> $count));
    }
    static function cold($count = 10){
        $products = new static;
        $products->countRel('sales');
        $products->countRel('quotations');
        $products->countRel('opportunities');
        return $products->find(array('organisation = ? ', \Base::instance()->get('SESSION.organisation')), array('order'=>'count_sales ASC, count_opportunities ASC, title','limit'=> $count));
    }
    function get_totalSales(){
        if(!$this->sales)
            return 0.0;
        $total = 0.0;
        foreach ($this->sales as $sale) {
            $total += $sale->amount;
        }
        return $total;
    }


}