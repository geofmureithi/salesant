<?php
namespace Application\Models;

class Setting extends \BaseModel {
    protected $table ="settings";
    public  $list = array('services','products');
    protected $fieldConf = array(
        'name'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'value'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );
    static function reload(\Base $f3){
        $settings = (new static)->find(null)?(new static)->find(null):array();
        foreach($settings as $v){
            $f3->set($v->name, $v->value);
        }
    }
}