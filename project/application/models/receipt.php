<?php
namespace Application\Models;

class Receipt extends \BaseModel {
    protected $table ="receipts";
    protected $fieldConf = array(
        'invoice' => array(
            'belongs-to-one' => '\Application\Models\Invoice',
        ),
        'amount'=>array(
            'type' => \DB\SQL\Schema::DT_FLOAT,
        ),
        'mode'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
        ),
        'bank'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
        ),
        'transactionId'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
        ),

        'description'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );

}