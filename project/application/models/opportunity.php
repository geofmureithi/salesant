<?php
namespace Application\Models;

class Opportunity extends \BaseModel {
    protected $table ="opportunities";
    protected $fieldConf = array(
        'lead' => array(
            'belongs-to-one' => '\Application\Models\Lead',
        ),
        'contact' => array(
            'belongs-to-one' => '\Application\Models\Contact',
        ),
        'assignee' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'producttype' => array(
            'belongs-to-one' => '\Application\Models\ProductType',
        ),
        'expecteddate' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
        ),
		'amount' => array(
            'type' => \DB\SQL\Schema::DT_FLOAT
        ),
        'confidence' => array(
            'type' => \DB\SQL\Schema::DT_INT4
        ),
        'status' => array(
            'belongs-to-one' => '\Application\Models\OpportunityStatus',
        ),
        'products' => array(
            'has-many' => array('\Application\Models\Product','opportunities','opportunity_products')
        ),
        'quotations' => array(
            'has-many' => array('\Application\Models\Quotation','opportunity')
        ),
        'description' => array(
            'type' => \DB\SQL\Schema::DT_TEXT
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );
    function __construct(){
        parent::__construct();
        $this->afterinsert(function($self){
            Activity::push($self,"New Opportunity Created</br>{{$self->producttype->title}}</br>".$self->description);
        });
        $this->beforeerase(function($self){
            Activity::push($self,"Opportunity Was Deleted</br>{{$self->producttype->title}}</br>".$self->description);
            if($self->quotations)
                foreach($self->quotations as $quotation)
                    $quotation->erase();
            return true;
        });
    }
    static function hasProduct($product_id, $opportunity_id){
        if (!$opportunity_id) return false;
        $opportunity= Opportunity::id($opportunity_id);
        if($opportunity->dry()) return false;
        if(!$opportunity->products)
            return false;
        foreach($opportunity->products as $product){
            if($product->id == $product_id)
                return true;
        }
        return false;
    }
    function get_sale(){
        $quotations = $this->quotations;
        if(!$quotations)
            return false;
        foreach($quotations as $quotation)
            if($quotation->sale) return true;
        return false;
    }
    



}