<?php
namespace Application\Models;

class OpportunityStatus extends \BaseModel {
    protected $table ="configs_opportunity_statuses";
    protected $fieldConf = array(
        'title'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'description'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
        'opportunities' => array(
            'has-many' => array('\Application\Models\Opportunity', 'status')
        ),
        'organisation' => array(
            'belongs-to-one' => '\Application\Models\Organisation',
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'creator' => array(
            'belongs-to-one' => '\Application\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );

}