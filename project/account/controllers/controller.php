<?php

namespace Account\Controllers;


use Account\Models\User;

class Controller extends \BaseController{

    function beforeroute($f3,$params) {
        //code here;
        $f3 = \Base::instance();
        if(!\Account\Controllers\Auth::isLoggedIn())
            $f3->reroute('/auth/login');
        $f3->set('UI','project/account/views/');
        $user = new User();
        $user = $user->load(array('id = ?', $f3->get('SESSION.user_id')));
        $f3->set('USER',$user);
        $f3->set('subdomain',$user->account->subdomain);
        $f3->set('account',$user->account);
        parent::beforeroute($f3,$params);

    }
    function delete(\Base $f3, $params){

        $this->model->reset();
        $this->model
            ->load(array('id = ?',$params['id']));

        try{
            $this->model->erase();
            \Flash::instance()->addMessage(\Customlang::process(ucfirst($this->singular)). " #".$params['id']." Successfully Deleted",'');
        }
        catch (\Exception $e){
            \Flash::instance()->addMessage($e->getMessage(),'error');
        }
        if(!$f3->get('AJAX'))
            $f3->reroute('/' . $f3->get('PARAMS.app'). '/' . strtolower($this->prural). '/index');
        $this->view->data['message'] = "Delete was Successful";
    }
    final static function fileDetails($filename){
        $myFS = new \FAL\LocalFS('files/');
        $details = new \stdClass();
        if($myFS->exists($filename)){
            $details->name= basename($filename);
            $details->path = '/files/'.$filename;
            $details->size = $myFS->size($filename);
        }
        return $details;
    }


    function afterroute($f3,$params) {
        //code here;
        $f3->set('fileDetails', function($file){
            return static::fileDetails($file);
        });
        parent::afterroute($f3,$params);

    }

}