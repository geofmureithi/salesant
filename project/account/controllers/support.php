<?php

namespace Account\Controllers;


class Support extends Controller
{
    function index(\Base $base, $params){
        $base->set('page.title','Support');
        $base->set('INNER.PAGE', 'support/main.html');
    }
}