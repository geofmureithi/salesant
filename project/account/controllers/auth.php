<?php

namespace Account\Controllers;


use Account\Models\Account;
use Account\Models\User;
use Application\Models\Organisation;
use View\Base;

class Auth extends Controller {
    
    /**
     * init the View
     */
    public function beforeroute($f3,$params) {
        $ajax = \Base::instance()->get('AJAX');
        $f3->set('UI','project/account/views/');
        \Template::instance()->extend('pagebrowser','\Pagination::renderTag');
        if(!$ajax) {
            $this->view = new \View\Html();
        } else {
            $this->view = new \View\JSON();
        }
    }

    /**
     * check login state
     * @return bool
     */
    static public function isLoggedIn() {
        /** @var Base $f3 */
        $f3 = \Base::instance();
        if( $f3->get('SESSION.last_activity') < time() - $f3->get('SESSION.expire_time') ) {
            return false;
        } else{
            $f3->set('SESSION.last_activity', time());
        }
        if ($f3->exists('SESSION.user_id')) {
            $user = new \Account\Models\User();
            $user->load(array('_id = ?',$f3->get('SESSION.user_id')));
            if(!$user->dry()) {
                $f3->set('BACKEND_USER',$user);
                return true;
            }
        }
        return false;
    }

    static public function isAdmin() {
        /** @var Base $f3 */
        $f3 = \Base::instance();
        if ($f3->exists('SESSION.user_id')) {
            $user = new \Account\Models\User();
            $user->load(array('_id = ?',$f3->get('SESSION.user_id')));
            if(!$user->dry()) {
                if($user->type->id === 1)
                    return true;
            }
        }

        return false;
    }

    /**
     * login procedure
     */
    const ACTION_LOGIN = "Auth::Login";
    public function login(\Base $f3) {

        $f3->set('page.title', 'Login');
        if ($f3->exists('POST.email') && $f3->exists('POST.password')) {
            sleep(3); // login should take a while to kick-ass brute force attacks
            $user = new \Account\Models\User();
            $user->load(array('email = ?',$f3->get('POST.email')));
            if (!$user->dry()) {
                if($user->active == 0){
                    \Flash::instance()->addMessage('Account not Active! Please consult your Admin or check mail to activate', 'danger');
                } else {
                    // check hash engine
                    $hash_engine = $f3->get('password_hash_engine');
                    $valid = false;
                    if ($hash_engine == 'bcrypt') {
                        $valid = \Bcrypt::instance()->verify($f3->get('POST.password'), $user->password);
                    } elseif ($hash_engine == 'md5') {
                        $valid = (md5($f3->get('POST.password') . $f3->get('password_md5_salt')) == $user->password);
                    }

                    if ($valid) {
                            $f3->set('SESSION.user_id', $user->_id);
                            $f3->set('SESSION.logged_in', true); //set you've logged in
                            $f3->set('SESSION.last_activity', time()); //your last activity was now, having logged in.
                            $remember_me = $f3->get('POST.remember_me');
                            $f3->set('SESSION.expire_time', 2*60*60);// 2 Hours
                            if($remember_me == 'on'){
                                $f3->set('SESSION.expire_time', 24*60*60*5);// 5 Days
                            }
                            $f3->reroute('/');



                    }
                    \Flash::instance()->addMessage('Wrong Email/Password Combination', 'danger');
                }
            }else{
                \Flash::instance()->addMessage('Wrong Email/Password Combination', 'danger');
            }

        }
        $this->view->setTemplate('auth/login.html');
    }
    public function register($f3,$params) { //make possible

        if ($f3->exists('POST.user.email') && $f3->exists('POST.user.password')) {

            $email = $f3->get('POST.user.email');
            $user = new \Account\Models\User();
            $user->load(array('email = ?',$f3->get('POST.user.email')));
            if(empty($email)){
                \Flash::instance()->addMessage('Email cannot be empty', 'danger');
            }elseif ($user->dry()) {

                $repeat_password = $f3->get('POST.password_repeat');
                if (isset($repeat_password) && ($f3->get('POST.user.password') == $f3->get('POST.password_repeat'))) {

                    $subdomain = \Web::instance()->slug($f3->get('POST.account.username'));
                    $sd = (new \Subdomain());
                    $sd->load(array('@subdomain = ?', $subdomain));
                    if($sd->dry()){
                        $user->copyfrom('POST.user');
                        $user->active = 1;
                        $user->type = 1;
                        $user->activation_token = md5(time() . microtime());
                        if($user->save()){
                            //$this->register_mail($user);
                            \Flash::instance()->addMessage('Account Registered Check your mail to activate.', 'success');
                            $this->createOrganisation($f3->get('POST.account'),$f3->get('POST.user'));
                            $account = new Account();
                            $account->user = $user;
                            $account->subdomain = $subdomain;
                            $account->creator = $user;
                            $account->trial = 1;
                            $account->expires = date('Y-m-d', strtotime("+30 days")); //30 days trial
                            $account->save();
                            $f3->reroute('/auth/login');
                        }

                    } else {
                        \Flash::instance()->addMessage('Username already taken. Please Choose a different username', \Flash::DANGER);
                    }


                } else{
                    \Flash::instance()->addMessage('Passwords Don\'t Match', 'danger');
                }



            } else {
                \Flash::instance()->addMessage('Email already registered. Please try to login', 'danger');
            }


        }
        $this->view->setTemplate('auth/register.html');
    }
    static function logged_in_user($field = "fullname"){
        $user = new \Account\Models\User();
        $user->load(array('id = ?', \Base::instance()->get('SESSION.user_id')));
        return $user->{$field};
    }
    public function activate($f3,$params){
        $token = $f3->get('GET.token');

        $user = (new User())->load(array('activation_token = ?', $token));
        if($user->dry() OR empty($token)){
            \Flash::instance()->addMessage('Activation token already used or not applicable', 'danger');
        } else {
            $user->active = 1;
            $user->activation_token = md5(time().microtime());
            $user->save();
            \Flash::instance()->addMessage('Account activated! Please login', 'success');
        }
        $f3->reroute('/auth/login');
    }
    function recover(\Base $f3){
        $token = $f3->get('GET.token');
        $user = new User();
        if(isset($token)){
            $user->load(array('rememberme_token = ?',$token));
            if(!$user->dry()){
                if($user->active == 1 ){
                    $user->rememberme_token = md5(time().rand(1,100));
                    $user->save();
                    $f3->set('SESSION.user_id',$user->id);

                    if(self::isLoggedIn()){
                        \Flash::instance()->addMessage('Please set a new password', 'success');
                        $f3->reroute('/admin/profile/edit');
                    }

                }else{
                    \Flash::instance()->addMessage('Account inactive! Please Check consult your Admin', 'danger');
                }

            }else{
                \Flash::instance()->addMessage('Token Already used or not recognized.', 'danger');
                $f3->reroute('/auth/forgotpassword');
            }
        } else{
            \Flash::instance()->addMessage('Token Missing, please enter your email again', 'danger');
            $f3->reroute('/auth/forgotpassword');
        }


    }
    function forgotPassword(\Base $f3){
        $email = $f3->get('POST.email');
        if(isset($email)){
            $user = (new User())->load(array('email = ?', $email));
            if(!$user->dry()){
                $user->rememberme_token = md5(time().microtime().'12');
                $user->save();
                $to = $user->email;
                $subject = $f3->get('site.title'). ': Account Recovery';
                \Base::instance()->set('user',$user);
                $message = \Template::instance()->render('mails/recover.html');
                \Mailer::instance()->send($subject,$to,$message);
                \Flash::instance()->addMessage('Email Sent. Please get directions from your email', 'success');
            }else{
                \Flash::instance()->addMessage('Email does not exist in our database. Please sign up', 'success');
            }
        }
        $this->view->setTemplate('auth/recover.html');
    }
    private function createOrganisation($account, $userAccount){
        $f3 = \Base::instance();
        $pdo = new \PDO("mysql:host=localhost", $f3->get('mysql.username'), $f3->get('mysql.password'));
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $subdomain = \Web::instance()->slug($account['username']);

        $dbname = 'sub_'.$subdomain;
        if($dbname == '')
            return false;
        $pdo->query("CREATE DATABASE IF NOT EXISTS $dbname");
        $sd = new \Subdomain();
        $sd->subdomain = $subdomain;
        $sd->save();
        $f3->set('AppDB1', new \DB\SQL('mysql:host=localhost;port=3306;dbname='. $dbname, $f3->get('mysql.username'), $f3->get('mysql.password')));

        $db = $f3->get('AppDB1');
        if($db->driver() == 'mysql'){
            $db->begin();
            $db->exec($f3->read('config/install.sql'));
            $db->commit();
        } else {
            \Web::instance()->request("http://{$subdomain}.salesant.com/models/setup");
            $models = array('Activity','Call','Contact','CustomField', 'CustomValue', 'Email','Invoice', 'Lead','Leadsource','Leadstatus','Note', 'Opportunity','OpportunityStatus','Organisation','PaymentPlan','Product','ProductType','Quotation','Receipt','Setting','Task','User','UserType','Document','Language', 'Sale', 'Payment', 'Transcript','NewsletterGroup', 'NewsletterTemplate');
            foreach($models as $model){
                $model = '\Application\Models\\' . $model;
                $x = (new $model);
                $x::install();
            }
        }

        $f3->set('userAccount', $userAccount);
        $usertype = new \Application\Models\UserType();
        $usertype->title = 'Super Admin';
        $usertype->active = 1;
        $usertype->save();
        $user = new \Application\Models\User();
        $user->copyfrom('userAccount');
        $user->active = 1;
        $user->type = $usertype;
        $user->save();

        $organisation = new Organisation();
        $organisation->creator = $user;
        $organisation->users = $user;
        $organisation->telephone = $user->telephone;
        $organisation->email = $user->email;
        $organisation->active = 1;
        $organisation->name = ucwords($account['organisation']);
        $organisation->save();
        \Flash::instance()->clearMessages();
        \Flash::instance()->addMessage("Account[<a href='http://{$subdomain}.salesant.com'>{$subdomain}.salesant.com</a>] was created. Please login to manage the account", \Flash::SUCCESS);

        return true;

    }

    public function logout(\Base $f3) {
        $f3->clear('SESSION');
        $f3->reroute('http://'.$f3->get('HOST').$f3->get('BASE').'/');
    }


}