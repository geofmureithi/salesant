<?php

namespace Account\Controllers;


use Application\Models\Organisation;

class Organisations extends Controller
{
    function index(\Base $base, $params){
        $base->set('page.title','Organisations');
        $base->set('INNER.PAGE','organisations/listing.html');
        $organisations = null;
        $base->set('AppDB1', new \DB\SQL('mysql:host=localhost;port=3306;dbname=sub_'. $base->get('subdomain'), $base->get('mysql.username'), $base->get('mysql.password')));
        $organisations =  (new Organisation())->find();
        $base->set('organisations', $organisations);
    }
}