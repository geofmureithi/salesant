<?php
namespace Account\Models;

class User extends Model {
    protected $table ="users";
	protected $fieldConf = array(
        'firstname'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'lastname'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'telephone'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'email'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'password'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 1
        ),
        'account' => array(
            'has-one' => array('\Account\Models\Account', 'user')
        ),
        'type' => array(
            'belongs-to-one' => '\Account\Models\UserType',
        ),
        'creator' => array(
            'belongs-to-one' => '\Account\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
	);
    function __construct(){
        parent::__construct();
      /*  $this->beforeerase(function($self){
            if(isset($self->orders) && $self->orders->count()> 0){
                \Flash::instance()->addMessage('User Acount cannot be deleted since it has orders tagged to it','warning');
                return false;
            }
        });
      */
    }
    function set_password($value){
        $repeat = \Base::instance()->get('POST.password_repeat');
        if(!empty($value) && $value === $repeat){
            \Flash::instance()->addMessage('Password For User #' .$this->_id .' Set', 'warning');
            return \Bcrypt::instance()->hash($value,\Base::instance()->get('HASH'));
        } elseif(!empty($repeat)) {
            \Flash::instance()->addMessage('Password Change Failed. Please try again', 'warning');
        }
        return $this->password;
    }

    /**
     * Getter For Contact::fullname
     * @return string
     */
    function get_fullnames(){
        return ucwords($this->firstname . ' ' . $this->lastname);
    }
    function get_gravatar(){
        $userMail = $this->email;

        $imageWidth = '150'; //The image size

        $imgUrl = '//www.gravatar.com/avatar/'.md5($userMail).'fs='.$imageWidth;
        return $imgUrl;
    }
}