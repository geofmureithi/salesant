<?php
namespace Account\Models;


class Account extends Model {
    protected $table ="account";
    protected $fieldConf = array(
        'user' => array(
            'belongs-to-one' => '\Account\Models\User',
        ),
        'subdomain' => array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'expires' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
        ),
        'lastrenew' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
        ),
        'trial' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 0
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 0
        ),
        'creator' => array(
            'belongs-to-one' => '\Account\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );



}