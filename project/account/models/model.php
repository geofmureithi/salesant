<?php
namespace Account\Models;

class Model extends \DB\Cortex {
	protected
		$db = 'AccountDB',     // F3 hive key of a valid DB object
        $fluid = true,
        $ttl = 0,
		$table = '';


    /**
     *
     */
    static function install(){
		static::setup();
	}

    function __construct(){
		parent::__construct();
		$this->beforeinsert(function($self){
            if(!$self->creator)
			    $self->creator = \Base::instance()->get('SESSION.user_id');
		});

	}

}