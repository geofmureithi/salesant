<?php
namespace Account\Models;


class Support extends Model {
    protected $table ="support";
    protected $fieldConf = array(
        'title' => array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'slug' => array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'tags' => array(
            'type' => self::DT_JSON
        ),
		'description' => array(
            'type' => \DB\SQL\Schema::DT_TEXT
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 0
        ),
        'creator' => array(
            'belongs-to-one' => '\Account\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );



}