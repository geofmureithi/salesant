<?php
namespace Account\Models;

class UserType extends Model {
    protected
        $table ="user_types",
        $fieldConf = array(
        'code'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'title' => array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'users' => array(
            'has-many'=> array('\Account\Models\User','type')
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 0
        ),
        'creator' => array(
            'belongs-to-one' => '\Account\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );
    function __construct(){
        parent::__construct();
        $this->beforeerase(function($self){
            if(!count($self->users)){
                \Flash::instance()->addMessage('Usertype cannot be deleted since it has users tagged to it','warning');
                return false;
            }
        });
    }

}