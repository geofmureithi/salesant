<?php
namespace Account\Models;


class Bill extends Model {
    protected $table ="bill";
    protected $fieldConf = array(
        'user' => array(
            'belongs-to-one' => '\Account\Models\User',
        ),
        'amount' => array(
            'type' => \DB\SQL\Schema::DT_FLOAT,
        ),
		'description' => array(
            'type' => \DB\SQL\Schema::DT_TEXT
        ),
        'datedue' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
        ),
        'datepaid' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
        ),
        'paymentMethod' => array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 0
        ),
        'creator' => array(
            'belongs-to-one' => '\Account\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );



}