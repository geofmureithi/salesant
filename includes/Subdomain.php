<?php

/**
 * Created by PhpStorm.
 * User: geoff
 * Date: 4/8/16
 * Time: 11:40 AM
 */
defined("DOMAIN")
|| define("DOMAIN", 'crm.app');

class Subdomain extends \DB\Jig\Mapper
{
    public function __construct() {
        parent::__construct( new \DB\Jig('config/'),'subdomains.json' );
    }
    /**
     * @return bool
     */
    static function isMain(){
        if(!static::_handle() OR static::_handle() == 'www')
            return true;
    }

    /**
     * @return bool
     */
    static function isSub(){
        return !static::isMain();
    }

    /**
     * @return null|string
     */
    static function title(){
        if(static::isMain())
            return null;
        return strtolower(static::_handle());

    }

    /**
     * @return null|string
     */
    private static function _handle(){
        if (empty($_SERVER['SERVER_NAME'])) {

            return null;

        }

        $subDomain = substr($_SERVER['SERVER_NAME'], 0, -(strlen(DOMAIN)));

        if (empty($subDomain)) {

            return null;

        }

        return rtrim($subDomain, '.');
    }

    /**
     * @return bool
     */
    static function isRegistered(){
        if(static::isMain())
            return false;
        $subdomains = new static;
        return $subdomains->load(array('@subdomain = ?',static::title()));

    }

}