<?php


class DataTables extends \Prefab {

    protected $f3, $model;
	private $limit, $order, $offset, $order;

    function __construct(\BaseModel $model) {
        $this->f3 = \Base::instance();
		$this->model = $model;
		$this->request = $this->f3->get('POST');
		$bindings = array();
		$this->limit();
		$this->order( $this->request, $this->columns );
		$this->filter( $this->request, $this->columns, $bindings );
		//$this->start = $this->f3->get('POST.start');
		//$this->length = $this->f3->get('POST.length');
		//$this->search['value'] = $this->f3->get('POST.search.value');
		//$this->search['regex'] = $this->f3->get('POST.search.regex');
    }
	/**
	 * Paging
	 *
	 * Construct the LIMIT clause for server-side processing SQL query
	 *
	 *  @param  array $this->request Data sent to server by DataTables
	 *  @param  array $this->columns Column information array
	 *  @return string SQL limit clause
	 */
	private function limit ()
	{
		
		if ( isset($this->request['start']) && $this->request['length'] != -1 ) {
			$this->limit = $this->request['length'];
			$this->offset = $this->request['start'];
		}
		
	}
	/**
	 * Ordering
	 *
	 * Construct the ORDER BY clause for server-side processing SQL query
	 *
	 *  @param  array $this->request Data sent to server by DataTables
	 *  @param  array $this->columns Column information array
	 *  @return string SQL order by clause
	 */
	private function order ()
	{
		$order = '';
		if ( isset($this->request['order']) && count($this->request['order']) ) {
			$orderBy = array();
			$dtColumns = self::pluck( $this->columns, 'dt' );
			for ( $i=0, $ien=count($this->request['order']) ; $i<$ien ; $i++ ) {
				// Convert the column index into the column data property
				$columnIdx = intval($this->request['order'][$i]['column']);
				$this->requestColumn = $this->request['columns'][$columnIdx];
				$columnIdx = array_search( $this->requestColumn['data'], $dtColumns );
				$column = $this->columns[ $columnIdx ];
				if ( $this->requestColumn['orderable'] == 'true' ) {
					$dir = $this->request['order'][$i]['dir'] === 'asc' ?
						'ASC' :
						'DESC';
					$orderBy[] = '`'.$column['db'].'` '.$dir;
				}
			}
			$this->order = implode(', ', $orderBy);
		}
	}
	/**
	 * Searching / Filtering
	 *
	 * Construct the WHERE clause for server-side processing SQL query.
	 *
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here performance on large
	 * databases would be very poor
	 *
	 *  @param  array $this->request Data sent to server by DataTables
	 *  @param  array $this->columns Column information array
	 *  @param  array $bindings Array of values for PDO bindings, used in the
	 *    sql_exec() function
	 *  @return string SQL where clause
	 */
	static function filter (&$bindings )
	{
		$globalSearch = array();
		$this->columnsearch = array();
		$dtColumns = self::pluck( $this->columns, 'dt' );
		if ( isset($this->request['search']) && $this->request['search']['value'] != '' ) {
			$str = $this->request['search']['value'];
			for ( $i=0, $ien=count($this->request['columns']) ; $i<$ien ; $i++ ) {
				$this->requestColumn = $this->request['columns'][$i];
				$columnIdx = array_search( $this->requestColumn['data'], $dtColumns );
				$column = $this->columns[ $columnIdx ];
				if ( $this->requestColumn['searchable'] == 'true' ) {
					$binding = self::bind( $bindings, '%'.$str.'%', PDO::PARAM_STR );
					$globalSearch[] = "`".$column['db']."` LIKE ".$binding;
				}
			}
		}
		// Individual column filtering
		if ( isset( $this->request['columns'] ) ) {
			for ( $i=0, $ien=count($this->request['columns']) ; $i<$ien ; $i++ ) {
				$this->requestColumn = $this->request['columns'][$i];
				$columnIdx = array_search( $this->requestColumn['data'], $dtColumns );
				$column = $this->columns[ $columnIdx ];
				$str = $this->requestColumn['search']['value'];
				if ( $this->requestColumn['searchable'] == 'true' &&
				 $str != '' ) {
					$binding = self::bind( $bindings, '%'.$str.'%', PDO::PARAM_STR );
					$this->columnsearch[] = "`".$column['db']."` LIKE ".$binding;
				}
			}
		}
		// Combine the filters into a single string
		$where = '';
		if ( count( $globalSearch ) ) {
			$where = '('.implode(' OR ', $globalSearch).')';
		}
		if ( count( $this->columnsearch ) ) {
			$where = $where === '' ?
				implode(' AND ', $this->columnsearch) :
				$where .' AND '. implode(' AND ', $this->columnsearch);
		}
		if ( $where !== '' ) {
			$where = 'WHERE '.$where;
		}
		$this->where = $where;
	}
	/**
	 * Perform the SQL queries needed for an server-side processing requested,
	 * utilising the helper functions of this class, limit(), order() and
	 * filter() among others. The returned array is ready to be encoded as JSON
	 * in response to an SSP request, or can be modified if needed before
	 * sending back to the client.
	 *
	 *  @param  array $this->request Data sent to server by DataTables
	 *  @param  array|PDO $conn PDO connection resource or connection parameters array
	 *  @param  string $table SQL table to query
	 *  @param  string $primaryKey Primary key of the table
	 *  @param  array $this->columns Column information array
	 *  @return array          Server-side processing response array
	 */
	static function fetch ()
	{
		
		// Main query to actually get the data
		$data = $this->model->find(array($this->where),array('limit'=>$this->limit,'order'=>$this->order, 'offset' =>$this->offset ));

		$recordsFiltered = $data->count();
		// Total data set length
		
		$recordsTotal  = $this->model->count();
		/*
		 * Output
		 */
		return array(
			"draw"            => isset ( $this->request['draw'] ) ?
				intval( $this->request['draw'] ) :
				0,
			"recordsTotal"    => intval( $recordsTotal ),
			"recordsFiltered" => intval( $recordsFiltered ),
			"data"            => self::data_output( $this->columns, $data )
		);
	}
	/**
	 * The difference between this method and the `simple` one, is that you can
	 * apply additional `where` conditions to the SQL queries. These can be in
	 * one of two forms:
	 *
	 * * 'Result condition' - This is applied to the result set, but not the
	 *   overall paging information query - i.e. it will not effect the number
	 *   of records that a user sees they can have access to. This should be
	 *   used when you want apply a filtering condition that the user has sent.
	 * * 'All condition' - This is applied to all queries that are made and
	 *   reduces the number of records that the user can access. This should be
	 *   used in conditions where you don't want the user to ever have access to
	 *   particular records (for example, restricting by a login id).
	 *
	 *  @param  array $this->request Data sent to server by DataTables
	 *  @param  array|PDO $conn PDO connection resource or connection parameters array
	 *  @param  string $table SQL table to query
	 *  @param  string $primaryKey Primary key of the table
	 *  @param  array $this->columns Column information array
	 *  @param  string $whereResult WHERE condition to apply to the result set
	 *  @param  string $whereAll WHERE condition to apply to all queries
	 *  @return array          Server-side processing response array
	 */
	
	static function fatal ( $msg )
	{
		echo json_encode( array( 
			"error" => $msg
		) );
		exit(0);
	}
	/**
	 * Create a PDO binding key which can be used for escaping variables safely
	 * when executing a query with sql_exec()
	 *
	 * @param  array &$a    Array of bindings
	 * @param  *      $val  Value to bind
	 * @param  int    $type PDO field type
	 * @return string       Bound key to be used in the SQL where this parameter
	 *   would be used.
	 */
	static function bind ( &$a, $val, $type )
	{
		$key = ':binding_'.count( $a );
		$a[] = array(
			'key' => $key,
			'val' => $val,
			'type' => $type
		);
		return $key;
	}
	/**
	 * Pull a particular property from each assoc. array in a numeric array, 
	 * returning and array of the property values from each item.
	 *
	 *  @param  array  $a    Array to get data from
	 *  @param  string $prop Property to read
	 *  @return array        Array of property values
	 */
	static function pluck ( $a, $prop )
	{
		$out = array();
		for ( $i=0, $len=count($a) ; $i<$len ; $i++ ) {
			$out[] = $a[$i][$prop];
		}
		return $out;
	}
	/**
	 * Return a string from an array or a string
	 *
	 * @param  array|string $a Array to join
	 * @param  string $join Glue for the concatenation
	 * @return string Joined string
	 */
	static function _flatten ( $a, $join = ' AND ' )
	{
		if ( ! $a ) {
			return '';
		}
		else if ( $a && is_array($a) ) {
			return implode( $join, $a );
		}
		return $a;
	}
	  

}