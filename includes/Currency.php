<?php

/**
 * Created by PhpStorm.
 * User: geoff
 * Date: 3/10/16
 * Time: 11:19 AM
 */
class Currency extends \Prefab
{
    static function format($number){
        return number_format($number,2);
    }
}