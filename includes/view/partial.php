<?php
/**
 * Created by PhpStorm.
 * User: Ace
 * Date: 30/07/2015
 * Time: 15:57
 */
namespace View;

class Partial extends Base {

    public function reroute($url){
        echo json_encode(array('url'=> $url));
        die;
    }
    public function setTemplate($filepath){
        $this->template = $filepath;
    }
    public function render() {
        $data= array('title' => \Base::instance()->get('page.title'), 'html'=> '<span id="crumb" data-title="' . \Base::instance()->get('crumb') .'"></span>' . \Template::instance()->render('flash.html').\Template::instance()->render(\Base::instance()->get('INNER.PAGE')));
        \Flash::instance()->clearMessages();
        return $data['html'];
    }

}