<?php

use Application\Models\User;
class Notifications extends Prefab
{
    private $model;
    function __construct()
    {
       $this->model = new \Application\Models\Notification();
    }

    /**
     * Add Notification to DB
     * @param User $user
     * @param $title
     * @param $message
     * @param $link
     * @param bool $email
     * @return bool
     */
    function add(User $user, $title, $message, $link, $email = false){

        $this->model->reset();
        $this->model->creator = $user;
        $this->model->title = $title;
        $this->model->link = $link;
        $this->model->message = $message;
        $status = $this->model->save();
        if($email){
            $f3 = Base::instance();
            $this->model->copyto('notification',2);
            $mailer = Mailer::instance();
            $f3->set('UI','project/application/views/');
            $status = $status && $mailer->send('Notification: '. $this->model->title,$user->email,Template::instance()->render('notification.html'));
        }
       return $status ;

    }

    /**
     * Open a notification
     * @param \Application\Models\Notification $notification
     * @return bool
     */
    static function open(\Application\Models\Notification $notification){
        return $notification->opened();
    }
}