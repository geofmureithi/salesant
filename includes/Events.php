<?php

/**
 * Created by PhpStorm.
 * User: geoff
 * Date: 5/31/16
 * Time: 12:29 PM
 */
class Events
{
    public static function listen($hook, $action){
        $fw = \Base::instance();
        if(!$fw->exists('event.'. $hook))
            $fw->set('event.'. $hook,array($action) );
        else
            $fw->merge('event.'. $hook, array($action));
    }

    public static function trigger ($hook){
        $fw = \Base::instance();
        $actions = $fw->get('event.'. $hook);

        if(is_array($actions))

            foreach($actions as $action){
                if(is_array($action))
                    $fw->call(@$action['callable']?$action['callable']:$action,@$action['args']?$action['args']:null,@$action['hooks']?$action['hooks']:'');
                else
                    $fw->call($action);
            }
        return true;
    }

}