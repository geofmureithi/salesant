<?php

class BaseController {
    public $view;
    protected $limit = 10;
    function beforeroute($f3,$params) {
		//code here;
        //\Admin\Models\Setting::reload($f3); // Load saved settings
        $this->page = \Pagination::findCurrentPage();
        $this->option = array('order' => 'datecreated DESC');
        $this->filter = null;
        $ajax = Base::instance()->get('AJAX');

        \Template::instance()->extend('pagebrowser','\Pagination::renderTag');

        if(!$ajax) {
            $this->view = new \View\Html();
        } else {
            $this->view = new \View\JSON();
            if($f3->get('HEADERS.Partial') OR $f3->get('HEADERS.X-Pjax'))
                $this->view = new \View\Partial();
        }
	}
	function afterroute($f3,$params) {
		//code here;
        echo $this->view->render();
	}
}