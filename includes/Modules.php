<?php

/**
 * Created by PhpStorm.
 * User: geoff
 * Date: 4/5/16
 * Time: 11:43 AM
 */
final class Modules{
    /*
     * Module: For Modulating
     * Modules:
     *      -Basic (All required)
     */
    const BASIC = "Basic";
    const PRODUCTS = "Products";
    const LEADS = "Leads";
    const OPPORTUNITIES = "Opportunities";
    const NEWSLETTERS = "Newsletters";
    const SALES = "Sales";
    const EMAILS = "Email Integration";
}