<?php

/**
 * Created by PhpStorm.
 * User: geoff
 * Date: 3/10/16
 * Time: 11:19 AM
 */
class Customlang extends \Prefab
{
    static function process($text, $organisation = null){
        $f3= Base::instance();
        $cache = Cache::instance();
        $organisation?$organisation:$organisation=$f3->get('SESSION.organisation');
        $replacements = $cache->get('customlang.'.$organisation)?$cache->get('customlang.'.$organisation):$f3->get('CUSTOMLANG');
        $search = array();
        $replace = array();

        foreach($f3->get('CUSTOMLANG') as $key=> $value){
            $search[] = '/'.$value.'$/i';
            $replace[] = $replacements['customlang.'.$organisation.'.'.$key];
        }
        $newText = preg_replace($search,$replace, $text);
        return $newText?$newText:$text;


    }
}