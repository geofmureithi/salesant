<?php
/**
 * Created by PhpStorm.
 * User: Ace
 * Date: 11/10/2015
 * Time: 13:35
 */

class Mailer extends Prefab {
    public $smtp;
    function __construct(){
        $f3= Base::instance();
        $this->smtp = new SMTP( $f3->get('smtp.host'), $f3->get('smtp.port'), $f3->get('smtp.scheme'), $f3->get('smtp.user'),$f3->get('smtp.password'));
        $this->smtp->set('Errors-to', '<'. $f3->get('site.errors_email').'>');
        $this->smtp->set('From','"'. $f3->get('site.title'). '" <'. $f3->get('site.email').'>');
    }
    function send($subject, $to, $message, $attachments = array()){
        foreach($attachments as $attachment)
            $this->smtp->attach( $attachment);
        $this->smtp->set('To', '<'. $to .'>');
        $this->smtp->set('Subject', $subject);
        $this->smtp->send($message);
    }


} 