<!-- Task Form Popup -->
    <div class="panel popup-basic">
        <div class="panel-heading">
            <span class="panel-title">
                <i class="fa fa-building"></i><?php echo $page['title']; ?>
            </span>
        </div>
        <form method="post" action="<?php echo $form['action']; ?>" class="ajaxable">
            <div class="panel-body">

                <div class="section">
                    <div class="form-group">
                        <label for="task_title" class="control-label">Task Title</label>
                        <div class="bs-component">
                            <input type="text" name="task[title]" id="task_title" class="form-control" placeholder="Follow up" required value="<?php echo isset($POST['task']['title'])?$POST['task']['title']:''; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="task_duedate" class="control-label">Due Date</label>
                        <div class="bs-component">
                            <div class="input-group date" id="task_duedate">
                                <span class="input-group-addon cursor">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input type="text" name="task[datedue]" class="form-control" readonly required style="background-color: #fff !important; cursor: text !important;" value="<?php echo isset($POST['task']['datedue'])?$POST['task']['datedue']:''; ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="task_assignee" class="control-label">Assign to</label>
                        <div class="bs-component">
                            <select name="task[assignee]" class="select2-single form-control">
                            <?php foreach($assignees as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['task']['assignee']) && $POST['task']['assignee']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                        </div>
                    </div>
                </div>
                <!-- end section -->

            </div>
            <!-- end .form-body section -->

            <div class="panel-footer">
                <?php if ($PARAMS['mothercontroller']): ?>
                    
                        <input type="hidden" name="task[lead]" value="<?php echo $PARAMS['motherid']; ?>" />
                <?php endif; ?>

                <button type="submit" class="btn btn-sm btn-primary">Submit</button>
            </div>
            <!-- end .form-footer section -->
        </form>
    </div>
    <!-- end: .panel -->
<!-- end: .task-form -->

