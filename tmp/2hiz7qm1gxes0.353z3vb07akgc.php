
<div class="row">

    <div class="col-md-12">

        <h1 class="mtn">Account</h1>

        <!-- Account Form -->
        <div class="panel">
            <div class="panel-heading">
                <span class="panel-title">Profile</span>
            </div>
            <div class="panel-body">
                <div id="profile-results"></div>
                <form autocomplete="off" action="/account/profile?change=profile" class="form-horizontal" id="profile-form" role="form" method="post" accept-charset="utf-8">
                    <div class="form-group">
                        <label for="first_name" class="col-lg-3 control-label">First Name</label>
                        <div class="col-lg-8">
                            <div class="bs-component">
                                <input type="text" name="user[firstname]" id="first_name" class="form-control" value="<?php echo isset($POST['user']['firstname'])?$POST['user']['firstname']:''; ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="last_name" class="col-lg-3 control-label">Last Name</label>
                        <div class="col-lg-8">
                            <div class="bs-component">
                                <input type="text" name="user[lastname]" id="last_name" class="form-control" value="<?php echo isset($POST['user']['lastname'])?$POST['user']['lastname']:''; ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phone" class="col-lg-3 control-label">Phone</label>
                        <div class="col-lg-8">
                            <div class="bs-component">
                                <input type="text" name="user[telephone]" id="phone" class="form-control" value="<?php echo isset($POST['user']['telephone'])?$POST['user']['telephone']:''; ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phone" class="col-lg-3 control-label">Profile Image</label>
                        <div class="col-lg-8">
                            <div class="bs-component">
                                <img class="img-responsive thumbnail mr15" src="<?php echo $user->gravatar; ?>" align="left">
                                You can change your profile image at <a href="http://www.gravatar.com/" target="_blank">Gravatar.com</a>, using the email address listed below.
                            </div>
                        </div>
                    </div>
                    <?php if ($POST['user']): ?>
                        
                            <div class="form-group">
                                <label for="apiKey" class="col-sm-3 control-label">API Key</label>
                                <div class="col-sm-9">
                                    <p id="apiKey" class="well well-sm"><?php echo UrlEncryption::instance()->encode($POST['user']['_id']); ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="apiSecret" class="col-sm-3 control-label">API Secret</label>
                                <div class="col-sm-9">
                                    <p id="apiSecret" class="well well-sm"><?php echo UrlEncryption::instance()->encode($POST['user']['password']); ?></p>
                                </div>
                            </div>

                        
                    <?php endif; ?>
                    <div class="form-group">
                        <div class="col-lg-8 col-lg-offset-3">
                            <div class="bs-component">
                                <button type="submit" class="btn btn-sm btn-primary btn-gradient dark">Save profile</button>
                            </div>
                        </div>
                    </div>
                </form>                </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <span class="panel-title">Change Email Address</span>
            </div>
            <div class="panel-body">
                <div id="email-results"></div>
                <p>Set the email address you use to log in</p>
                <form autocomplete="off" action="/account/profile?change=email" class="form-horizontal" id="email-form" role="form" method="post" accept-charset="utf-8">
                    <div class="form-group">
                        <label for="password" class="col-lg-3 control-label">Current Password</label>
                        <div class="col-lg-8">
                            <div class="bs-component">
                                <input type="password" name="password" id="password" autocomplete="off" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-lg-3 control-label">Email</label>
                        <div class="col-lg-8">
                            <div class="bs-component">
                                <input type="email" name="user[email]" id="email" class="form-control" autocomplete="off" value="<?php echo isset($POST['user']['email'])?$POST['user']['email']:''; ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-8 col-lg-offset-3">
                            <div class="bs-component">
                                <button type="submit" class="btn btn-sm btn-primary btn-gradient dark">Change email address</button>
                            </div>
                        </div>
                    </div>
                </form>                </div>
        </div>
        <div class="panel">
            <div class="panel-heading">
                <span class="panel-title">Change Password</span>
            </div>
            <div class="panel-body">
                <div id="password-results"></div>
                <p>Set the password you use to log in</p>
                <form autocomplete="off" action="/account/profile?change=password" class="form-horizontal" id="password-form" role="form" method="post" accept-charset="utf-8">
                    <div class="form-group">
                        <label for="old" class="col-lg-3 control-label">Current Password</label>
                        <div class="col-lg-8">
                            <div class="bs-component">
                                <input type="password" name="old" id="old" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="new" class="col-lg-3 control-label">New Password</label>
                        <div class="col-lg-8">
                            <div class="bs-component">
                                <input type="password" name="new" id="new" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="new_confirm" class="col-lg-3 control-label">Retype Password</label>
                        <div class="col-lg-8">
                            <div class="bs-component">
                                <input type="password" name="new_confirm" id="new_confirm" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-8 col-lg-offset-3">
                            <div class="bs-component">
                                <button type="submit" class="btn btn-sm btn-primary btn-gradient dark">Change password</button>
                            </div>
                        </div>
                    </div>
                </form>                </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <span class="panel-title">SMTP Settings</span>
            </div>
            <div class="panel-body">
                <p>Set the settings to send emails</p>
                <form autocomplete="off" action="/account/profile?change=smtp" class="form-horizontal" id="smtp-form" role="form" method="post" accept-charset="utf-8">
                    <div class="form-group">
                        <label for="server" class="col-lg-3 control-label">Server Name</label>
                        <div class="col-lg-7">
                            <div class="bs-component">
                                <input type="text" name="smtp[server]" id="server" class="form-control" autocomplete="off" placeholder="SMTP Server" value="<?php echo isset($POST['smtp']['server'])?$POST['smtp']['server']:''; ?>" />
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="bs-component">
                                <input type="text" name="smtp[port]" id="port" class="form-control" autocomplete="off" placeholder="SMTP Port" value="<?php echo isset($POST['smtp']['port'])?$POST['smtp']['port']:''; ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="uname" class="col-lg-3 control-label">Username</label>
                        <div class="col-lg-8">
                            <div class="bs-component">
                                <input type="text" name="smtp[username]" id="uname" class="form-control" autocomplete="off" value="<?php echo isset($POST['smtp']['username'])?$POST['smtp']['username']:''; ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pword" class="col-lg-3 control-label">Password</label>
                        <div class="col-lg-8">
                            <div class="bs-component">
                                <input type="text" onfocus="this.type='password'" name="smtp[password]" value="<?php echo $POST['smtp']['password']; ?>" id="pword" class="form-control" autocomplete="off" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-8 col-lg-offset-3">
                            <div class="bs-component">
                                <button type="submit" class="btn btn-sm btn-primary btn-gradient dark">Save Settings</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="panel">
            <div class="panel-heading">
                <span class="panel-title">IMAP Settings</span>
            </div>
            <div class="panel-body">
                <p class="well">Set the settings to recieve emails
                Struggling with the Gmail Imap/pop connection? It keeps saying too many failed login connections were made?

                    Head To:
                    <a href="https://www.google.com/settings/security/lesssecureapps">https://www.google.com/settings/security/lesssecureapps</a>
                    <br/>
                    Click "Turn on"
                </p>
                <form autocomplete="off" action="/account/profile?change=imap" class="form-horizontal" id="imap-form" role="form" method="post" accept-charset="utf-8">
                    <div class="form-group">
                        <label for="imap-server" class="col-lg-3 control-label">Server Name</label>
                        <div class="col-lg-7">
                            <div class="bs-component">
                                <input type="text" name="imap[server]" id="imap-server" autocomplete="off" placeholder="IMAP Server" class="form-control" value="<?php echo isset($POST['imap']['server'])?$POST['imap']['server']:''; ?>" />
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="bs-component">
                                <input type="text" name="imap[port]" id="imap-port" autocomplete="off" class="form-control" placeholder="IMAP Port" value="<?php echo isset($POST['imap']['port'])?$POST['imap']['port']:''; ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="imap-uname" class="col-lg-3 control-label">Username</label>
                        <div class="col-lg-8">
                            <div class="bs-component">
                                <input type="text" name="imap[username]" id="imap-uname" autocomplete="off" class="form-control" value="<?php echo isset($POST['imap']['username'])?$POST['imap']['username']:''; ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="imap-pword" class="col-lg-3 control-label">Password</label>
                        <div class="col-lg-8">
                            <div class="bs-component">
                                <input type="text" onfocus="this.type='password'" name="imap[password]" id="imap-pword" autocomplete="off" class="form-control" value="<?php echo isset($POST['imap']['password'])?$POST['imap']['password']:''; ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-8 col-lg-offset-3">
                            <div class="bs-component">
                                <button type="submit" class="btn btn-sm btn-primary btn-gradient dark">Save Settings</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- end: Account Form -->
    </div>
</div>