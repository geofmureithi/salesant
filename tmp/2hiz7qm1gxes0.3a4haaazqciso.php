<div class="col-md-10 col-sm-8">
    <div class="panel panel-visible" id="spy4">
        <div class="panel-heading">
            <div class="panel-title hidden-xs">
                <span class="glyphicon glyphicon-shopping-cart"></span><?php echo $page['title']; ?></div>
        </div>
        <div class="panel-menu"></div>
        <div class="panel-body pn">
            <table class="table table-striped table-hover display" id="datatable6" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Client Name</th>
                    <th><?php echo Customlang::process('Products'); ?></th>
                    <th>Amount</th>
                    <th>Creator</th>
                    <th>Created</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (($list?:array()) as $sale): ?>
                    <tr>
                        <td><?php echo $sale->contact->fullnames; ?></td>
                        <td><?php echo $sale->products['0']->title; ?> <?php echo $sale->products?count($sale->products):0> 1?'+'.count($sale->products)-1:''; ?></td>
                        <td><?php echo Currency::format($sale->amount); ?></td>
                        <td><?php echo $sale->creator->fullnames; ?></td>
                        <td><time class="cw-relative-date" datetime="<?php echo $sale->datecreated; ?>"></time></td>
                        <td><a href="/sales/view/<?php echo $sale->id; ?>" >View</a></td>
                    </tr>
                <?php endforeach; ?>


                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="col-md-2 text-center">
    <div class="clearfix"><br/></div>
    <div class="panel panel-tile text-center">
        <div class="panel-body bg-info">
            <h1 class="fs35 mbn"><?php echo $list?count($list):0; ?></h1>
            <h6 class="text-white">SALE(s)</h6>
        </div>
        <div class="panel-footer br-n p12">
    <span class="fs11">
      <i class="fa fa-arrow-up text-info pr5"></i>
      <b></b>
    </span>
        </div>
    </div>
</div>

