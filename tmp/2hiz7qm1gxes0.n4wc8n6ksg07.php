<div class="col-md-12">
    <div class="panel panel-visible" id="spy4">
        <div class="panel-heading">
            <div class="panel-title">
                <span class="glyphicon glyphicon-tasks"></span><?php echo $page['title']; ?></div>
        </div>
        <div class="panel-menu text-right">
            <a href="/leads/add" class="btn btn-sm btn-alert ajax-modal"> <i class="fa fa-plus-circle"></i> Add Lead</a>
        </div>
        <div class="panel-body pn">
            <table class="table table-striped table-hover table-responsive" id="datatable" cellspacing="0">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Status</th>
                    <th class="hidden-sm hidden-xs">Source</th>
                    <th>Contacts</th>
                    <th class="hidden-sm">Creator</th>
                    <th class="hidden-sm">Created</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php $i=0; foreach (($list?:array()) as $lead): $i++; ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $lead->title; ?></td>
                        <td><?php echo $lead->status->title; ?></td>
                        <td class="hidden-sm hidden-xs"><?php echo $lead->source->title; ?></td>
                        <td><?php echo @$lead->contacts['0']->fullnames; ?></td>
                        <td class="hidden-sm"><?php echo $lead->creator->fullnames; ?></td>
                        <td class="hidden-sm"><time class="cw-relative-date" datetime="<?php echo $lead->datecreated; ?>"></time></td>
                        <td>
                            <a data-toggle="tooltip" title="View <?php echo Customlang::process('Lead'); ?>" href="/leads/view/<?php echo $lead->id; ?>" class="iconLink" ><i class="fa fa-search-plus"></i> View</a>
                            <a data-toggle="tooltip" data-type="<?php echo Customlang::process('Lead'); ?>" title="Delete <?php echo Customlang::process('Lead'); ?>" class="delete iconLink" href="/leads/delete/<?php echo $lead->id; ?>" ><i class="fa fa-remove"></i> Delete</a>

                        </td>
                    </tr>
                <?php endforeach; ?>


                </tbody>
            </table>
        </div>
    </div>
</div>