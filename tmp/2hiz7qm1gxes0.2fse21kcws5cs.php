<div class="alert alert-system alert-dismissable mb30">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h3 class="mt5">Approve This <?php echo Customlang::process('Quotation'); ?>?</h3>
    <p><i class="fa fa-user"></i> Ensure The Client Details (<?php echo $quotation->opportunity->contact->fullnames; ?>) are Ok.</p><br/>
    <p><i class="fa fa-money"></i> Confirm This Payment Plan<br/>
        <table class="table table-condensed">
    <tr>
        <th class="">Date</th>
        <th class="">Amount</th>
        <th class="">Description</th>
    </tr>
        <?php foreach (($quotation->paymentPlan?:array()) as $payment): ?>
            <tr>
                <td class="text-left"><?php echo $payment->datedue; ?></td>
                <td class=""><?php echo Currency::format($payment->amount); ?></td>
                <td class=""><?php echo $payment->description; ?></td>
            </tr>
        <?php endforeach; ?>
        </table>
    </p>
    <br/>
    <p><i class="fa fa-check-square-o"></i> Ensure The <?php echo Customlang::process('Products'); ?> details are OK </p>
    <br>
    <p>
        <form action="/opportunity/<?php echo $quotation->opportunity->id; ?>/quotations/approve/<?php echo $quotation->id; ?>" method="post">
            <input type="hidden" name="approveKey" value="<?php echo isset($POST['approveKey'])?$POST['approveKey']:''; ?>" />
            <button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Approve</button>
            <a class="btn btn-primary" href="/opportunity/<?php echo $quotation->opportunity->id; ?>/quotations/edit/<?php echo $quotation->id; ?>">Edit</a>
            <a class="btn btn-system" href="/leads/view/<?php echo $quotation->opportunity->lead->id; ?>">Cancel</a>

</form>
            </p>
</div>
<?php echo $this->render('/quotations/view.html',$this->mime,get_defined_vars()); ?>