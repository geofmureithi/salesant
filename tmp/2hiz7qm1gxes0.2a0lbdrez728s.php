<div class="panel popup popup-basic popup-lg">
    <div class="panel-heading">
         <span class="panel-icon">
                      <i class="fa fa-user-plus"></i>
                    </span>
        <span class="panel-title"> <?php echo $page['title']; ?></span>
    </div>
    <div class="panel-body">
        <form id="usertype-form" method="post" action="<?php echo $form['action']; ?>" data-parsley-validate>
            <div class="row general-fc">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Name</label>
                        <input required class="form-control" name="usertype[title]" placeholder="eg Admin" type="text" value="<?php echo isset($POST['usertype']['title'])?$POST['usertype']['title']:''; ?>" />
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea name="usertype[description]" class="form-control" placeholder="Description: Max 50 words" cols="86" rows="5"><?php echo @$POST['usertype']['description']; ?></textarea>
                    </div>
                </div>
            </div>


            <div class="row">
                <div>
                    <div class="col-md-6">
                        <button class="btn blue-btn-check pull-left">Save and Continue</button></div>
                </div>
            </div>
        </form>
    </div>

</div>

