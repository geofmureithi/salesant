<div class="row panel">
    <form method="post" action="<?php echo $form['action']; ?>">
    <div class="panel-heading">
        <ul class="nav panel-tabs-border panel-tabs panel-tabs-left">
            <li class="active">
                <a href="#tab2_1" data-toggle="tab">Organisation Details</a>
            </li>
            <li>
                <a href="#tab2_2" data-toggle="tab">Quote Template</a>
            </li>
            <li>
                <a href="#tab2_3" data-toggle="tab">Invoice Template</a>
            </li>
            <li>
                <a href="#tab2_4" data-toggle="tab">Receipt Template</a>
            </li>
            <li>
                <a href="#tab2_5" data-toggle="tab">Settings</a>
            </li>
        </ul>
    </div>
    <div class="panel-body">
        <div class="tab-content pn br-n">
            <div id="tab2_1" class="tab-pane active">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name" class="control-label">Organisation Name</label>
                            <div class="bs-component">
                                <input type="text" name="organisation[name]" id="name" class="form-control" value="<?php echo isset($POST['organisation']['name'])?$POST['organisation']['name']:''; ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="telephone" class="control-label">Tel</label>
                            <div class="bs-component">
                                <input type="text" name="organisation[telephone]" id="telephone" class="form-control" value="<?php echo isset($POST['organisation']['telephone'])?$POST['organisation']['telephone']:''; ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="email" class="control-label">Email</label>
                            <div class="bs-component">
                                <input type="text" name="organisation[email]" id="email" class="form-control" value="<?php echo isset($POST['organisation']['email'])?$POST['organisation']['email']:''; ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="website" class="control-label">Website</label>
                            <div class="bs-component">
                                <input type="text" name="organisation[website]" id="website" class="form-control" value="<?php echo isset($POST['organisation']['website'])?$POST['organisation']['website']:''; ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="contact" class="control-label">Choose a Contact Person</label>
                        <div class="bs-component">
                            <select name="organisation[contact]" id="contact" class="select2-single form-control">
                            <?php foreach($contacts as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['organisation']['contact']) && $POST['organisation']['contact']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="postaladdress" class="control-label">Postal Address</label>
                        <div class="bs-component">
                            <textarea  name="organisation[postalAddress]" id="postaladdress" class="form-control"><?php echo @$POST['organisation']['postalAddress']; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="physicaladdress" class="control-label">Physical Address</label>
                        <div class="bs-component">
                            <textarea  name="organisation[physicalAddress]" id="physicaladdress" class="form-control smallnote"><?php echo @$POST['organisation']['physicalAddress']; ?></textarea>
                        </div>
                    </div>

                </div>
            </div>
            <div id="tab2_2" class="tab-pane">
                <div class="row">
                    <div class="section col-md-12">
                        <div class="form-group">
                            <div class="bs-component dropzone" id="quotation">

                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab2_3" class="tab-pane">
                <div class="row">
                    <div class="section col-md-12">
                        <div class="form-group">
                            <div class="bs-component dropzone" id="invoice">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab2_4" class="tab-pane">
                <div class="row">
                    <div class="section col-md-12">
                        <div class="form-group">
                            <div class="bs-component dropzone" id="receipt">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab2_5" class="tab-pane">
                <div class="row">
                    <div class="section col-md-12">
                        <div class="form-group">
                            <label class="control-label">Export Options</label>
                            <div class="radio-custom mb10">
                                <input type="radio" id="export" value="pdf" name="organisation[settings][export]" <?php echo (isset($POST['organisation']['settings']['export']) && $POST['organisation']['settings']['export']=='pdf')?'checked="checked"':''; ?> />
                                <label for="export">Export <?php echo Customlang::process('Quotation'); ?> as PDF</label>
                            </div>
                            <div class="radio-custom mb10">
                                <input type="radio" id="export2" value="pdf" name="organisation[settings][export]" <?php echo (isset($POST['organisation']['settings']['export']) && $POST['organisation']['settings']['export']=='pdf')?'checked="checked"':''; ?> />
                                <label for="export2">Export <?php echo Customlang::process('Quotation'); ?> as Word</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Payment Terms</label>
                            <div class="radio-custom mb10">
                                <input type="radio" id="payment" value="single" name="organisation[settings][payment]" <?php echo (isset($POST['organisation']['settings']['payment']) && $POST['organisation']['settings']['payment']=='single')?'checked="checked"':''; ?> />
                                <label for="payment">Single Payment</label>
                            </div>
                            <div class="radio-custom mb10">
                                <input type="radio" id="payment2" value="multiple" name="organisation[settings][payment]" <?php echo (isset($POST['organisation']['settings']['payment']) && $POST['organisation']['settings']['payment']=='multiple')?'checked="checked"':''; ?> />
                                <label for="payment2">Multiple Payments</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label"><?php echo Customlang::process('Products'); ?> Per <?php echo Customlang::process('Quotation'); ?></label>
                            <div class="radio-custom mb10">
                                <input type="radio" id="products" value="single" name="organisation[settings][products]" <?php echo (isset($POST['organisation']['settings']['products']) && $POST['organisation']['settings']['products']=='single')?'checked="checked"':''; ?> />
                                <label for="products">Single <?php echo Customlang::process('Product'); ?> </label>
                            </div>
                            <div class="radio-custom mb10">
                                <input type="radio" id="products2" value="multiple" name="organisation[settings][products]" <?php echo (isset($POST['organisation']['settings']['products']) && $POST['organisation']['settings']['products']=='multiple')?'checked="checked"':''; ?> />
                                <label for="products2">Multiple <?php echo Customlang::process('Products'); ?> </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Show Quantity</label>
                            <div class="radio-custom mb10">
                                <input type="radio" id="qty" value="no" name="organisation[settings][qty]" <?php echo (isset($POST['organisation']['settings']['qty']) && $POST['organisation']['settings']['qty']=='no')?'checked="checked"':''; ?> />
                                <label for="qty">No (QTY = 1)</label>
                            </div>
                            <div class="radio-custom mb10">
                                <input type="radio" id="qty2" value="yes" name="organisation[settings][qty]" <?php echo (isset($POST['organisation']['settings']['qty']) && $POST['organisation']['settings']['qty']=='yes')?'checked="checked"':''; ?> />
                                <label for="qty2">Yes (Allows selling of more than 1 <?php echo Customlang::process('Product'); ?> in 1 <?php echo Customlang::process('Quotation'); ?>) </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- end .form-footer section -->
    </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
        </div>
    </form>
</div>
<?php $quotationTemplate=$fileDetails($POST['organisation']['quoteTemplate']); ?>
<?php $invoiceTemplate=$fileDetails($POST['organisation']['invoiceTemplate']); ?>
<?php $receiptTemplate=$fileDetails($POST['organisation']['receiptTemplate']); ?>
<script>
    $(function(){
        $(".labelauty").labelauty();
            function mocKFile(dropzoneId, $file){
                if(!$file.size)
                return false;
                var $dropzone = Dropzone.forElement(dropzoneId);
                // Create the mock file:
                var mockFile = { name: $file.name, size: $file.size };
                var mockRes = { success: true, fileName: $file.name, size: $file.size };

                // Call the default addedfile event handler
                $dropzone.emit("addedfile", mockFile);

                // Make sure that there is no progress bar, etc...
                $dropzone.emit("success", mockFile,mockRes);
                $dropzone.emit("complete", mockFile);

                // If you use the maxFiles option, make sure you adjust it to the
                // correct amount:
                //var existingFileCount = 1; // The number of files already uploaded
                //$dropzone.options.maxFiles = $dropzone.options.maxFiles - existingFileCount;
                return true;
            }
        $(window).load(function(){
           var quotationTemplate = { name: "<?php echo $quotationTemplate->name; ?>", size: <?php echo $quotationTemplate->size?$quotationTemplate->size:0; ?> };
           var invoiceTemplate = { name: "<?php echo $invoiceTemplate->name; ?>", size: <?php echo $invoiceTemplate->size?$invoiceTemplate->size:0; ?> };
           var receiptTemplate = { name: "<?php echo $receiptTemplate->name; ?>", size: <?php echo $receiptTemplate->size?$receiptTemplate->size:0; ?> };
            mocKFile('#quotation',quotationTemplate);
            mocKFile('#invoice',invoiceTemplate);
            mocKFile('#receipt',receiptTemplate);
        })


    });
</script>

