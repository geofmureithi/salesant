    <div id="content" class="table-layout animated fadeIn">
        <!-- begin: .tray-right -->
        <?php echo $this->render('organisations/aside.html',$this->mime,get_defined_vars()); ?>
        <!-- end: .tray-right -->
        <!-- begin: .tray-center -->
        <div class="tray tray-center">
            <div class="row">
                <div class="col-md-9">
                    <form action="/organisations/customlang/<?php echo $org->id; ?>" method="post">
                    <div class="panel">
                        <div class="panel-heading">
                            <span class="panel-title">Customize Organisation Terminology</span>
                        </div>
                        <div class="panel-body pn">

                            <div class="bs-component">

                                <table class="table table-striped">
                                    <tbody>
                                    <?php foreach (($CUSTOMLANG?:array()) as $iKey=>$lang): ?>
                                        <tr>
                                            <td><?php echo $lang; ?></td>
                                            <td class="text-right">
                                             <input class="form-control" type="text" name="lang[<?php echo $iKey; ?>]" value="<?php echo Customlang::process($lang, $org->id); ?>" />
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>

                                    </tbody>
                                </table>



                            </div>
                        </div>
                        <div class="panel-footer block text-right">
                            <button class="btn btn-lg btn-success " type="submit">Save</button>
                        </div>

                    </div>
                    </form>
                </div>

            </div>
        </div>
        <!-- end: .tray-center -->


