<!-- Contact Form Popup -->
<div class="clearfix"></div>
<div id="customfields-form" class="popup-basic popup-lg">
    <div class="panel">
        <div class="panel-heading">
            <span class="panel-title">
                <i class="fa fa-apple"></i><?php echo $page['title']; ?>
            </span>
        </div>
        <!-- end .panel-heading section -->
        <div id="contact-results"></div>
        <form method="post" action="<?php echo $form['action']; ?>" class="ajaxable">
            <div class="panel-body p25">

                <div class="section">
                    <div class="form-group col-md-12">
                        <label for="title" class="control-label">Name</label>
                        <div class="bs-component">
                            <input type="text" name="customfield[name]" id="title" class="form-control" required value="<?php echo isset($POST['customfield']['name'])?$POST['customfield']['name']:''; ?>" />
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="default" class="control-label">Default Value</label>
                        <div class="bs-component">
                            <input type="text" name="customfield[default]" id="default" class="form-control" value="<?php echo isset($POST['customfield']['default'])?$POST['customfield']['default']:''; ?>" />
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="opportunity_type" class="control-label"><?php echo Customlang::process('Product Type'); ?></label>
                        <div class="bs-component">
                            <select id="opportunity_type" name="customfield[producttype]" class="form-control">
                            <?php foreach($producttypes as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['customfield']['producttype']) && $POST['customfield']['producttype']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="description" class="control-label">Description</label>
                        <div class="bs-component">
                            <textarea  name="customfield[description]" id="description" class="form-control"><?php echo @$POST['customfield']['description']; ?></textarea>
                        </div>
                    </div>
                </div>
                <!-- end section -->
            </div>
            <!-- end .form-body section -->

            <div class="panel-footer">
                <button type="submit" class="btn btn-sm btn-primary">Submit</button>
            </div>
            <!-- end .form-footer section -->
        </form>
    </div>
    <!-- end: .panel -->
</div>
<!-- end: .contact-form -->


