<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="UTF-8">
    <title><?php echo $app['title']; ?> | <?php echo $page['title']; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base href="<?php echo $SCHEME.'://'.$HOST.$BASE.'/'; ?>"/>

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>

    <!-- Glyphicons Pro CSS(font) -->
    <link rel="stylesheet" type="text/css" href="/public/assets/fonts/glyphicons-pro/glyphicons-pro.css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" type="text/css" href="/public/vendor/plugins/magnific/magnific-popup.css">

    <!-- Datatables CSS -->
    <link rel="stylesheet" type="text/css" href="/public/vendor/plugins/datatables/media/css/dataTables.bootstrap.css">

    <!-- Datatables Addons CSS -->
    <link rel="stylesheet" type="text/css" href="/public/vendor/plugins/datatables/media/css/dataTables.plugins.css">

    <!-- Select2 Plugin CSS  -->
    <link rel="stylesheet" type="text/css" href="/public/vendor/plugins/select2/css/core.css">

    <!-- Bootstrap Datepicker -->
    <link rel="stylesheet" type="text/css" href="/public/vendor/plugins/datepicker/css/bootstrap-datetimepicker.css">

    <!-- Summernote Plugin CSS  -->
    <link rel="stylesheet" type="text/css" href="/public/vendor/plugins/summernote/summernote.css">

    <!-- Dropzone -->
    <link rel="stylesheet" type="text/css" href="/public/vendor/plugins/dropzone/css/dropzone.css">

    <!-- Tagsinput -->
    <link rel="stylesheet" type="text/css" href="/public/vendor/plugins/tagsinput/tagsinput.css">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="/public/assets/skin/default_skin/css/theme.css">

    <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="/public/assets/admin-tools/admin-forms/css/admin-forms.css">
    <link rel="stylesheet" type="text/css" href="/public/assets/admin-tools/admin-forms/css/jquery-labelauty.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="/public/assets/skin/default_skin/css/custom.css">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.1/css/selectize.min.css">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.1/css/selectize.bootstrap3.min.css">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/blue/pace-theme-minimal.css">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/0.4.5/sweet-alert.min.css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="/public/assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

</head>

<body class="sb-l-o sb-r-c">
<script>
    function calculateTotal( ) {
        var sum = 0;
        $('input.amount').each(function( index, elem ) {
            var val = parseFloat($(elem).val().replace(/[^0-9-.]/g, ''));
            if( !isNaN( val ) ) {
                sum += val;
            }
        });
        $('span.total').html(sum.toFixed(2));
        var pay = 0;
        $('input.productscheck').each(function( index, elem ) {
            if($(elem).is(':checked')){
               var qty= $(this).closest('tr').find('input.qtycheck').val();
                var val = parseFloat($(elem).attr('data-amount').replace(/[^0-9-.]/g, ''));
                if( !isNaN( val ) && !isNaN( qty )  ) {
                    pay += val*qty;
                }
            }

        });
        $('span.expected').html(pay.toFixed(2));
        $('button.quote-generate').prop('disabled',true)
        if(pay == sum && pay>0){
            $('button.quote-generate').prop('disabled',false)
        }


    }
</script>

<!-- Start: Main -->
<div id="main">

    <!-- Start: Header -->
    <header class="navbar navbar-fixed-top bg-success">
        <div class="navbar-branding">
            <a class="navbar-brand" href="/">
                <b>Sales</b><i>Ant</i> CRM
            </a>
            <span id="toggle_sidemenu_l" class="ad ad-lines"></span>
        </div>

        <form class="navbar-form navbar-left navbar-search" role="search">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search..." />
            </div>
        </form>

        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <span class="ad ad-radio-tower fs18"></span>
                </a>
                <ul class="dropdown-menu media-list w350 animated animated-shorter fadeIn" role="menu">
                    <li class="dropdown-header">
                        <span class="dropdown-title"> Notifications</span>
                        <span class="label label-warning">12</span>
                    </li>
                    <li class="media">
                        <a class="media-left" href="#"> <img src="/public/assets/img/avatars/5.jpg" class="mw40" alt="avatar"> </a>
                        <div class="media-body">
                            <h5 class="media-heading">Article
                                <small class="text-muted">- 08/16/22</small>
                            </h5> Last Updated 36 days ago by
                            <a class="text-system" href="#"> Max </a>
                        </div>
                    </li>
                    <li class="media">
                        <a class="media-left" href="#"> <img src="/public/assets/img/avatars/2.jpg" class="mw40" alt="avatar"> </a>
                        <div class="media-body">
                            <h5 class="media-heading mv5">Article
                                <small> - 08/16/22</small>
                            </h5>
                            Last Updated 36 days ago by
                            <a class="text-system" href="#"> Max </a>
                        </div>
                    </li>
                    <li class="media">
                        <a class="media-left" href="#"> <img src="/public/assets/img/avatars/3.jpg" class="mw40" alt="avatar"> </a>
                        <div class="media-body">
                            <h5 class="media-heading">Article
                                <small class="text-muted">- 08/16/22</small>
                            </h5> Last Updated 36 days ago by
                            <a class="text-system" href="#"> Max </a>
                        </div>
                    </li>
                    <li class="media">
                        <a class="media-left" href="#"> <img src="/public/assets/img/avatars/4.jpg" class="mw40" alt="avatar"> </a>
                        <div class="media-body">
                            <h5 class="media-heading mv5">Article
                                <small class="text-muted">- 08/16/22</small>
                            </h5> Last Updated 36 days ago by
                            <a class="text-system" href="#"> Max </a>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle" <?php echo count($USER->organisations->castAll())>1?'data-toggle="dropdown" href="#"':''; ?>>
                    <span class="fa fa-dot-circle-o text-danger-darker"></span> <?php echo \Models\Organisation::id($USER->organisation)->name; ?>
                </a>
                <ul class="dropdown-menu pv5 animated animated-short flipInX" role="menu">
                    <?php foreach (($USER->organisations?:array()) as $organisation): ?>
                        <?php if ($organisation->id == $USER->organisation): ?>
                            <?php else: ?>
                                <li>
                                    <a href="javascript:void(0);" data-organisation="<?php echo $organisation->id; ?>" class="organisation-switch">
                                        <span class="fa fa-dot-circle-o text-danger mr10"></span> <?php echo $organisation->name; ?> </a>
                                </li>
                            
                        <?php endif; ?>
                    <?php endforeach; ?>

                </ul>
            </li>


            <li class="menu-divider hidden-xs">
                <i class="fa fa-circle"></i>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle fw600 p15" data-toggle="dropdown"> <img src="<?php echo $USER->gravatar; ?>" alt="avatar" class="mw30 br64 mr15">
                    <?php echo $USER->firstname; ?>
                    <span class="caret caret-tp hidden-xs"></span>
                </a>
                <ul class="dropdown-menu list-group dropdown-persist w250" role="menu">
                    <li class="dropdown-header clearfix">

                        <div class="ml10">
                            <strong><?php echo $USER->fullnames; ?></strong><br/>
                            <?php echo $USER->email; ?><br/>
                            <?php echo $USER->telephone; ?><br/>
                        </div>

                    </li>
                    <li class="list-group-item">
                        <a href="/account/profile" class="animated animated-short fadeInUp">
                            <span class="fa fa-lightbulb-o"></span> Profile </a>
                    </li>
                    <li class="list-group-item">
                        <a href="/organisations/view/<?php echo $SESSION['organisation']; ?>" class="animated animated-short fadeInUp">
                            <span class="fa fa-gear"></span> Settings </a>

                    </li>
                    <li class="list-group-item">
                        <a href="/organisations" class="animated animated-short fadeInUp">
                            <span class="fa fa-bank"></span> Organisations </a>

                    </li>
                    <li class="list-group-item">

                        <a href="/users" class="animated animated-short fadeInUp">
                            <span class="glyphicons glyphicons-user"></span> Users
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="/auth/logout" class="animated animated-short fadeInUp">
                            <span class="fa fa-power-off"></span> Logout </a>
                    </li>
                </ul>
            </li>
        </ul>

    </header>
    <!-- End: Header -->

    <!-- Start: Sidebar -->
    <aside id="sidebar_left" class="nano nano-primary affix">

        <!-- Start: Sidebar Left Content -->
        <div class="sidebar-left-content nano-content">
            <!-- Start: Sidebar Menu -->
            <ul class="nav sidebar-menu">
                <li class="sidebar-label pt20">Menu</li>
                <li data-module="tasks">
                    <a href="/">
                        <span class="fa fa-envelope"></span>
                        <span class="sidebar-title">Dashboard</span>

                    </a>
                </li>
                <li data-module="products">
                    <a href="/products">
                        <span class="glyphicon glyphicon-tags"></span>
                        <span class="sidebar-title"><?php echo Customlang::process('Products'); ?></span>
                    </a>
                </li>
                <li data-module="leads">
                    <a href="/leads">
                        <span class="glyphicons glyphicons-nameplate"></span>
                        <span class="sidebar-title">Leads</span>
                    </a>
                </li>
                <li data-module="opportunities">
                    <a href="/opportunities">
                        <span class="glyphicons glyphicons-cup"></span>
                        <span class="sidebar-title">Opportunities</span>
                    </a>
                </li>
                <li data-module="newsletters">
                    <a class="accordion-toggle" href="#">
                        <span class="glyphicons glyphicons-briefcase"></span>
                        <span class="sidebar-title">Newsletters</span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a href="/newsletters">
                                <?php echo Customlang::process('Groups'); ?></a>
                        </li>
                        <li>
                            <a href="/newslettertemplates">
                                <?php echo Customlang::process('Templates'); ?></a>
                        </li>
                        <li>
                            <a href="/newsletters/send">
                                <?php echo Customlang::process('Send'); ?></a>
                        </li>

                    </ul>
                </li>
                <li data-module="quotations">
                    <a href="/quotations">
                        <span class="glyphicons glyphicons-book_open"></span>
                        <span class="sidebar-title"><?php echo Customlang::process('Quotations'); ?></span>
                    </a>
                </li>
                <li data-module="sales">
                    <a href="/sales">
                        <span class="glyphicon glyphicon-shopping-cart"></span>
                        <span class="sidebar-title"><?php echo Customlang::process('Sales'); ?></span>
                    </a>
                </li>
                <li>
                    <a class="accordion-toggle" href="#">
                        <span class="fa fa-bar-chart"></span>
                        <span class="sidebar-title">Reporting</span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                                <li>
                                    <a href="/reporting/opportunities">
                                        <?php echo Customlang::process('Projected Sales'); ?></a>
                                </li>
                                <li>
                                    <a href="/reporting/sales">
                                        <?php echo Customlang::process('Sales & Payments'); ?></a>
                                </li>

                    </ul>
                </li>

                <!-- sidebar bullets -->
                <li class="sidebar-label pt20">Smart Views</li>
                <li class="sidebar-proj">
                    <a href="#">
                        <span class="fa fa-dot-circle-o text-info"></span>
                        <span class="sidebar-title">Recently Created</span>
                    </a>
                </li>
                <li class="sidebar-proj">
                    <a href="#">
                        <span class="fa fa-dot-circle-o text-info"></span>
                        <span class="sidebar-title">Never Contacted</span>
                    </a>
                </li>
                <?php foreach (($leadstatuses?:array()) as $status): ?>
                    <li class="sidebar-proj">
                        <a href="/leads/smartview/<?php echo $status->id; ?>?status=<?php echo $status->title; ?>">
                            <span class="fa fa-dot-circle-o text-info"></span>
                            <span class="sidebar-title">Status: <?php echo $status->title; ?></span>
                        </a>
                    </li>
                <?php endforeach; ?>


            </ul>
            <!-- End: Sidebar Menu -->

            <!-- Start: Sidebar Collapse Button -->
            <div class="sidebar-toggle-mini">
                <a href="#">
                    <span class="fa fa-sign-out"></span>
                </a>
            </div>
            <!-- End: Sidebar Collapse Button -->

        </div>
        <!-- End: Sidebar Left Content -->

    </aside>

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

        <!-- Start: Topbar -->
        <header id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">

                    </li>
                    <li class="crumb-icon">
                        <a href="/">
                            <span class="glyphicon glyphicon-home"></span>
                        </a>
                    </li>

                </ol>
            </div>
        </header>
        <!-- End: Topbar -->
        <!-- Begin: Content -->

        <div class="col-lg-12" style="margin-top: 10px">
            <?php foreach ((\Flash::instance()->getMessages()?:array()) as $msg): ?>
                <div class="alert alert-<?php echo $msg['status']; ?> alert-dismissable alert-dismiss">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->esc($msg['text']); ?>
                </div>
            <?php endforeach; ?>
        </div>
        <div id="content" class="animated fadeIn">

            <?php if ($INNER['PAGE']) echo $this->render($INNER['PAGE'],$this->mime,get_defined_vars()); ?>
        </div>
        <!-- End: Content -->

            
        </section>
</div>

<!-- BEGIN: PAGE SCRIPTS -->
<!-- PACE JS-->
<script src="//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
<!-- jQuery -->
<script src="/public/vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="/public/vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Magnific Popup -->
<script src="/public/vendor/plugins/magnific/jquery.magnific-popup.js"></script>

<!-- Datatables -->
<script src="/public/vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>

<!-- Datatables Tabletools addon -->
<script src="/public/vendor/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>

<!-- Datatables ColReorder addon -->
<script src="/public/vendor/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>

<!-- Datatables Bootstrap Modifications  -->
<script src="/public/vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>

<!-- Holder js  -->
<script src="/public/vendor/plugins/holder/holder.min.js"></script>

<!-- Select2 Plugin Plugin -->
<script src="/public/vendor/plugins/select2/select2.min.js"></script>

<!-- Time/Date Plugin Dependencies -->
<script src="/public/vendor/plugins/globalize/globalize.min.js"></script>
<script src="/public/vendor/plugins/moment/moment.min.js"></script>

<!-- DateTime Plugin -->
<script src="/public/vendor/plugins/datepicker/js/bootstrap-datetimepicker.js"></script>

<!-- Populate Plugin -->
<script src="/public/vendor/plugins/populate/jquery.populate.pack.js"></script>

<!-- Summernote Plugin -->
<script src="/public/vendor/plugins/summernote/summernote.min.js"></script>

<!-- Drop Zone -->
<script src="/public/vendor/plugins/dropzone/dropzone.min.js"></script>

<!-- Tags Input -->
<script src="/public/vendor/plugins/tagsinput/tagsinput.min.js"></script>

<!-- Theme Javascript -->
<script src="/public/assets/js/utility/utility.js"></script>
<script src="/public/assets/js/main.js"></script>
<script src="/public/assets/js/custom.js"></script>
<script src="/public/assets/js/jquery-labelauty.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.pjax/1.9.6/jquery.pjax.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/garlic.js/1.2.4/garlic.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/mousetrap/1.4.6/mousetrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/0.4.5/sweet-alert.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.1/js/standalone/selectize.min.js"></script>
<script src="/public/vendor/plugins/jqueryflot/jquery.flot.min.js"></script>
<script src="/public/vendor/plugins/jqueryflot/jquery.flot.pie.min.js"></script>
<script src="/public/vendor/plugins/typeahead/typeahead.bundle.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        "use strict";
        // Init Theme Core
        Core.root = "<?php echo $SCHEME.'://'.$HOST.$BASE.'/'; ?>";
        Core.init();
        Core.load();
        Custom.init();
        updateAllRelativeDates();
        updateScrollers();
        $('a.organisation-switch').click(function (e) {
            Pace.restart();
            $.get('/organisations/changeto/'+ $(this).attr('data-organisation'),function(res){
                if(res.success)
                    window.location = '/';
            });
            e.preventDefault();
        })
        $(document).pjax("a[target!='_blank']", '#content');
        $(document).on('pjax:complete', function() {
            Core.init();
            Core.load();
            Custom.init();
            updateAllRelativeDates();
            updateScrollers();
            $(window).trigger( 'load' );
        })
        $.pjax.defaults.timeout = 1200;



        $('.sidebar-menu li a.accordion-toggle').on('click', function(e) {
            e.preventDefault();

            // If the clicked menu item is minified and is a submenu (has sub-nav parent) we do nothing
            if ($('body').hasClass('sb-l-m') && !$(this).parents('ul.sub-nav').length) { return; }

            // If the clicked menu item is a dropdown we open its menu
            if (!$(this).parents('ul.sub-nav').length) {

                // If sidebar menu is set to Horizontal mode we return
                // as the menu operates using pure CSS
                if ($(window).width() > 900) {
                    if ($('body.sb-top').length) { return; }
                }

                $('a.accordion-toggle.menu-open').next('ul').slideUp('fast', 'swing', function() {
                    $(this).attr('style', '').prev().removeClass('menu-open');
                });
            }
            // If the clicked menu item is a dropdown inside of a dropdown (sublevel menu)
            // we only close menu items which are not a child of the uppermost top level menu
            else {
                var activeMenu = $(this).next('ul.sub-nav');
                var siblingMenu = $(this).parent().siblings('li').children('a.accordion-toggle.menu-open').next('ul.sub-nav')

                activeMenu.slideUp('fast', 'swing', function() {
                    $(this).attr('style', '').prev().removeClass('menu-open');
                });
                siblingMenu.slideUp('fast', 'swing', function() {
                    $(this).attr('style', '').prev().removeClass('menu-open');
                });
            }

            // Now we expand targeted menu item, add the ".open-menu" class
            // and remove any left over inline jQuery animation styles
            if (!$(this).hasClass('menu-open')) {
                $(this).next('ul').slideToggle('fast', 'swing', function() {
                    $(this).attr('style', '').prev().toggleClass('menu-open');
                });
            }

        });

    });
</script>

<!-- END: PAGE SCRIPTS -->

</body>

</html>
