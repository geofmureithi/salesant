<!-- Task Widget -->
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-widget task-widget task-alt" id="p05">
            <div class="panel-heading cursor">
                    <span class="panel-icon">
                      <i class="fa fa-cog"></i>
                    </span>
                <span class="panel-title"> Upcoming Tasks</span>
            </div>
            <div class="panel-scroller scroller-md scroller-pn pn panel-body p15">
                <ul class="task-list task-current">
                    <?php foreach (($USER->pendingtasks?:array()) as $task): ?>
                        <li class="task-item warning">
                            <div class="task-handle">
                                <div class="checkbox-custom">
                                    <input type="checkbox" />
                                    <label for="task4"></label>
                                </div>
                            </div>
                            <div class="task-desc"><b><?php echo $task->title; ?> </b> - <?php echo $task->lead->title; ?> <time class="cw-relative-date" datetime="<?php echo $task->datedue; ?>"></time><a class="pull-right" href="/leads/view/<?php echo $task->lead->id; ?>"><i class="fa fa-search-plus"></i>View</a></div>
                            <div class="task-menu"></div>
                        </li>
                    <?php endforeach; ?>

                </ul>
            </div>

        </div>
    </div>
    <div class="col-md-6">

        <div class="panel">
            <div class="panel-heading cursor">
                    <span class="panel-icon">
                      <i class="fa fa-angle-up"></i>
                    </span>
                <span class="panel-title"> Recent Activity</span>
            </div>
            <div class="panel-body <?php echo count(\Application\Models\Activity::happening())>7? 'panel-scroller scroller-md scroller-pn pn':''; ?>">
                <ol class="timeline-list">
                    <?php foreach ((\Application\Models\Activity::happening()?:array()) as $activity): ?>
                        <li class="timeline-item">
                            <div class="timeline-icon bg-dark light">
                                <span class="<?php echo $activity->icon; ?>"></span>
                            </div>
                            <div class="timeline-desc">

                                <p><span class="text-darker"><a href="/leads/view/<?php echo $activity->lead->id; ?>"><b><?php echo $activity->lead->title; ?></b></a>: <?php echo $activity->description; ?></span>
                                    </br><time class="cw-relative-date" datetime="<?php echo $activity->datecreated; ?>"></time>
                                </p>

                            </div>
                            <div class="timeline-date rightn">
                                <?php echo $activity->creator->firstname; ?>

                                <img src="<?php echo $activity->creator->gravatar; ?>" alt="avatar" class="mw30 br4 ml10">

                            </div>
                        </li>

                    <?php endforeach; ?>

                </ol>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">

        <div class="panel">
            <div class="panel-heading cursor">
                    <span class="panel-icon">
                      <i class="fa fa-angle-left"></i>
                    </span>
                <span class="panel-title"> Latest Leads</span>
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                    <tr class="success">
                        <th>#</th>
                        <th>Lead Name</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach (($leads?:array()) as $lead): ?>
                        <tr>
                            <td><?php echo $lead->id; ?></td>
                            <td><?php echo $lead->title; ?></td>
                            <td><?php echo $lead->status->title; ?></td>
                            <td><a href="/leads/view/<?php echo $lead->id; ?>"><i class="fa fa-eye"></i>View</a> </td>
                        </tr>
                    <?php endforeach; ?>


                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">

        <div class="panel">
            <div class="panel-heading cursor">
                    <span class="panel-icon">
                      <i class="fa fa-angle-left"></i>
                    </span>
                <span class="panel-title">Warm Opportunities</span>
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                    <tr class="system">
                        <th>Contact</th>
                        <th><?php echo Customlang::process('Product Type'); ?></th>
                        <th><?php echo Customlang::process('Products'); ?></th>
                        <th>Amount</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach (($opportunities?:array()) as $opportunity): ?>
                        <tr>
                            <td><?php echo $opportunity->contact->fullnames; ?></td>
                            <td><?php echo $opportunity->producttype->title; ?></td>
                            <td><?php echo $opportunity->products['0']->title; ?> <?php echo $opportunity->products?count($opportunity->products):0 > 1?'+'.count($opportunity->products)-1:''; ?></td>
                            <td><?php echo Currency::format($opportunity->amount); ?></td>
                            <td><a href="/leads/view/<?php echo $opportunity->lead->id; ?>"><i class="fa fa-eye"></i></a> </td>
                        </tr>
                    <?php endforeach; ?>


                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-widget">
            <div class="panel-heading cursor">
                    <span class="panel-icon">
                      <i class="fa fa-plus-circle"></i>
                    </span>
                <span class="panel-title"> Quick Access</span>
            </div>
            <div class="panel-body">
                <div class="col-md-3 text-center">
                    <a href="/leads/add" class="btn btn-default btn-sm light fw600 ml10 ajax-modal text-center">
                        <span class="fa fa-plus pr5"></span> Add New Lead</a>
                    <div class="clearfix"><br/></div>
                    <div class="panel panel-tile text-center">
                        <div class="panel-body bg-info">
                            <h1 class="fs35 mbn"><?php echo $USER->myleads?count($USER->myleads):'0'; ?></h1>
                            <h6 class="text-white">LEAD(s)</h6>
                        </div>
                        <div class="panel-footer br-n p12">
    <span class="fs11">
      <a href="/leads"><i class="fa fa-arrow-right text-info pr5"></i></a>
      <b></b>
    </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 text-center">
                    <a href="/products/add" class="btn btn-default btn-sm light fw600 ml10 ajax-modal text-center">
                        <span class="fa fa-plus pr5"></span> Add <?php echo Customlang::process('Product'); ?></a>
                    <div class="clearfix"><br/></div>
                    <div class="panel panel-tile text-center">
                        <div class="panel-body bg-info">
                            <h1 class="fs35 mbn"><?php echo $USER->myproducts?count($USER->myproducts):'0'; ?></h1>
                            <h6 class="text-white"><?php echo Customlang::process('Product'); ?>(s)</h6>
                        </div>
                        <div class="panel-footer br-n p12">
    <span class="fs11">
        <a href="/products"><i class="fa fa-arrow-right text-info pr5"></i></a>
      <b></b>
    </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 text-center">
                    <a href="/quotations" class="btn btn-default btn-sm light fw600 ml10 text-center">
                        <span class="fa fa-plus pr5"></span> View <?php echo Customlang::process('Quotations'); ?></a>
                    <div class="clearfix"><br/></div>
                    <div class="panel panel-tile panel-warning text-center">
                        <div class="panel-body bg-info">
                            <h1 class="fs35 mbn"><?php echo $USER->quotations?count($USER->quotations):'0'; ?></h1>
                            <h6 class="text-white"><?php echo Customlang::process('Quotations'); ?>(s)</h6>
                        </div>
                        <div class="panel-footer br-n p12">
    <span class="fs11">
        <a href="/quotations"><i class="fa fa-arrow-right text-info pr5"></i></a>
      <b></b>
    </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 text-center">
                    <a href="/opportunities" class="btn btn-default btn-sm light fw600 ml10 text-center">
                        <span class="fa fa-plus pr5"></span> View Opportunities</a>
                    <div class="clearfix"><br/></div>
                    <div class="panel panel-tile panel-warning text-center">
                        <div class="panel-body bg-info">
                            <h1 class="fs35 mbn"><?php echo $USER->myopportunities?count($USER->myopportunities):'0'; ?></h1>
                            <h6 class="text-white"><?php echo Customlang::process('Opportunity'); ?>(s)</h6>
                        </div>
                        <div class="panel-footer br-n p12">
    <span class="fs11">
      <a href="/opportunities"><i class="fa fa-arrow-right text-info pr5"></i></a>
      <b></b>
    </span>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>


