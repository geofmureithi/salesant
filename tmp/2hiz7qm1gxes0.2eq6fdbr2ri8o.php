<!-- create new order panel -->
<div class="clearfix">

</div>
<div class="panel mb25 mt5">
    <div class="panel-heading">
        <span class="panel-title hidden-xs"><?php echo Customlang::process('Quotation'); ?> Summary</span>
        <div class="panel-header-menu pull-right mr10">
            <a target="_blank" class="btn btn-xs btn-default btn-gradient mr5">
                <i class="fa fa-file-pdf-o fs13"></i> Export PDF
            </a>
            <a target="_blank" href="/opportunity/<?php echo $quotation->opportunity->id; ?>/quotations/generateword/<?php echo $quotation->id; ?>" class="btn btn-xs btn-default btn-gradient mr5">
                <i class="fa fa-file-word-o fs13"></i> Export Word
            </a>
            <a href="/quotation/<?php echo $quotation->id; ?>/documents/add" class="btn btn-xs btn-default btn-gradient mr5 ajax-modal" style="display: none">
                <i class="fa fa-file-o fs13"></i> Add Document
            </a>
            <div class="btn-group">
                <button type="button" class="btn btn-xs btn-default btn-gradient dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class="glyphicon glyphicon-cog"></span>
                </button>
                <ul class="dropdown-menu checkbox-persist pull-right text-left" role="menu">
                    <li>
                        <a href="/opportunity/<?php echo $quotation->opportunity->id; ?>/quotations/approve/<?php echo $quotation->id; ?>">
                            <i class="fa fa-plus-circle"></i> Approve </a>
                    </li>
                    <li>
                        <a href="/opportunity/<?php echo $quotation->opportunity->id; ?>/quotations/edit/<?php echo $quotation->id; ?>">
                            <i class="fa fa-edit"></i> Edit </a>
                    </li>
                    <li>
                        <a>
                            <i class="fa fa-times"></i> Delete</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel-body p25 pb5">
        <div class="tab-content pn br-n admin-form">
            <div id="tab1_1" class="tab-pane active">

                <div class="section row">
                    <div class="col-md-6">
                        <div class="media">
                            <a class="media-left">
                                <img alt="60x60" src="<?php echo $quotation->opportunity->contact->gravatar; ?>" style="width: 60px; height: 60px;">
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading"><?php echo $quotation->opportunity->contact->fullnames; ?></h4>
                                Email: <?php echo $quotation->opportunity->contact->email; ?><br/>
                                Tel: <?php echo $quotation->opportunity->contact->telephone; ?><br/>
                                <a href="/contacts/edit/<?php echo $quotation->opportunity->contact->id; ?>" class="ajax-modal"><i class="fa fa-edit"></i>Edit</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <p class="text-info mn"><strong>Date Created: </strong><?php echo $quotation->datecreated; ?></p>
                        <p class="text-info mn"><strong>Created By: </strong><?php echo $quotation->creator->fullnames; ?></p>
                    </div>
                </div>
                <!-- end section -->

                <div class="section row">
                    <div class="col-md-6">
                        <div class="well">
                            <address>
                                <strong>Postal Address</strong><br/>
                                <?php echo $quotation->opportunity->contact->postalAddress; ?>
                            </address>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="well">
                            <address>
                                <strong>Physical Address</strong><br/>
                                <?php echo $quotation->opportunity->contact->physicalAddress; ?>
                            </address>
                        </div>
                    </div>
                </div>
                <!-- end section -->
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title hidden-xs"><?php echo Customlang::process('Products'); ?> Details</span>
                    </div>
                    <div class="panel-body pn">
                        <div class="table-responsive">
                            <table class="table admin-form theme-warning tc-checkbox-1 fs13">
                                <thead>
                                <tr class="bg-light">
                                    <th class="">Image</th>
                                    <th class=""><?php echo Customlang::process('Product Title'); ?></th>
                                    <th class="">ID</th>
                                    <th class="">Price</th>
                                    <th class="">QTY</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach (($quotation->products?:array()) as $product): ?>
                                <tr>
                                    <td class="w100">
                                        <img class="img-responsive mw40 ib mr10" title="user" src="assets/img/stock/products/thumb_1.jpg">
                                    </td>
                                    <td class=""><?php echo $product->title; ?> <a href="/products/edit/<?php echo $product->id; ?>" class="ajax-modal"><i class="fa fa-edit"></i>Edit</a></td>
                                    <td class="">#<?php echo $product->id; ?></td>
                                    <td class=""><?php echo Currency::format($product->price); ?></td>
                                    <td class=""><?php echo \Application\Models\QuotationsProducts::getQuantity($quotation->id,$product->id); ?></td>
                                </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title hidden-xs"><?php echo Customlang::process('Payment Terms'); ?></span>
                    </div>
                    <div class="panel-body pn">
                        <div class="table-responsive">
                            <table class="table admin-form theme-warning tc-checkbox-1 fs13 text-left">
                                <thead>
                                <tr class="bg-light">
                                    <th class="">Date</th>
                                    <th class="">Amount</th>
                                    <th class="">Description</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach (($quotation->paymentPlan?:array()) as $payment): ?>
                                <tr>
                                    <td class="text-left"><?php echo $payment->datedue; ?></td>
                                    <td class=""><?php echo Currency::format($payment->amount); ?></td>
                                    <td class=""><?php echo $payment->description; ?></td>
                                </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

