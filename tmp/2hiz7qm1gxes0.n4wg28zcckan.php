<div class="panel">
    <div class="panel-heading">
         <span class="panel-icon">
                      <i class="fa fa-user-plus"></i>
                    </span>
        <span class="panel-title"> <?php echo $page['title']; ?></span>
    </div>
    <div class="panel-body">
        <form method="post" action="<?php echo $form['action']; ?>" data-parsley-validate>
            <div class="row general-fc">
                <?php foreach (($permissions?:array()) as $module=>$perms): ?>
                    <div class="col-md-6">
                        <div class="panel">
                            <div class="panel-heading">
                                <span class="panel-title"><?php echo strtoupper($module); ?></span>
                            </div>
                            <div class="panel-body bg-light pt20 pbn pl30">
                                <div class="row">
                                    <?php foreach (($perms?:array()) as $action=>$method): ?>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="col-sm-12 pl15">
                                                    <div class="bs-component">
                                                        <div class="checkbox-custom checkbox-primary mb10">
                                                            <input type="checkbox" id="checkbox_<?php echo $module.$action; ?>" name="permission[@module][@action]" <?php echo (isset($POST['permission'][$module][$action]) && $POST['permission'][$module][$action]=='on')?'checked="checked"':''; ?> />
                                                            <label for="checkbox_<?php echo $module.$action; ?>"><?php echo str_replace("ACTION_", "" , $action); ?></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>


                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>


            <div class="panel-footer text-right">
                        <button class="btn btn-success">Save and Continue</button>
            </div>
        </form>
    </div>

</div>

