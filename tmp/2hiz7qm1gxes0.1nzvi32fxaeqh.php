<div class="row">
    <form class="<?php echo $form['action']; ?>" method="get">
    <div class="panel ">
        <div class="panel-heading">
            <div class="panel-title hidden-xs">
                <span class="glyphicon glyphicon-tasks"></span><?php echo Customlang::process('Sort'); ?></div>
        </div>
        <div class="panel-body">
            <?php echo $this->render('/general/timespan.html',$this->mime,get_defined_vars()); ?>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="opportunity_type" class="control-label"><?php echo Customlang::process('Opportunity Type'); ?></label>
                    <div class="bs-component">
                        <select id="opportunity_type" name="productType" class="select2-single form-control">
                        <?php foreach($producttypes as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['productType']) && $POST['productType']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label"></label>
                    <div class="bs-component">
                        <button class="btn btn-warning pull-right col-md-12" type="submit">Process</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title hidden-xs">
                    <span class="glyphicon glyphicon-copy"></span>Expected <?php echo Customlang::process('Payments'); ?></div>
            </div>
            <div class="panel-body pn" id="expectedPayments" style="height: 200px">

            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title hidden-xs">
                    <span class="glyphicon glyphicon-copy"></span>Late <?php echo Customlang::process('Payments'); ?></div>
            </div>
            <div class="panel-body pn" id="latePayments" style="height: 200px">

            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title hidden-xs">
                    <span class="glyphicon glyphicon-copy"></span> Paid <?php echo Customlang::process('Payments'); ?></div>
            </div>
            <div class="panel-body pn" id="paidPayments" style="height: 200px">

            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy4">
            <div class="panel-heading">
                <div class="panel-title hidden-xs">
                    <span class="glyphicon glyphicon-tasks"></span><?php echo Customlang::process('Sales Outlook'); ?> Report</div>
            </div>
            <div class="panel-menu"></div>
            <div class="panel-body pn">
                <table class="table table-striped table-hover display" id="datatable6" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Client</th>
                        <th><?php echo Customlang::process('Product'); ?>(s)</th>
                        <th><?php echo Customlang::process('Total'); ?>(s)</th>
                        <th><?php echo Customlang::process('Amount'); ?> Expected</th>
                        <th><?php echo Customlang::process('Amount'); ?> Paid</th>
                        <th><?php echo Customlang::process('Amount'); ?> Late</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach (($sales?:array()) as $sale): ?>
                        <tr>
                            <td><?php echo $sale->contact->fullnames; ?></td>
                            <td><?php echo $sale->products['0']->title; ?></td>
                            <td><?php echo Currency::format($sale->amount); ?></td>
                            <td><?php echo Currency::format($sale->expected($POST['startDate'],$POST['endDate'])); ?></td>
                            <td><?php echo Currency::format($sale->paid($POST['startDate'],$POST['endDate'])); ?></td>
                            <td><?php echo Currency::format($sale->late($POST['startDate'],$POST['endDate'])); ?></td>
                            <td><a href="/sales/view/<?php echo $sale->id; ?>" >View</a></td>
                        </tr>
                    <?php endforeach; ?>


                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    var sorter = function(a,b){
        return (b.data - a.data);
    };
    var latePayments = [
            <?php foreach (($sales?:array()) as $sale): ?>
        { label: "<?php echo $sale->contact->firstname; ?> : <?php echo Currency::format($sale->late($POST['startDate'],$POST['endDate'])); ?>",  data: <?php echo $sale->late($POST['startDate'],$POST['endDate']); ?>},
            <?php endforeach; ?>
    ];
    latePayments.sort(sorter);
    latePayments = latePayments.splice(0,5);
    var expectedPayments = [
            <?php foreach (($sales?:array()) as $sale): ?>
                { label: "<?php echo $sale->contact->firstname; ?> : <?php echo Currency::format($sale->expected($POST['startDate'],$POST['endDate'])); ?>",  data: <?php echo $sale->expected($POST['startDate'],$POST['endDate']); ?>},
            <?php endforeach; ?>
    ];
    expectedPayments.sort(sorter);
    expectedPayments = expectedPayments.splice(0,5);
    var paidPayments = [
            <?php foreach (($sales?:array()) as $sale): ?>
            { label: "<?php echo $sale->contact->firstname; ?> : <?php echo Currency::format($sale->paid($POST['startDate'],$POST['endDate'])); ?>",  data: <?php echo $sale->paid($POST['startDate'],$POST['endDate']); ?>},
        <?php endforeach; ?>
    ];
    paidPayments.sort(sorter);
    paidPayments = paidPayments.splice(0,5);
</script>

<script>
    $(function(){

        $.plot('#expectedPayments', expectedPayments, {
            series: {
                pie: {
                    show: true,
                    radius: 1,
                }
            },
            legend: {
                show: true
            }
        });
        $.plot('#latePayments', latePayments, {
            series: {
                pie: {
                    show: true,
                    radius: 1,
                }
            },
            legend: {
                show: true
            }
        });
        $.plot('#paidPayments', paidPayments, {
            series: {
                pie: {
                    show: true,
                    radius: 1,
                }
            },
            legend: {
                show: true
            }
        });
    })
</script>

