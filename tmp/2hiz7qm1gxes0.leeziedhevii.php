<!-- Admin Form Popup -->
<div id="modal-form" class="popup-basic mfp-with-anim">
    <div class="panel">
        <div class="panel-heading">
            <span class="panel-title">
                <i class="fa fa-building"></i><?php echo $page['title']; ?>
            </span>
        </div>
        <!-- end .panel-heading section -->
        <div id="results"></div>
        <form method="post" action="<?php echo $form['action']; ?>">
            <div class="panel-body p25">

                <div class="section">
                    <div class="form-group">
                        <label for="company" class="control-label">Company/Organization</label>
                        <div class="bs-component">
                            <input type="text" name="company" id="company" class="form-control" value="<?php echo isset($POST['company'])?$POST['company']:''; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="contact_name" class="control-label">Contact Name</label>
                        <div class="bs-component">
                            <input type="text" name="contact[name]" id="contact_name" class="form-control" value="<?php echo isset($POST['contact']['name'])?$POST['contact']['name']:''; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="source" class="control-label">Choose a Source</label>
                        <div class="bs-component">
                            <select name="lead[source]" id="source" class="select2-single form-control">
                            <?php foreach($sources as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['lead']['source']) && $POST['lead']['source']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                        </div>
                    </div>
                </div>
                <!-- end section -->

            </div>
            <!-- end .form-body section -->

            <div class="panel-footer">
                <button type="submit" class="btn btn-sm btn-primary">Submit</button>
            </div>
            <!-- end .form-footer section -->
        </form>
    </div>
    <!-- end: .panel -->
</div>
<!-- end: .admin-form -->
