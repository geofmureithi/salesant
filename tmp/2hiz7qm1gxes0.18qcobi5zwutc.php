<!-- Contact Form Popup -->
<div id="quotation-form" class="row">
    <div class="panel">
        <div class="panel-heading">
            <span class="panel-title">
                <i class="fa fa-building"></i><?php echo Customlang::process($page['title']); ?>
            </span>
        </div>
        <!-- end .panel-heading section -->

        <form id="quote-form" method="post" action="<?php echo $form['action']; ?>">
            <div class="panel-body p25">

                <div class="section">
                    <div class="form-group col-md-4">
                        <label for="contact" class="control-label">Contact</label>
                        <div class="bs-component">
                            <select id="contact" name="quotation[contact]" class="select2-single form-control">
                            <?php foreach($contacts as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['quotation']['contact']) && $POST['quotation']['contact']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="email" class="control-label">Email</label>
                        <div class="bs-component">
                            <input type="text" name="contact[email]" id="email" class="form-control" required value="<?php echo isset($POST['contact']['email'])?$POST['contact']['email']:''; ?>" />
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="telephone" class="control-label">Tel</label>
                        <div class="bs-component">
                            <input type="text" name="contact[telephone]" id="telephone" class="form-control" required value="<?php echo isset($POST['contact']['telephone'])?$POST['contact']['telephone']:''; ?>" />
                        </div>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="postaladdress" class="control-label">Postal Address</label>
                        <div class="bs-component">
                            <textarea  name="contact[postalAddress]" id="postaladdress" class="form-control"><?php echo @$POST['contact']['postalAddress']; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="physicaladdress" class="control-label">Physical Address</label>
                        <div class="bs-component">
                            <textarea  name="contact[physicalAddress]" id="physicaladdress" class="form-control"><?php echo @$POST['contact']['physicalAddress']; ?></textarea>
                        </div>
                    </div>
                    <div class="section col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <span class="panel-title">
                                    <i class="fa fa-building"></i>Choose <?php echo Customlang::process('Product'); ?>
                                </span>
                            </div>
                            <div class="panel-body">
                                <table class="table tc-checkbox-1">
                                    <thead>
                                    <tr class="bg-light">
                                        <th class="text-center">Select</th>
                                        <th class=""><?php echo Customlang::process('Product Title'); ?></th>
                                        <th class="">Unit</th>
                                        <th class="">Price</th>
                                        <th class="text-right">Qty</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach (($opportunity->products?:array()) as $product): ?>
                                        <tr>
                                            <td class="text-center">
                                                <label class="option block mn">
                                                    <input <?php echo $opportunity->organisation->settings['products']=='single'?'type="radio"':'type="checkbox"'; ?> class="productscheck" name="quotation[product][<?php echo $product->id; ?>][state]" data-amount="<?php echo $product->price; ?>" value="<?php echo $product->id; ?>" />
                                                    <span class="checkbox mn"></span>
                                                </label>
                                            </td>
                                            <td class=""><?php echo $product->title; ?></td>
                                            <td class=""><?php echo $product->unit; ?></td>
                                            <td class=""><?php echo Currency::format($product->price); ?></td>
                                            <td class="text-right input-xs qty col-xs-1">
                                                <input type="number" <?php echo $opportunity->organisation->settings['qty']=='no'?'readonly':''; ?> class="qtycheck form-control" name="quotation[product][<?php echo $product->id; ?>][qty]" value="1" /><?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="section col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <span class="panel-title">
                                    <?php if ($opportunity->organisation->settings['payment'] == 'multiple'): ?>
                                        
                                            <i class="fa fa-building"></i><?php echo Customlang::process('Payment Plan'); ?> <i>Press CTRL + A To add new Plan</i>
                                        
                                        <?php else: ?>
                                            <i class="fa fa-money"></i><?php echo Customlang::process('Payment'); ?> <i>Enter <?php echo Customlang::process('Quotation'); ?> Payment Details</i>
                                        

                                    <?php endif; ?>
                                </span>
                                <span class="pull-right">
                                    <?php if ($opportunity->organisation->settings['payment'] == 'multiple'): ?>
                                        
                                            <a class="add-payment-plan" href="#" >
                                                <i class="fa fa-plus"></i> Add
                                            </a>
                                        

                                    <?php endif; ?>

                                </span>
                            </div>
                            <div class="panel-body">
                                <div class="payment-removable">
                                    <div class="row single-payable">
                                        <div class="form-group col-sm-4">
                                            <label class="control-label">Amount</label>
                                            <div class="bs-component input-group">
                                                <span class="input-group-addon remover">
                                                     <i class="fa fa-close cursor"></i>
                                                </span>
                                                <input name="paymentplan[amount][]" class="form-control amount" required />
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label for="dop" class="control-label">Date of Payment</label>
                                            <div class="bs-component">
                                                <div class="input-group simpledate" id="dop">
                                                <span class="input-group-addon cursor">
                                                     <i class="fa fa-calendar"></i>
                                                </span>
                                                    <input type="text" name="paymentplan[datedue][]" class="form-control" style="background-color: #fff !important; cursor: text !important;" value="<?php echo isset($POST['paymentplan']['datedue'][''])?$POST['paymentplan']['datedue']['']:''; ?>" />
                                                </div>

                                            </div>

                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label class="control-label">Description</label>
                                            <div class="bs-component">
                                                <input name="paymentplan[description][]" class="form-control" />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="panel-heading">
                                <span class="panel-title">
                                    <?php if ($opportunity->organisation->settings['payment'] == 'multiple'): ?>
                                        
                                            <i class="fa fa-building"></i><?php echo Customlang::process('Payment Plan'); ?> <i>Press CTRL + A To add new Plan</i>
                                        
                                        <?php else: ?>
                                            <i class="fa fa-money"></i><?php echo Customlang::process('Payment'); ?> <i>Enter <?php echo Customlang::process('Quotation'); ?> Payment Details</i>
                                        

                                    <?php endif; ?>

                                </span>
                                <span class="pull-right">
                                    <?php if ($opportunity->organisation->settings['payment'] == 'multiple'): ?>
                                        
                                            <a class="add-payment-plan" href="#" >
                                                <i class="fa fa-plus"></i> Add
                                            </a>
                                        

                                    <?php endif; ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end section -->

            </div>
            <!-- end .form-body section -->

            <div class="panel-footer">
                <button type="submit" class="btn btn-sm btn-upload quote-generate" disabled><i class="fa fa-shopping-cart"></i> Generate</button>
                <span class="btn btn-sm btn-warning pull-right ">Current Value: <span class="total">0</span></span>
                <span class="btn btn-sm btn-primary pull-right">Expected Value: <span class="expected">0</span></span>
            </div>
            <!-- end .form-footer section -->
        </form>
    </div>
    <!-- end: .panel -->
</div>

<div id="quote-results" class="col-md-3">

</div>
<!-- end: .contact-form -->
<script>
    $(function() {

        Mousetrap.bind(['ctrl+a'], function(e) {
            $('#addPlan').trigger('click');
            return false;
        });


        $('input.amount').on('keyup',function () {
            calculateTotal();
        });
        $('input.qtycheck').on('change',function () {
            calculateTotal();
        });
        $('input.productscheck').on('change',function () {
            calculateTotal();
        });
        $('span.remover').on('click',function () {
            $(this).closest('div.single-payable').remove();
        });
        $('select#contact').on('change', function(){
            var $form = $('form#quote-form');
            $("form#quote-form :input").attr("disabled", true);
            $.get( "contacts/view/"+ $(this).val(), function(data) {
                $form.populate(data);
                $("form#quote-form :input").attr("disabled", false);
            });
        })
        $('input.amount').keyup(function(event) {

            // skip for arrow keys
            if(event.which >= 37 && event.which <= 40) return;

            // format number
            $(this).val(function(index, value) {
                return value
                        .replace(/\D/g, "")
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                        ;
            });
        });

    });
</script>


