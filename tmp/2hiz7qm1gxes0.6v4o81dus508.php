<div class="col-md-12">
    <div class="panel panel-visible" id="spy4">
        <div class="panel-heading">
            <div class="panel-title hidden-xs">
                <span class="glyphicon glyphicon-tasks"></span><?php echo $page['title']; ?></div>
        </div>
        <div class="panel-menu"></div>
        <div class="panel-body pn">
            <table class="table table-striped table-hover display" id="datatable6" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Contact Name</th>
                    <th>Status</th>
                    <th><?php echo Customlang::process('Opportunity Type'); ?></th>
                    <th>Value</th>
                    <th>Assignee</th>
                    <th>Created</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (($list?:array()) as $quotation): ?>
                    <tr>
                        <td><?php echo $quotation->opportunity->contact->fullnames; ?></td>
                        <td><?php echo $quotation->opportunity->status->title; ?></td>
                        <td><?php echo $quotation->opportunity->producttype->title; ?></td>
                        <td><?php echo Currency::format(@$quotation->amount); ?></td>
                        <td><?php echo @$quotation->opportunity->assignee->fullnames; ?></td>
                        <td><time class="cw-relative-date" datetime="<?php echo @$quotation->opportunity->datecreated; ?>"></time></td>
                        <td><a href="/opportunity/<?php echo $quotation->opportunity->id; ?>/quotations/view/<?php echo $quotation->id; ?>" >View</a></td>
                    </tr>
                <?php endforeach; ?>


                </tbody>
            </table>
        </div>
    </div>
</div>

