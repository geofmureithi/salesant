<div class="popup-basic panel popup-lg">
    <form method="post" action="<?php echo $form['action']; ?>" class="ajaxable">
    <div class="panel-heading">
        <ul class="nav panel-tabs-border panel-tabs panel-tabs-left">
            <li class="active">
                <a href="#tab2_1" data-toggle="tab">Opportunity Details</a>
            </li>
            <li>
                <a href="#tab2_2" data-toggle="tab"><?php echo Customlang::process('Products'); ?></a>
            </li>
        </ul>
    </div>
    <div class="panel-body">
        <div class="tab-content pn br-n">
            <div id="tab2_1" class="tab-pane active">
                <div class="row">
                    <div class="section col-md-6">
                        <div class="form-group">
                            <label for="opportunity_type" class="control-label"><?php echo Customlang::process('Opportunity Type'); ?></label>
                            <div class="bs-component">
                                <select id="opportunity_type" required="required" name="opportunity[producttype]" class="select2-single form-control">
                                <?php foreach($producttypes as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['opportunity']['producttype']) && $POST['opportunity']['producttype']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Status</label>
                            <div class="bs-component">
                                <select name="opportunity[status]" required="required" class="select2-single form-control">
                                <?php foreach($statuses as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['opportunity']['status']) && $POST['opportunity']['status']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="opportunity_value" class="control-label">Value</label>
                            <div class="bs-component">
                                <div class="input-group">
                                <span class="input-group-addon">
                                    Ksh.
                                </span>
                                    <input name="opportunity[amount]" id="opportunity_value" class="form-control" type="number" value="<?php echo isset($POST['opportunity']['amount'])?$POST['opportunity']['amount']:''; ?>" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="opportunity_closedate" class="control-label">Estimated Close Date</label>
                            <div class="bs-component">
                                <div class="input-group simpledate" id="opportunity_closedate">
                                <span class="input-group-addon cursor">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                    <input type="text" name="opportunity[expecteddate]" class="form-control" required style="background-color: #fff !important; cursor: text !important;" value="<?php echo isset($POST['opportunity']['expecteddate'])?$POST['opportunity']['expecteddate']:''; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section col-md-6">

                        <div class="form-group">
                            <label class="control-label">Confidence: <span id="percentage" style="color:#f6931f;"><?php echo @$POST['opportunity']['confidence']?$POST['opportunity']['confidence']:'50'; ?>%</span></label>
                            <div class="admin-form">
                                <div class="slider-wrapper slider-primary">
                                    <div id="confidence-count"></div>
                                </div>
                            </div>
                            <input type="hidden" name="opportunity[confidence]" id="opportunity_confidence" value="50" class="form-control" />
                        </div>

                        <div class="form-group">
                            <label  class="control-label">Contact</label>
                            <div class="bs-component">
                                <select name="opportunity[contact]" required="required" class="select2-single form-control">
                                <?php foreach($contacts as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['opportunity']['contact']) && $POST['opportunity']['contact']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Assign to</label>
                            <div class="bs-component">
                                <select name="opportunity[assignee]" required="required" class="select2-single form-control">
                                <?php foreach($assignees as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['opportunity']['assignee']) && $POST['opportunity']['assignee']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="opportunity_comments" class="control-label">Comments</label>
                            <div class="bs-component">
                                <textarea name="opportunity[description]" class="form-control" id="opportunity_comments" rows="3"></textarea>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div id="tab2_2" class="tab-pane">
                <div class="row">
                    <div class="section col-md-12">
                        <div class="panel">
                            <div class="panel-body">
                                    <table class="table" id="datatable5">
                                        <thead>
                                        <tr class="bg-light">
                                            <th class="text-center">Select</th>
                                            <th class=""><?php echo Customlang::process('Product Title'); ?></th>
                                            <th class="">Unit</th>
                                            <th class="">Price</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach (($products?:array()) as $product): ?>
                                        <tr>
                                            <td class="text-center">
                                                <label class="option block mn">
                                                    <input <?php echo $opportunity->organisation->settings['products']=='single'?'type="radio"':'type="checkbox"'; ?> name="opportunity[products][]" value="<?php echo $product->id; ?>" <?php echo \Models\Opportunity::hasProduct($PARAMS['id'],$product->id)?'checked':''; ?> />
                                                    <span class="checkbox mn"></span>
                                                </label>
                                            </td>
                                            <td class=""><?php echo $product->title; ?></td>
                                            <td class=""><?php echo $product->unit; ?></td>
                                            <td class=""><?php echo Currency::format($product->price); ?></td>
                                        </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- end .form-footer section -->
    </div>
        <div class="panel-footer">
            <?php if ($PARAMS['mothercontroller']): ?>
                
                    <input type="hidden" name="opportunity[lead]" value="<?php echo $PARAMS['motherid']; ?>" />
            <?php endif; ?>
            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
        </div>
    </form>
</div>

