<div class="clearfix"></div>
<div id="newsletter-form">
    <div class="panel">
        <div class="panel-heading">
            <span class="panel-title">
                <i class="fa fa-building"></i><?php echo Customlang::process($page['title']); ?>
            </span>
        </div>
        <form method="post" action="<?php echo $form['action']; ?>">
            <div class="panel-body p25">

                <div class="section" style="min-height: 90px;">
                    <div class="form-group col-md-12">
                        <label for="title" class="control-label"><?php echo Customlang::process('Newsletter'); ?> Group Name</label>
                        <div class="bs-component">
                            <input type="text" name="newsletter[title]" id="title" class="form-control" required value="<?php echo isset($POST['newsletter']['title'])?$POST['newsletter']['title']:''; ?>" />
                        </div>
                    </div>


                </div>
                <div class="form-group text-center">
                    <label class="control-label text-left" style="display: block">Newsletter Contacts Type</label>
                    <div class="radio-custom mb10" style="display: inline">
                        <input type="radio" id="algorithm" value="algorithm" name="newsletter[type]" data-storage="false" checked <?php echo (isset($POST['newsletter']['type']) && $POST['newsletter']['type']=='algorithm')?'checked="checked"':''; ?> />
                        <label for="algorithm">From Database </label>
                    </div>
                    <div class="radio-custom mb10" style="display: inline">
                        <input type="radio" id="file" value="file" name="newsletter[type]" data-storage="false" <?php echo (isset($POST['newsletter']['type']) && $POST['newsletter']['type']=='file')?'checked="checked"':''; ?> />
                        <label for="file">Import From File </label>
                    </div>
                </div>
                <div class="algorithm panel" style="display: block">
                    <div class="panel-heading">
                        <h5>Select Your Criteria</h5>
                    </div>
                    <div class="panel-body">
                        <div class="form-group col-md-12">
                            <label for="status" class="control-label">Lead/Contact Status</label>
                            <div class="bs-component">
                                <select name="algorithm[leadStatus]" id="status" class="form-control">
                                <?php foreach($leads['statuses']?$leads['statuses']:array() as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['algorithm']['leadStatus']) && $POST['algorithm']['leadStatus']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="source" class="control-label">Lead/Contact Source</label>
                            <div class="bs-component">
                                <select name="algorithm[leadSource]" id="source" class="form-control">
                                <?php foreach($leads['sources']?$leads['sources']:array() as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['algorithm']['leadSource']) && $POST['algorithm']['leadSource']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="form-group col-md-12">
                                <label for="opportunity" class="control-label"><?php echo Customlang::process('Opportunities'); ?></label>
                                <div class="bs-component">
                                    <div class="checkbox-custom mb5">
                                        <input type="checkbox" name="algorithm[hasOpportunity]" value="1" id="opportunity" <?php echo (isset($POST['algorithm']['hasOpportunity']) && $POST['algorithm']['hasOpportunity']=='1')?'checked="checked"':''; ?> />
                                        <label for="opportunity">Contact Has Opportunity</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="status" class="control-label">Opportunity Status</label>
                            <div class="bs-component">
                                <select name="algorithm[opportunityStatus]" id="opportunitystatus" class="form-control">
                                <?php foreach($opportunity['statuses']?$opportunity['statuses']:array() as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['algorithm']['opportunityStatus']) && $POST['algorithm']['opportunityStatus']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="source" class="control-label"><?php echo Customlang::process('Product Type'); ?></label>
                            <div class="bs-component">
                                <select name="algorithm[productType]" id="productType" class="form-control"><?php foreach($producttypes?$producttypes:array() as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['algorithm']['productType']) && $POST['algorithm']['productType']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="source" class="control-label"><?php echo Customlang::process('Sales'); ?></label>
                            <div class="bs-component">
                                <div class="checkbox-custom mb5">
                                    <input type="checkbox" name="algorithm[hasSale]" value="1" id="sales" <?php echo (isset($POST['algorithm']['hasSale']) && $POST['algorithm']['hasSale']=='1')?'checked="checked"':''; ?> />
                                    <label for="sales">Has Ever made a Sale with our Organisation</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="source" class="control-label"><?php echo Customlang::process('Email'); ?></label>
                            <div class="bs-component">
                                <div class="checkbox-custom mb5">
                                    <input type="checkbox" name="algorithm[hasEmail]" value="1" id="emails" <?php echo (isset($POST['algorithm']['hasEmail']) && $POST['algorithm']['hasEmail']=='1')?'checked="checked"':''; ?> />
                                    <label for="emails">Sent or Received an Email</label>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="file panel" style="display: none">
                    <div class="panel-heading">
                        <h5>Or Upload A CSV File</h5>
                    </div>
                    <div class="panel-body">
                        <div class="dropzone" id="importnewsletter">

                        </div>
                        <div class="clearfix"><br/><br/></div>
                        <p class="well">
                            Please maintain a simple format for the CSV as Shown below (Contact Name and Email are madatory or the line is ignored)<br/>
                            -----------------------------------------------------------------<br/>
                            Contact Names | Email | Phone (Optional)<br/>
                            -----------------------------------------------------------------<br/>
                        </p>
                    </div>

                </div>



                <!-- end section -->

            </div>
            <!-- end .form-body section -->

            <div class="panel-footer">
                <?php if ($PARAMS['id']): ?>
                    
                        <input id="newsletter_id" type="hidden" name="newsletter[id]" value="<?php echo $PARAMS['id']; ?>" />
                <?php endif; ?>
                <button type="submit" class="btn btn-sm btn-primary">Submit</button>
            </div>
            <!-- end .form-footer section -->
        </form>
    </div>
    <!-- end: .panel -->
</div>
<!-- end: .contact-form -->
<script>
    $(function(){
        $('input[name="newsletter[type]"]').trigger('change');
        $('input[name="newsletter[type]"]').on('change',function(){

            var $class = $(this).val();
            if($class == 'algorithm'){
                $('div.algorithm').show();
                $('div.file').hide();
            }
            if($class == 'file'){
                $('div.algorithm').hide();
                $('div.file').show();
            }

        })
    })
</script>

