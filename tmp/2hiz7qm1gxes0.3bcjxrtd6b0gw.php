    <div id="content" class="table-layout animated fadeIn">
        <!-- begin: .tray-right -->
        <?php echo $this->render('organisations/aside.html',$this->mime,get_defined_vars()); ?>
        <!-- end: .tray-right -->
        <!-- begin: .tray-center -->
        <div class="tray tray-center">
            <div class="row">
                <div class="col-md-9">
                    <div class="panel">
                        <div class="panel-heading">
                            <span class="panel-title"><?php echo Customlang::process('Product Types'); ?></span>
                            <a class="ajax-modal pull-right" href="/organisation/<?php echo $org->id; ?>/producttypes/add">
                                <i class="fa fa-plus"></i> Add
                            </a>
                        </div>
                        <div class="panel-body pn">

                            <div class="leadstatus bs-component">
                                <table class="table table-striped">
                                    <tbody>
                                    <?php foreach (($org->producttypes?:array()) as $type): ?>
                                        <tr>
                                            <td><?php echo $type->title; ?></td>
                                            <td class="text-right">
                                                <a href="/producttypes/edit/<?php echo $type->id; ?>" class="ajax-modal"><span class="fa fa-pencil"></span> Edit </a> &nbsp;&nbsp;
                                                <a href="/producttypes/del/<?php echo $type->id; ?>"><span class="fa fa-trash"></span> Delete </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>

                                    </tbody>
                                </table>


                            </div>
                        </div>

                    </div>
                    <div class="panel">
                        <div class="panel-heading">
                            <span class="panel-title">Lead Statuses</span>
                            <a class="ajax-modal pull-right" href="/organisation/<?php echo $org->id; ?>/leadstatuses/add">
                                <i class="fa fa-plus"></i> Add
                            </a>
                        </div>
                        <div class="panel-body pn">

                            <div class="leadstatus bs-component">
                                <table class="table table-striped">
                                    <tbody>
                                    <?php foreach (($org->leadstatuses?:array()) as $status): ?>
                                        <tr>
                                            <td><?php echo $status->title; ?></td>
                                            <td class="text-right">
                                                <a href="/leadstatuses/edit/<?php echo $status->id; ?>" class="ajax-modal"><span class="fa fa-pencil"></span> Edit </a> &nbsp;&nbsp;
                                                <a href="/leadstatuses/del/<?php echo $status->id; ?>"><span class="fa fa-trash"></span> Delete </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>

                                    </tbody>
                                </table>


                            </div>
                        </div>

                    </div>
                    <div class="panel">
                        <div class="panel-heading">
                            <span class="panel-title">Opportunity Statuses</span>
                            <a class="ajax-modal pull-right" href="/organisation/<?php echo $org->id; ?>/opportunitystatuses/add">
                                <i class="fa fa-plus"></i> Add
                            </a>
                        </div>
                        <div class="panel-body pn">

                            <div class="leadstatus bs-component">
                                <table class="table table-striped">
                                    <tbody>
                                    <?php foreach (($org->opportunitystatuses?:array()) as $status): ?>
                                        <tr>
                                            <td><?php echo $status->title; ?></td>
                                            <td class="text-right">
                                                <a href="/opportunitystatuses/edit/<?php echo $status->id; ?>" class="ajax-modal"><span class="fa fa-pencil"></span> Edit </a> &nbsp;&nbsp;
                                                <a href="/opportunitystatuses/del/<?php echo $status->id; ?>"><span class="fa fa-trash"></span> Delete </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>

                                    </tbody>
                                </table>


                            </div>
                        </div>

                    </div>
                    <div class="panel">
                        <div class="panel-heading">
                            <span class="panel-title">Lead Sources</span>
                            <a class="ajax-modal pull-right" href="/organisation/<?php echo $org->id; ?>/leadsources/add">
                                <i class="fa fa-plus"></i> Add
                            </a>
                        </div>
                        <div class="panel-body pn">

                            <div class="bs-component">
                                <table class="table table-striped">
                                    <tbody>
                                    <?php foreach (($org->leadsources?:array()) as $source): ?>
                                        <tr>
                                            <td><?php echo $source->title; ?></td>
                                            <td class="text-right">
                                                <a href="/leadsources/edit/<?php echo $source->id; ?>" class="ajax-modal"><span class="fa fa-pencil"></span> Edit </a> &nbsp;&nbsp;
                                                <a href="/leadsources/del/<?php echo $source->id; ?>"><span class="fa fa-trash"></span> Delete </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>

                                    </tbody>
                                </table>


                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <!-- end: .tray-center -->


