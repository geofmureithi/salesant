<aside class="tray tray-left tray300">

    <!-- User Avatar Media -->

    <!-- Organization Settings Panel -->

    <div class="panel panel-warning panel-border top mt30">
        <div class="panel-body bg-light p10">
            <div class="list-group list-group-links list-group-spacing-sm mbn">
                <div class="list-group-header"> <?php echo $org->name; ?> </div>
                <a href="/organisations/edit/<?php echo $org->id; ?>" class="list-group-item"> Edit Organisation </a>
                <a href="#" class="list-group-item"> Team </a>
                <a href="/customfields" class="list-group-item"> Custom Fields </a>
                <a href="/organisations/language/<?php echo $org->id; ?>" class="list-group-item"> Language </a>
            </div>
        </div>
    </div>
</aside>