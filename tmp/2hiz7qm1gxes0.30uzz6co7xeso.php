<!-- end: .task-form -->
<div id="note-form" class="popup-basic mfp-with-anim">
    <div class="panel">
        <div class="panel-heading">
            <span class="panel-title">
                <i class="fa fa-building"></i>Add Note
            </span>
        </div>
        <!-- end .panel-heading section -->
        <div id="note-results"></div>
        <form method="post" action="<?php echo $form['action']; ?>" class="ajaxable" id="create-transcript">
            <div class="panel-body p25">
                <div class="section">
                    <div class="form-group">
                        <label for="note_text" class="control-label">Description</label>
                        <div class="bs-component">
                            <textarea name="transcript[description]" class="form-control" id="note_text" rows="6"></textarea>
                        </div>
                    </div>

                </div>
                <!-- end section -->

            </div>
            <!-- end .form-body section -->

            <div class="panel-footer">
                <?php if ($PARAMS['mothercontroller']): ?>
                    
                        <input type="hidden" name="transcript[sale]" value="<?php echo $PARAMS['motherid']; ?>" />
                    
                <?php endif; ?>
                <button type="submit" class="btn btn-sm btn-primary">Submit</button>
            </div>
            <!-- end .form-footer section -->
        </form>
    </div>
    <!-- end: .panel -->
</div>
<!-- end: .note-form -->


