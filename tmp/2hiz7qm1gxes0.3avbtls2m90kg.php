
<div class="col-md-9">
    <div class="panel panel-visible" id="spy4">
        <div class="panel-heading">
            <div class="panel-title">
                <span class="glyphicon glyphicon-tasks"></span><?php echo Customlang::process('Products'); ?> Catalogue
            </div>
        </div>
        <div class="panel-menu text-right">
            <a href="/products/add" class="btn btn-sm btn-alert"> <i class="fa fa-plus-circle"></i> Add <?php echo Customlang::process('Product'); ?></a>
        </div>
        <div class="panel-body pn">
            <table class="table table-striped table-hover display" id="datatable" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Status</th>
                    <th><?php echo Customlang::process('Product Type'); ?></th>
                    <th>Price</th>
                    <th>Unit</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (($list?:array()) as $product): ?>
                    <tr>
                        <td><?php echo $product->title; ?></td>
                        <td><span class="label label-<?php echo $product->status->badge; ?>"><?php echo $product->status->title; ?></span></td>
                        <td><?php echo $product->type->title; ?></td>
                        <td><?php echo Currency::format($product->price); ?></td>
                        <td><?php echo $product->unit; ?></td>
                        <td>
                            <a data-toggle="tooltip" title="Edit <?php echo Customlang::process('Product'); ?>" href="/products/edit/<?php echo $product->id; ?>" class="iconLink" ><i class="fa fa-edit"></i> Edit</a>
                            <a data-toggle="tooltip" data-type="<?php echo Customlang::process('Product'); ?>" title="Delete <?php echo Customlang::process('Product'); ?>" class="delete iconLink" href="/products/delete/<?php echo $product->id; ?>" ><i class="fa fa-remove"></i> Delete</a>
                        </td>
                    </tr>
                <?php endforeach; ?>


                </tbody>
            </table>
        </div>
    </div>
</div>
<aside class="col-md-3">
    <form action="/products/filter" method="post">
    <div class="panel">
    <!-- Start: Sidebar Right Content -->
    <div class="sidebar-right-content nano-content p10 panel-body"> 

        <h5 class="title-divider text-muted mb20"> Quick Filter
        </h5>

        <!--h5 class="title-divider text-muted mt30 mb10">Traffic Margins</--h5-->
        <div class="row">
            <div class="form-group col-md-12">
                <label>Status</label>
                <div class="bs-component">
                    <?php foreach (($productStatuses?:array()) as $status): ?>
                        <span class="label label-<?php echo $status->badge; ?>"><input type="checkbox" name="filter[status][]" value="<?php echo $status->id; ?>" title="<?php echo $status->description; ?>" <?php echo (isset($POST['filter']['status']) && is_array($POST['filter']['status']) && in_array($status->id,$POST['filter']['status']))?'checked="checked"':''; ?> /><?php echo $status->title; ?></span>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="form-group col-md-12">
                <label>Price</label>
                <div class="bs-component">
                    <div class="col-md-6">
                        <input type="text" name="filter[priceStart]" placeholder="0" id="start" class="col-xs-6 form-control" value="<?php echo isset($POST['filter']['priceStart'])?$POST['filter']['priceStart']:''; ?>" />
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="filter[priceEnd]" placeholder="999999999999" id="end" class="col-xs-6 form-control" value="<?php echo isset($POST['filter']['priceEnd'])?$POST['filter']['priceEnd']:''; ?>" />
                    </div>


                </div>

            </div>
            <div class="form-group col-md-12">
                <label for="product_type" class="control-label"><?php echo Customlang::process('Product Type'); ?></label>
                <div class="bs-component">
                    <select id="product_type" placeholder="Choose <?php echo Customlang::process('Product Type'); ?>" name="filter[type]" class="select2-single form-control">
                        <option>Choose <?php echo Customlang::process('Product Type'); ?></option>
                    <?php foreach($types as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['filter']['type']) && $POST['filter']['type']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                </div>
            </div>
            <div class="form-group col-md-12" id="cf">
                <label class="control-label">Custom Fields</label>
                <div class="form-group customfields">

                </div>

            </div>
            <div class="form-group col-md-12">
                <label class="control-label">Settings <small class="label label-warning">Custom <?php echo Customlang::process('Product'); ?> Settings</small></label>
                <div class="checkbox-custom fill checkbox-primary mb5">
                    <input type="checkbox" name="filter[isService]" id="service" <?php echo (isset($POST['filter']['isService']) && $POST['filter']['isService']=='on')?'checked="checked"':''; ?> />
                    <label for="service">Is a Service?</label>
                </div>
                <div class="checkbox-custom fill checkbox-primary mb5">
                    <input type="checkbox" name="filter[isResale]" id="resale" <?php echo (isset($POST['filter']['isResale']) && $POST['filter']['isResale']=='on')?'checked="checked"':''; ?> />
                    <label for="resale">Can be Resold?</label>
                </div>
            </div>
        </div>



    </div>
        <div class="panel-footer text-right">
            <a class="btn btn-sm btn-warning" href="/products">Reset</a><button type="submit" class="btn btn-sm btn-primary">Filter</button>
        </div>
        <script>
            $(function(){
                var customdata = <?php echo json_encode($POST['custom']); ?>;
                function loadFields(){
                        $.post('/producttypes/customfields/'+$('select#product_type').val()+'?full=true&handler=filter', customdata).done(function(res){
                            $('#cf').hide();
                            var html = $.parseHTML(res.fields.template);
                            $(".customfields").html(html);
                            if(html != null)
                                $('#cf').show('slow');

                        });
                }
                $('select#product_type').on('change',function(e){
                    loadFields();
                });
                loadFields();
            });
        </script>

    </div>
    </form>
</aside>
