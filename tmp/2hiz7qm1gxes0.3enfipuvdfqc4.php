<!-- Contact Form Popup -->
<div class="clearfix"></div>
<div id="newsletter-form">
    <div class="panel">
        <div class="panel-heading">
            <span class="panel-title">
                <i class="fa fa-building"></i><?php echo Customlang::process($page['title']); ?>
            </span>
        </div>
        <form method="post" action="<?php echo $form['action']; ?>">
            <div class="panel-body p25">

                <div class="section" style="min-height: 90px;">
                    <div class="form-group col-md-12">
                        <label for="title" class="control-label"><?php echo Customlang::process('Newsletter'); ?> Template Name</label>
                        <div class="bs-component">
                            <input type="text" name="newslettertemplate[title]" id="title" class="form-control" required value="<?php echo isset($POST['newslettertemplate']['title'])?$POST['newslettertemplate']['title']:''; ?>" />
                        </div>
                    </div>


                </div>
                <div class="form-group col-md-12">
                    <label class="control-label">Template</label>
                    <div class="bs-component">
                        <textarea class="summernote" id="templatetext" name="newslettertemplate[template]"><?php echo @$POST['newslettertemplate']['template']; ?></textarea>
                    </div>
                </div>


                <!-- end section -->

            </div>
            <!-- end .form-body section -->

            <div class="panel-footer">
                <?php if ($PARAMS['id']): ?>
                    
                        <input id="newslettertemplate_id" type="hidden" name="newslettertempate[id]" value="<?php echo $PARAMS['id']; ?>" />
                <?php endif; ?>
                <button type="submit" class="btn btn-sm btn-primary">Submit</button>
            </div>
            <!-- end .form-footer section -->
        </form>
    </div>
    <!-- end: .panel -->
</div>
<!-- end: .contact-form -->
<script>
    $(function(){
        $('input[name="newsletter[type]"]').trigger('change');
        $('input[name="newsletter[type]"]').on('change',function(){

            var $class = $(this).val();
            if($class == 'algorithim'){
                $('div.algorithm').show();
                $('div.file').hide();
            }
            if($class == 'file'){
                $('div.algorithm').hide();
                $('div.file').show();
            }

        })
    })
</script>

