<div class="clearfix"></div>
<div class="row">
    <div class="col-md-7">
        <div class="panel pn">
            <div class="panel-heading">
                <span class="panel-title">Client</span>
                <div class="widget-menu pull-right mr10">
                    <div class="progress progress-bar-sm" style="width: 120px;">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $sale->amount?number_format($sale->paid * 100/$sale->amount):0; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo number_format($sale->paid * 100/$sale->amount); ?>%">
                            <span class="sr-only"><?php echo $sale->amount? number_format($sale->paid * 100/$sale->amount):0; ?>% Paid</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-md-2">
                    <img src="<?php echo $sale->contact->gravatar; ?>" alt="avatar" >
                </div>
                <div class="well well-sm col-md-10" style="padding: 0px; padding-left: 10px">
                    <address>
                        <h4><?php echo $sale->contact->fullnames; ?></h4>
                        <?php echo $sale->contact->email; ?>
                        <br>
                        <abbr title="Phone">Tel: </abbr><?php echo $sale->contact->telephone; ?>
                    </address>
                </div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-heading">
              <span class="panel-title">
                <span class="fa fa-archive"></span><?php echo Customlang::process('Products'); ?></span>
            </div>

            <div class="panel-body pn">
                <div class="bs-component">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th><?php echo Customlang::process('Product'); ?></th>
                            <th><?php echo Customlang::process('Product Type'); ?></th>
                            <th>Price</th>
                            <th>Unit</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=0; foreach (($sale->products?:array()) as $product): $i++; ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $product->title; ?></td>
                                <td><?php echo $product->type->title; ?></td>
                                <td><?php echo Currency::format($product->price); ?></td>
                                <td><?php echo $product->unit; ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-heading">
              <span class="panel-title">
                <span class="fa fa-bank"></span>Payments Made</span>
                <span class="pull-right">
                        <a class="ajax-modal" href="/sale/<?php echo $sale->id; ?>/payments/add">
                            <i class="fa fa-plus"></i> Add
                        </a>
                </span>
            </div>

            <div class="panel-body pn">
                <div class="bs-component">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Date</th>
                            <th>Receipt No</th>
                            <th>Bank</th>
                            <th>Cheque No</th>
                            <th class="text-right">Amount</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=0; foreach (($sale->payments?:array()) as $payment): $i++; ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $payment->datepaid; ?></td>
                                <td><?php echo $payment->receiptNo; ?></td>
                                <td><?php echo $payment->bank; ?></td>
                                <td><?php echo $payment->chequeNo; ?></td>
                                <td class="text-right"><?php echo Currency::format($payment->amount); ?></td>
                                <td>
                                    <a data-toggle="tooltip" title="Edit Payment" class="ajax-modal" href="/sale/<?php echo $sale->id; ?>/payments/edit/<?php echo $payment->id; ?>"><i class="fa fa-edit"></i></a>&nbsp;
                                    <a data-toggle="tooltip" title="Delete Payment" class="delete" href="/sale/<?php echo $sale->id; ?>/payments/delete/<?php echo $payment->id; ?>"><i class="fa fa-remove"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-heading">
              <span class="panel-title">
                <span class="fa fa-money"></span>Payment Plan</span>
            </div>
            <div class="panel-body pn">
                <div class="bs-component">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Amount</th>
                            <th>Date</th>
                            <th>Remarks</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=0; foreach (($sale->quotation->paymentPlan?:array()) as $paymentplan): $i++; ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td class="text-right text-danger"><?php echo Currency::format($paymentplan->amount); ?></td>
                                <td><?php echo $paymentplan->datedue; ?></td>
                                <td><?php echo $paymentplan->description; ?></td>
                                <td><a target="_blank" data-toggle="tooltip" title="Generate Invoice"  href="/sales/plans/<?php echo $paymentplan->id; ?>"><i class="fa fa-file"></i></a></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>
    <div class="col-md-5">
        <div class="panel">
            <div class="panel-heading">
    <span class="panel-icon">
      <i class="fa fa-clock-o"></i>
    </span>
                <span class="panel-title"> Payments Timeline</span>
                <span class="pull-right">
                        <a class="ajax-modal" href="/sale/<?php echo $sale->id; ?>/transcripts/add">
                            <i class="fa fa-plus"></i> Add Note
                        </a>
                </span>
            </div>
            <div class="panel-body ptn pbn p10">
                <ol class="timeline-list">
                    <?php foreach (($sale->allactivities?:array()) as $transcript): ?>
                        <li class="timeline-item">
                            <div class="timeline-icon <?php echo $transcript->bgClass; ?>">
                                <span class="<?php echo $transcript->icon; ?>"></span>
                            </div>
                            <div class="timeline-desc">
                                <b><?php echo $transcript->type== 'note'? $transcript->creator->firstname .' :' : ''; ?><?php echo $transcript->type== 'payment'? $sale->contact->firstname .' :' : ''; ?></b> <?php echo $transcript->description; ?>
                            </div>
                            <time class="cw-relative-date" datetime="<?php echo $transcript->datecreated; ?>"></time>
                        </li>
                    <?php endforeach; ?>

                </ol>
            </div>
        </div>
    </div>

</div>