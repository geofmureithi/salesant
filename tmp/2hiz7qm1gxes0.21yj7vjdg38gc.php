<?php foreach (($fields?:array()) as $field): ?>

    <div class="form-group <?php echo $colSize; ?> <?php echo $GET['handler']=='filter' && $field->default?'hidden':''; ?>">
        <label for="<?php echo $field->slug; ?>" class="control-label"><?php echo $field->name; ?></label>
        <div class="bs-component">
            <input type="text" <?php echo $GET['handler']=='filter' && $field->default?'disabled':''; ?> placeholder="<?php echo $field->name; ?>" name="custom[<?php echo $field->id; ?>]" id="<?php echo $field->slug; ?>" value="<?php echo $field->customValue($prod)?$field->customValue($prod):$field->default; ?>" class="form-control" />
        </div>
    </div>
<?php endforeach; ?>
