<div class="col-md-12">
    <div class="panel panel-visible" id="spy4">
        <div class="panel-heading">
            <div class="panel-title">
                <span class="glyphicon glyphicon-tasks"></span><?php echo $page['title']; ?></div>
        </div>
        <div class="panel-menu text-right">
            <a href="/quotations/quick" class="btn btn-info btn-sm text-center">
                <span class="fa fa-plus pr5"></span> Quick Create</a>
        </div>
        <div class="panel-body pn">
            <table class="table table-striped table-hover display" id="datatable" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Contact Name</th>
                    <th>Status</th>
                    <th><?php echo Customlang::process('Opportunity Type'); ?></th>
                    <th>Value</th>
                    <th>Assignee</th>
                    <th>Created</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (($list?:array()) as $quotation): ?>
                    <tr>
                        <td><?php echo $quotation->opportunity->contact->fullnames; ?></td>
                        <td><?php echo $quotation->opportunity->status->title; ?></td>
                        <td><?php echo $quotation->opportunity->producttype->title; ?></td>
                        <td><?php echo Currency::format(@$quotation->amount); ?></td>
                        <td><?php echo @$quotation->opportunity->assignee->fullnames; ?></td>
                        <td><time class="cw-relative-date" datetime="<?php echo @$quotation->opportunity->datecreated; ?>"></time></td>
                        <td>
                            <a data-toggle="tooltip" data-title="View <?php echo Customlang::process('Quotation'); ?>" class="iconLink" href="/opportunity/<?php echo $quotation->opportunity->id; ?>/quotations/view/<?php echo $quotation->id; ?>"><i class="fa fa-search-plus"></i> View</a> |
                            <a data-toggle="tooltip" data-type="Quotation" data-title="Delete <?php echo Customlang::process('Quotation'); ?>" class="delete iconLink" href="/opportunity/<?php echo $quotation->opportunity->id; ?>/quotations/delete/<?php echo $quotation->id; ?>"><i class="fa fa-remove"></i> Delete</a>
                        </td>
                    </tr>
                <?php endforeach; ?>


                </tbody>
            </table>
        </div>
    </div>
</div>

