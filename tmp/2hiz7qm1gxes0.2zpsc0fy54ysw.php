
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-heading">
                <span class="panel-icon"><i class="fa fa-bank"></i></span>
                <span class="panel-title">Organisations</span>
            </div>
            <div class="panel-menu text-right">
                <a href="http://<?php echo $subdomain; ?>.<?php echo DOMAIN; ?>/organisations/add" class="btn btn-default"> <i class="fa fa-plus-circle"></i> Add Organisation</a>
            </div>
            <div class="panel-body">
                <table class="table table-striped mfp-no-margins table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i=0; foreach (($organisations?:array()) as $organisation): $i++; ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $organisation->name; ?></td>
                            <td><a data-toggle="tooltip" data-title="View Organisations" href="http://<?php echo $subdomain; ?>.<?php echo DOMAIN; ?>/organisations/view/<?php echo $organisation->id; ?>"><i class="fa fa-plus"></i> View</a></td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
            <div class="panel-footer">Total: <?php echo $organisations?count($organisations):0; ?> organisation(s)</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-tile text-warning light">
            <div class="panel-body pn pl20 p5">
                <i class="fa fa-clock-o icon-bg"></i>
                <h2 class="mt15 lh15">
                    <b><?php echo date_format(date_create($account->expires),"Y/m/d"); ?></b>
                </h2>
                <h5 class="text-muted">Subscription Expires</h5>
            </div>
        </div>
        <div class="panel panel-tile text-system light">
            <div class="panel-body pn pl20 p5">
                <i class="fa fa-envelope-o icon-bg"></i>
                <h2 class="mt15 lh15">
                    <b data-toggle="tooltip" data-title="[Leads + Newsletters + Sales]">PROFESSIONAL</b>
                </h2>
                <h5 class="text-muted">Account Type</h5>
            </div>
        </div>
        <div class="panel panel-tile text-primary light">
            <div class="panel-body pn pl20 p5">
                <i class="fa fa-bank icon-bg"></i>
                <h2 class="mt15 lh15">
                    <b><?php echo $organisations?count($organisations):0; ?></b>
                </h2>
                <h5 class="text-muted">Organisation(s)</h5>
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-8">
        <div class="panel panel-widget draft-widget">
            <div class="panel-heading">
    <span class="panel-icon">
      <i class="fa fa-pencil"></i>
    </span>
                <span class="panel-title"> Quick Contact</span>
            </div>
            <div class="panel-body p20">
                <p class="well-sm"> Use this form to ask a question or request support</p>
                <div class="admin-form theme-primary">
                    <div class="section mb20">
                        <input type="text" id="draft-title" class="gui-input bg-light form-control" placeholder="Subject" />
                    </div>
                    <div class="section">
                            <textarea class="form-control" id="comment" name="comment" placeholder="Message here..." rows="5"></textarea>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-primary btn-sm ph15" type="button">Send</button>
            </div>
        </div>
    </div>
</div>