<div class="col-md-10">
    <div class="panel panel-visible" id="spy4">
        <div class="panel-heading">
            <div class="panel-title hidden-xs">
                <span class="glyphicon glyphicon-tasks"></span><?php echo $page['title']; ?></div>
        </div>
        <div class="panel-menu"></div>
        <div class="panel-body pn">
            <table class="table table-striped table-hover display" id="datatable6" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Name</th>
                    <th><?php echo Customlang::process('Product Type'); ?></th>
                    <th>Default Value</th>
                    <th>Description</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (($list?:array()) as $field): ?>
                    <tr>
                        <td><?php echo $field->name; ?></td>
                        <td><?php echo $field->producttype->title; ?></td>
                        <td><?php echo $field->default; ?></td>
                        <td><?php echo $field->description; ?></td>
                        <td><a href="/customfields/edit/<?php echo $field->id; ?>" class="ajax-modal" >Edit</a> </td>
                    </tr>
                <?php endforeach; ?>


                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="col-md-2 text-center">
    <a href="/customfields/add" class="btn btn-default btn-sm light fw600 ml10 ajax-modal text-center">
        <span class="fa fa-plus pr5"></span> Add New</a>
    <div class="clearfix"><br/></div>
    <div class="panel panel-tile text-center">
        <div class="panel-body bg-info">
            <h1 class="fs35 mbn"><?php echo $list?count($list):0; ?></h1>
            <h6 class="text-white">Custom Fields(s)</h6>
        </div>
        <div class="panel-footer br-n p12">
    <span class="fs11">
      <i class="fa fa-arrow-up text-info pr5"></i>
      <b></b>
    </span>
        </div>
    </div>
</div>



