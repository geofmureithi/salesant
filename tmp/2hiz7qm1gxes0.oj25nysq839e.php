<!-- Contact Form Popup -->
<div id="product-form" class="popup-basic popup-lg">
    <div class="panel">
        <div class="panel-heading">
            <span class="panel-title">
                <i class="fa fa-building"></i><?php echo $page['title']; ?>
            </span>
        </div>
        <!-- end .panel-heading section -->
        <div id="contact-results"></div>
        <form method="post" action="<?php echo $form['action']; ?>" class="ajaxable">
            <div class="panel-body p25">

                <div class="section">
                    <div class="form-group col-md-12">
                        <label for="title" class="control-label">Product Type</label>
                        <div class="bs-component">
                            <input type="text" name="productType[title]" id="title" class="form-control" required value="<?php echo isset($POST['productType']['title'])?$POST['productType']['title']:''; ?>" />
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="description" class="control-label">Description</label>
                        <div class="bs-component">
                            <textarea  name="productType[description]" id="description" class="form-control"><?php echo @$POST['productType']['description']; ?></textarea>
                        </div>
                    </div>
                    <?php if ($PARAMS['mothercontroller']): ?>
                        
                            <input type="hidden" name="productType[organisation]" value="<?php echo $PARAMS['motherid']; ?>" />
                    <?php endif; ?>
                </div>
                <!-- end section -->

            </div>
            <!-- end .form-body section -->

            <div class="panel-footer">
                <button type="submit" class="btn btn-sm btn-primary">Submit</button>
            </div>
            <!-- end .form-footer section -->
        </form>
    </div>
    <!-- end: .panel -->
</div>
<!-- end: .contact-form -->


