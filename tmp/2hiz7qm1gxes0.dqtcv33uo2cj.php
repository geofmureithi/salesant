<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <span class="panel-icon"><i class="fa fa-bank"></i></span>
                <span class="panel-title">Organisations</span>
            </div>
            <div class="panel-menu text-right">
                <a href="http://<?php echo $subdomain; ?>.<?php echo DOMAIN; ?>/organisations/add" class="btn btn-default"> <i class="fa fa-plus-circle"></i> Add Organisation</a>
            </div>
            <div class="panel-body">
                <table class="table table-striped mfp-no-margins table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Leads</th>
                        <th>Users</th>
                        <th>Website</th>
                        <th>Creator</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i=0; foreach (($organisations?:array()) as $organisation): $i++; ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $organisation->name; ?></td>
                            <td><?php echo $organisation->leads?count($organisation->leads):0; ?></td>
                            <td><?php echo $organisation->users?count($organisation->users):0; ?></td>
                            <td><a target="_blank" href="<?php echo $organisation->website?$organisation->website:'http://'. $subdomain .'.'. DOMAIN; ?>"><?php echo $organisation->website?$organisation->website:'http://'. $subdomain .'.'. DOMAIN; ?></a></td>
                            <td><?php echo $organisation->creator->fullnames; ?></td>
                            <td><a href="http://<?php echo $subdomain; ?>.<?php echo DOMAIN; ?>/organisations/view/<?php echo $organisation->id; ?>"> <i class="fa fa-plus"></i>View</a></td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
            <div class="panel-footer">Total: <?php echo $organisations?count($organisations):0; ?> organisation(s)</div>
        </div>
    </div>

</div>