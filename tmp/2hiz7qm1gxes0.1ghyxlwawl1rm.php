<div class="col-md-10">
    <div class="panel panel-visible" id="spy4">
        <div class="panel-heading">
            <div class="panel-title hidden-xs">
                <span class="glyphicon glyphicon-tasks"></span><?php echo $page['title']; ?></div>
        </div>
        <div class="panel-menu"></div>
        <div class="panel-body pn">
            <table class="table table-striped table-hover display table-condensed" id="datatable6" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (($list?:array()) as $opportunityStatus): ?>
                    <tr>
                        <td><?php echo $opportunityStatus->title; ?></td>
                        <td><?php echo $opportunityStatus->description; ?></td>
                        <td><a href="/opportunitystatuses/edit/<?php echo $opportunityStatus->id; ?>" class="ajax-modal" >Edit</a> </td>
                    </tr>
                <?php endforeach; ?>


                </tbody>
            </table>
        </div>
    </div>
</div>

