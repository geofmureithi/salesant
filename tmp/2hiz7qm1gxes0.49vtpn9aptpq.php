
<!-- end: .note-form -->

<!-- Call Form Popup -->
<div id="call-form" class="popup-basic mfp-with-anim">
    <div class="panel">
        <div class="panel-heading">
            <span class="panel-title">
                <i class="fa fa-building"></i><?php echo $page['title']; ?>
            </span>
        </div>
        <!-- end .panel-heading section -->
        <div id="call-results"></div>
        <form method="post" action="<?php echo $form['action']; ?>" class="ajaxable" id="logcall">
            <div class="panel-body p25">

                <div class="section">

                    <div class="form-group">
                        <label for="call_text" class="control-label">Add a note about this call</label>
                        <div class="bs-component">
                            <textarea name="call[description]" class="form-control" id="call_text" rows="6"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="call_contact" class="control-label">Choose a Contact</label>
                        <div class="bs-component">
                            <select name="call[contact]" id="call_contact" class="select2-single form-control">
                            <?php foreach($contacts as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['call']['contact']) && $POST['call']['contact']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                        </div>
                    </div>



                </div>
                <!-- end section -->

            </div>
            <!-- end .form-body section -->

            <div class="panel-footer">
                <?php if ($PARAMS['mothercontroller']): ?>
                    
                        <input type="hidden" name="call[lead]" value="<?php echo $PARAMS['motherid']; ?>" />
                    
                <?php endif; ?>
                <button type="submit" class="btn btn-sm btn-primary">Submit</button>
            </div>
            <!-- end .form-footer section -->
        </form>
    </div>
    <!-- end: .panel -->
</div>
<!-- end: .call-form -->




