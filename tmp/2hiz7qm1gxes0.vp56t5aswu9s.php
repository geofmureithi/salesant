<div class="col-md-12">
    <div class="panel panel-visible" id="spy4">
        <div class="panel-heading">
            <div class="panel-title">
                <span class="glyphicon glyphicon-shopping-cart"></span><?php echo $page['title']; ?></div>
        </div>
        <div class="panel-body pn">
            <table class="table table-striped table-hover display" id="datatable" cellspacing="0">
                <thead>
                <tr>
                    <th>Client Name</th>
                    <th><?php echo Customlang::process('Products'); ?></th>
                    <th>Amount</th>
                    <th>Creator</th>
                    <th>Created</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (($list?:array()) as $sale): ?>
                    <tr>
                        <td><?php echo $sale->contact->fullnames; ?></td>
                        <td><?php echo $sale->products['0']->title; ?> <?php echo $sale->products?count($sale->products):0> 1?'+'.count($sale->products)-1:''; ?></td>
                        <td><?php echo Currency::format($sale->amount); ?></td>
                        <td><?php echo $sale->creator->fullnames; ?></td>
                        <td><time class="cw-relative-date" datetime="<?php echo $sale->datecreated; ?>"></time></td>
                        <td>
                            <a data-title="View Sale" data-toggle="tooltip" class="iconLink" href="/sales/view/<?php echo $sale->id; ?>"><i class="fa fa-search-plus"></i> View</a> |
                            <a data-type="Sale" data-title="Delete Sale" data-toggle="tooltip" class="delete iconLink" href="/sales/delete/<?php echo $sale->id; ?>"><i class="fa fa-remove"></i> Delete</a>
                        </td>
                    </tr>
                <?php endforeach; ?>


                </tbody>
            </table>
        </div>
    </div>
</div>