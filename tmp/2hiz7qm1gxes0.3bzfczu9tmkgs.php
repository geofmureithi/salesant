<div class="col-md-12">
    <div class="panel panel-visible" id="spy4">
        <div class="panel-heading">
            <div class="panel-title">
                <span class="glyphicon glyphicon-tasks"></span><?php echo $page['title']; ?></div>
        </div>
        <div class="panel-body pn">
            <table class="table table-striped table-hover display table-responsive" id="datatable" cellspacing="0">
                <thead>
                <tr>
                    <th>Contact Name</th>
                    <th class="hidden-sm hidden-xs">Status</th>
                    <th><?php echo Customlang::process('Opportunity Type'); ?></th>
                    <th>Value</th>
                    <th class="hidden-sm hidden-xs">Assignee</th>
                    <th class="hidden-sm hidden-xs">Created</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (($list?:array()) as $opportunity): ?>
                    <tr>
                        <td><?php echo $opportunity->contact->fullnames; ?></td>
                        <td class="hidden-sm hidden-xs"><?php echo $opportunity->status->title; ?></td>
                        <td><?php echo $opportunity->producttype->title; ?></td>
                        <td><?php echo Currency::format(@$opportunity->amount); ?></td>
                        <td class="hidden-sm hidden-xs"><?php echo $opportunity->assignee->fullnames; ?></td>
                        <td class="hidden-sm hidden-xs"><time class="cw-relative-date" datetime="<?php echo $opportunity->datecreated; ?>"></time></td>
                        <td><a data-toggle="tooltip" title="View <?php echo Customlang::process('Opportunity'); ?>" href="/leads/view/<?php echo $opportunity->lead->id; ?>" class="iconLink" ><i class="fa fa-search-plus"></i> View</a></td>
                    </tr>
                <?php endforeach; ?>


                </tbody>
            </table>
        </div>
    </div>
</div>

