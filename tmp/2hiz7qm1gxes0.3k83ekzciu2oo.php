<!-- Contact Form Popup -->
<div id="product-form" class="popup-basic popup-lg">
    <div class="panel">
        <div class="panel-heading">
            <span class="panel-title">
                <i class="fa fa-building"></i><?php echo Customlang::process($page['title']); ?>
            </span>
        </div>
        <!-- end .panel-heading section -->
        <div id="contact-results"></div>
        <form method="post" action="<?php echo $form['action']; ?>" class="ajaxable">
            <div class="panel-body p25">

                <div class="section">
                    <div class="form-group col-md-6">
                        <label for="title" class="control-label"><?php echo Customlang::process('Product Name'); ?></label>
                        <div class="bs-component">
                            <input type="text" name="product[title]" id="title" class="form-control" required value="<?php echo isset($POST['product']['title'])?$POST['product']['title']:''; ?>" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="product_type" class="control-label"><?php echo Customlang::process('Product Type'); ?></label>
                        <div class="bs-component">
                            <select id="product_type" name="product[type]" class="select2-single form-control">
                            <?php foreach($types as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['product']['type']) && $POST['product']['type']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="price" class="control-label">Price</label>
                        <div class="bs-component">
                            <input type="text" name="product[price]" id="price" class="form-control" required value="<?php echo isset($POST['product']['price'])?$POST['product']['price']:''; ?>" />
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="product_unit" class="control-label">Unit</label>
                        <div class="bs-component">
                            <input type="text" name="product[unit]" id="product_unit" class="form-control" value="<?php echo isset($POST['product']['unit'])?$POST['product']['unit']:''; ?>" />
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="product_description" class="control-label">Description</label>
                        <div class="bs-component">
                            <input type="text" name="product[description]" id="product_description" class="form-control" value="<?php echo isset($POST['product']['description'])?$POST['product']['description']:''; ?>" />
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label">Custom Data</label>
                        <div class="form-group customfields">

                        </div>

                    </div>
                    <!--div class="form-group col-md-12">
                        <label for="productphotos" class="control-label">Photos</label>
                        <div id="productphotos" class="dropzone">

                        </div>
                    </div-->

                </div>


                <!-- end section -->

            </div>
            <!-- end .form-body section -->

            <div class="panel-footer">
                <?php if ($PARAMS['id']): ?>
                    
                        <input id="product_id" type="hidden" name="product[id]" value="<?php echo $PARAMS['id']; ?>" />
                <?php endif; ?>
                <button type="submit" class="btn btn-sm btn-primary">Submit</button>
            </div>
            <!-- end .form-footer section -->
        </form>
    </div>
    <!-- end: .panel -->
</div>
<!-- end: .contact-form -->
<script>
    $(function(){
        function loadFields(){
           var product = $('#product_id').val();
            if(product > 0){
                $.get('/producttypes/customfields/'+$('select#product_type').val()+'?product='+product,function(res){
                    var html = $.parseHTML(res.fields.template);
                    $(".customfields").html(html);
                });
            } else{
                $.get('/producttypes/customfields/'+$('select#product_type').val(),function(res){
                    var html = $.parseHTML(res.fields.template);
                    $(".customfields").html(html);
                });
            }
        }
        $('select#product_type').on('change',function(e){
            loadFields();
        });
        loadFields();
    });
</script>


