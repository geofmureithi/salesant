<div class="col-md-10">
    <div class="panel panel-visible" id="spy4">
        <div class="panel-heading">
            <div class="panel-title hidden-xs">
                <span class="glyphicon glyphicon-tasks"></span><?php echo $page['title']; ?></div>
        </div>
        <div class="panel-menu"></div>
        <div class="panel-body pn">
            <table class="table table-striped table-hover display" id="datatable6" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Contact Name</th>
                    <th>Status</th>
                    <th><?php echo Customlang::process('Opportunity Type'); ?></th>
                    <th>Value</th>
                    <th>Assignee</th>
                    <th>Created</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (($list?:array()) as $opportunity): ?>
                    <tr>
                        <td><?php echo $opportunity->contact->fullnames; ?></td>
                        <td><?php echo $opportunity->status->title; ?></td>
                        <td><?php echo $opportunity->producttype->title; ?></td>
                        <td><?php echo Currency::format(@$opportunity->amount); ?></td>
                        <td><?php echo $opportunity->assignee->fullnames; ?></td>
                        <td><time class="cw-relative-date" datetime="<?php echo $opportunity->datecreated; ?>"></time></td>
                        <td><a href="/leads/view/<?php echo $opportunity->lead->id; ?>" >View</a></td>
                    </tr>
                <?php endforeach; ?>


                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="col-md-2 text-center">
    <a href="/leads/add" class="btn btn-default btn-sm light fw600 ml10 ajax-modal text-center">
        <span class="fa fa-plus pr5"></span> Add New Lead</a>
    <div class="clearfix"><br/></div>
    <div class="panel panel-tile text-center">
        <div class="panel-body bg-info">
            <h6 class="text-white">My Opportunities</h6>
            <h3><?php echo Currency::format($USER->myopportunitiesvalue); ?></h3>

        </div>
        <div class="panel-footer br-n p12">
    <span class="fs11">
      <b></b>
    </span>
        </div>
    </div>
    <div class="panel panel-tile text-center">
        <div class="panel-body bg-info">
            <h6 class="text-white">Total Opportunities</h6>
            <h3><?php echo Currency::format($USER->opportunitiesvalue); ?></h3>

        </div>
        <div class="panel-footer br-n p12">
    <span class="fs11">
      <b></b>
    </span>
        </div>
    </div>
</div>


