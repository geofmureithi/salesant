
<div class="col-md-12">
    <div class="panel panel-visible" id="spy4">
        <div class="panel-heading">
            <div class="panel-title">
                <span class="glyphicon glyphicon-tasks"></span><?php echo Customlang::process('Templates'); ?> Listing</div>
        </div>
        <div class="panel-menu text-right">
            <a href="/newslettertemplates/add" class="btn btn-info btn-sm light fw600 ml10">
                <span class="fa fa-plus pr5"></span> Add <?php echo Customlang::process('Template'); ?> </a>
        </div>
        <div class="panel-body pn">
            <table class="table table-striped table-hover display" id="datatable" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (($list?:array()) as $group): ?>
                    <tr>
                        <td><?php echo $group->title; ?></td>
                        <td><a href="/newslettertemplates/edit/<?php echo $group->id; ?>">Edit</a> |
                            <a href="/newslettertemplates/view/<?php echo $group->id; ?>" >View</a> |
                            <a class="delete" data-type="<?php echo Customlang::process('Template'); ?>" href="/newslettertemplates/delete/<?php echo $group->id; ?>" >Delete</a>
                        </td>
                    </tr>
                <?php endforeach; ?>


                </tbody>
            </table>
        </div>
    </div>
</div>

