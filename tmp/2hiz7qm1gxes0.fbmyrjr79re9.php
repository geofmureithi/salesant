<div class="col-md-12">
    <div class="panel panel-visible" id="spy4">
        <div class="panel-heading">
            <div class="panel-title">
                <span class="fa fa-table"></span><?php echo $page['title']; ?></div>
        </div>
        <div class="panel-menu text-right">
            <a href="/organisations/add" class="btn btn-info btn-sm light fw600 ml10 text-center">
                <span class="fa fa-plus pr5"></span> Add Organisation</a>
        </div>
        <div class="panel-body pn">
            <table class="table table-striped table-hover display" id="datatable" cellspacing="0">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Members</th>
                    <th>Leads</th>
                    <th>Opportunities</th>
                    <th>Created</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (($list?:array()) as $organisation): ?>
                    <tr>
                        <td><?php echo $organisation->name; ?></td>
                        <td><?php echo count($organisation->users); ?></td>
                        <td><?php echo count($organisation->leads); ?></td>
                        <td><?php echo count($organisation->opportunities); ?></td>
                        <td><time class="cw-relative-date" datetime="<?php echo $organisation->datecreated; ?>"></time></td>
                        <td>
                            <a data-toggle="tooltip" data-title="Edit <?php echo Customlang::process('Organisation'); ?>" class="iconLink" href="/organisations/edit/<?php echo $organisation->id; ?>"><i class="fa fa-edit"></i> Edit</a> |
                            <a data-toggle="tooltip" data-title="View <?php echo Customlang::process('Organisation'); ?>" class="iconLink" href="/organisations/view/<?php echo $organisation->id; ?>"><i class="fa fa-search-plus"></i> View</a> |
                            <a data-toggle="tooltip" data-title="Delete <?php echo Customlang::process('Organisation'); ?>" class="iconLink delete" data-type="Organisation" href="/organisations/delete/<?php echo $organisation->id; ?>"><i class="fa fa-remove"></i> Delete</a>
                        </td>
                    </tr>
                <?php endforeach; ?>


                </tbody>
            </table>
        </div>
    </div>
</div>
