
<div class="col-md-10">
    <div class="panel panel-visible" id="spy4">
        <div class="panel-heading">
            <div class="panel-title hidden-xs">
                <span class="glyphicon glyphicon-tasks"></span><?php echo Customlang::process('Newsletter Templates'); ?> Listing</div>
        </div>
        <div class="panel-menu"></div>
        <div class="panel-body pn">
            <table class="table table-striped table-hover display" id="datatable6" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Edit</th>
                    <th>View</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (($list?:array()) as $group): ?>
                    <tr>
                        <td><?php echo $group->title; ?></td>
                        <td><a href="/newslettertemplates/edit/<?php echo $group->id; ?>">Edit</a> </td>
                        <td><a href="/newslettertemplates/view/<?php echo $group->id; ?>" >View</a> </td>
                    </tr>
                <?php endforeach; ?>


                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="col-md-2">
    <a href="/newslettertemplates/add" class="btn btn-default btn-sm light fw600 ml10">
        <span class="fa fa-plus pr5"></span> Add <?php echo Customlang::process('Template'); ?> </a>
</div>

