<!-- Contact Form Popup -->
<div id="contact-form" class="popup-basic">
    <div class="panel">
        <div class="panel-heading">
            <span class="panel-title">
                <i class="fa fa-building"></i><?php echo $page['title']; ?>
            </span>
        </div>
        <!-- end .panel-heading section -->
        <div id="contact-results"></div>
        <form method="post" action="<?php echo $form['action']; ?>" class="ajaxable">
            <div class="panel-body p25">

                <div class="section">
                    <div class="form-group col-md-6">
                        <label for="name" class="control-label">First Name</label>
                        <div class="bs-component">
                            <input type="text" name="contact[firstname]" id="name" class="form-control" required value="<?php echo isset($POST['contact']['firstname'])?$POST['contact']['firstname']:''; ?>" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="name" class="control-label">Last Name</label>
                        <div class="bs-component">
                            <input type="text" name="contact[lastname]" id="name" class="form-control" required value="<?php echo isset($POST['contact']['lastname'])?$POST['contact']['lastname']:''; ?>" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="contact_position" class="control-label">Title</label>
                        <div class="bs-component">
                            <input type="text" name="contact[position]" id="contact_position" class="form-control" value="<?php echo isset($POST['contact']['position'])?$POST['contact']['position']:''; ?>" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="contact_phone" class="control-label">Phone</label>
                        <div class="bs-component">
                            <input type="text" name="contact[telephone]" id="contact_phone" class="form-control" value="<?php echo isset($POST['contact']['telephone'])?$POST['contact']['telephone']:''; ?>" />
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="contact_email" class="control-label">Email</label>
                        <div class="bs-component">
                            <input type="text" name="contact[email]" id="contact_email" class="form-control" value="<?php echo isset($POST['contact']['email'])?$POST['contact']['email']:''; ?>" />
                        </div>
                    </div>
                </div>
                <!-- end section -->

            </div>
            <!-- end .form-body section -->

            <div class="panel-footer">
                <?php if ($PARAMS['mothercontroller']): ?>
                    
                        <input type="hidden" name="contact[lead]" value="<?php echo $PARAMS['motherid']; ?>" />
                <?php endif; ?>
                <button type="submit" class="btn btn-sm btn-primary">Submit</button>
            </div>
            <!-- end .form-footer section -->
        </form>
    </div>
    <!-- end: .panel -->
</div>
<!-- end: .contact-form -->


