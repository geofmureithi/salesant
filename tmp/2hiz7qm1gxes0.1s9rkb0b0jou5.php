
<div class="col-md-10">
    <div class="panel panel-visible" id="spy4">
        <div class="panel-heading">
            <div class="panel-title hidden-xs">
                <span class="glyphicon glyphicon-tasks"></span><?php echo Customlang::process('Newsletter Groups'); ?> Listing</div>
        </div>
        <div class="panel-menu"></div>
        <div class="panel-body pn">
            <table class="table table-striped table-hover display" id="datatable6" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Contact Count</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (($list?:array()) as $group): ?>
                    <tr>
                        <td><?php echo $group->title; ?></td>
                        <td><?php echo $group->contacts?count($group->contacts->castAll()):0; ?></td>
                        <td><a href="/newsletters/edit/<?php echo $group->id; ?>">Edit</a> </td>
                    </tr>
                <?php endforeach; ?>


                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="col-md-2">
    <a href="/newsletters/add" class="btn btn-default btn-sm light fw600 ml10">
        <span class="fa fa-plus pr5"></span> Add <?php echo Customlang::process('Group'); ?> </a>
</div>

