<!-- Contact Form Popup -->
<div class="clearfix"></div>
<div id="newsletter-form">
    <div class="panel">
        <div class="panel-heading">
            <span class="panel-title">
                <i class="fa fa-building"></i>Create Newsletter & Send
            </span>
        </div>
        <form method="post" action="<?php echo $form['action']; ?>">
            <div class="panel-body p25">

                <div class="section" style="min-height: 90px;">
                    <div class="form-group col-md-12">
                        <label for="group" class="control-label">Contact Group</label>
                        <div class="bs-component">
                            <select id="group" name="send[group]"><?php foreach($groups as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['send']['group']) && $POST['send']['group']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="template" class="control-label">Select Template</label>
                        <div class="bs-component">
                            <select id="template"><?php foreach($templates as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['']) && $POST['']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                        </div>
                    </div>


                </div>
                <div class="form-group col-md-12">
                    <label class="control-label">Template</label>
                    <div class="bs-component">
                        <textarea class="summernote" id="templatetext" name="send[template]"><?php echo $templatetext->template; ?></textarea>
                    </div>
                </div>


                <!-- end section -->

            </div>
            <!-- end .form-body section -->

            <div class="panel-footer text-right">
                <?php if ($PARAMS['id']): ?>
                    
                        <input id="newslettertemplate_id" type="hidden" name="newslettertempate[id]" value="<?php echo $PARAMS['id']; ?>" />
                <?php endif; ?>
                <button type="submit" disabled class="btn btn-sm btn-primary">Send</button>
                <!--button class="btn btn-sm btn-warning">Draft</button-->
            </div>
            <!-- end .form-footer section -->
        </form>
    </div>
    <!-- end: .panel -->
</div>
<!-- end: .contact-form -->
<script>
    $(function(){
        $('input[name="newsletter[type]"]').trigger('change');
        $('input[name="newsletter[type]"]').on('change',function(){

            var $class = $(this).val();
            if($class == 'algorithim'){
                $('div.algorithm').show();
                $('div.file').hide();
            }
            if($class == 'file'){
                $('div.algorithm').hide();
                $('div.file').show();
            }

        })
    })
</script>

