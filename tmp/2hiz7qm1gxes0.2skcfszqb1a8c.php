
<div class="col-md-10">
    <div class="panel panel-visible" id="spy4">
        <div class="panel-heading">
            <div class="panel-title hidden-xs">
                <span class="glyphicon glyphicon-tasks"></span><?php echo $page['title']; ?></div>
        </div>
        <div class="panel-menu"></div>
        <div class="panel-body pn">
            <table class="table table-striped table-hover display" id="datatable6" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Full Names</th>
                    <th>Tel. No.</th>
                    <th>Email</th>
                    <th>User Type</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (($list?:array()) as $user): ?>
                    <tr>
                        <td><?php echo $user->fullnames; ?></td>
                        <td><?php echo $user->telephone; ?></td>
                        <td class="sorting_1"><?php echo $user->email; ?></td>
                        <td><?php echo $user->type->title; ?></td>
                        <td><input type="checkbox" class="icheck" disabled="disabled" <?php echo $user->active?'checked="checked"':''; ?> /></td>
                        <td><a href="/users/edit/<?php echo $user->id; ?>"><i
                                class="fa fa-fw fa-pencil"></i></a> <span></span> <input type="checkbox" class="icheck deletables" name="deletables[]" data-id="<?php echo $user->id; ?>" <?php echo (isset($POST['deletables']) && is_array($POST['deletables']) && in_array('on',$POST['deletables']))?'checked="checked"':''; ?> /></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
                <tfoot>
                <tr>
                    <th>Full Names</th>
                    <th>Tel. No.</th>
                    <th>Email</th>
                    <th>User Type</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </tfoot>
            </table>

        </div>
    </div>
</div>
<div class="col-md-2 text-center">
    <a href="/users/add" class="btn btn-default btn-sm light fw600 ml10">
        <span class="fa fa-plus pr5"></span> Add <?php echo Customlang::process('User'); ?> </a>
</div>

