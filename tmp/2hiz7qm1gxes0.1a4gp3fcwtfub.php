<div class="alert alert-danger alert-dismissable mb30">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h3 class="mt5">Reject This <?php echo Customlang::process('Quotation'); ?>?</h3>
    <p>
        <a class="btn btn-warning" href="#"><i class="fa fa-times"></i> Reject</a>
        <a class="btn btn-primary" href="/opportunity/<?php echo $quotation->opportunity->id; ?>/quotations/edit/<?php echo $quotation->id; ?>">Edit</a>
        <a class="btn btn-system" href="/leads/view/<?php echo $quotation->opportunity->lead->id; ?>">Cancel</a>
    </p>
</div>
<?php echo $this->render('/quotations/view.html',$this->mime,get_defined_vars()); ?>