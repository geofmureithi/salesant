    <div class="row">
        <div class="col-md-5">
            <h2 class="mtn"><?php echo $lead->title; ?></h2>

            <br/>
            <div class="panel listgroup-widget">
                <div class="panel-heading">
                    <span class="panel-icon">
                        <i class="fa fa-users"></i>
                    </span>
                    <span class="panel-title"> Contacts</span>
                    <span class="pull-right">
                        <a class="ajax-modal" href="/lead/<?php echo $lead->id; ?>/contacts/add">
                            <i class="fa fa-plus"></i> Add
                        </a>
                    </span>
                </div>
                <div class="panel-body pn">

                    <table class="table mbn tc-med-last">
                        <tbody>
                        <?php foreach (($lead->contacts?:array()) as $contact): ?>
                            <tr>
                                <td>
                                    <b><?php echo $contact->fullnames; ?></b><br/>
                                    <?php echo $contact->position; ?><?php echo $contact->position?'<br>':''; ?>
                                    <?php echo $contact->phone; ?><?php echo $contact->phone?'<br>':''; ?>
                                    <?php echo $contact->email; ?><?php echo $contact->email?'<br>':''; ?>
                                </td>
                                <td class="text-right">
                                    <a class="ajax-modal" href="/contacts/edit/<?php echo $contact->id; ?>"><span class="fa fa-pencil"></span> Edit </a> &nbsp;&nbsp;
                                    <a href="/contacts/delete/<?php echo $contact->id; ?>" class="delete" data-type="Contact" ><span class="fa fa-trash"></span> Delete </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- Task Widget -->
            <div class="panel panel-widget task-widget">
                <div class="panel-heading cursor">
                    <span class="panel-icon">
                        <i class="fa fa-cog"></i>
                    </span>
                    <span class="panel-title"> Tasks </span>

                    <span class="pull-right">
                        <a class="ajax-modal" href="/lead/<?php echo $lead->id; ?>/tasks/add">
                            <i class="fa fa-plus"></i> Add
                        </a>
                    </span>
                </div>
                <div class="panel-body <?php echo count($lead->tasks)?'panel-scroller scroller-md':''; ?>">

                    <ul class="task-list task-current">
                        <li class="task-label">Current Tasks</li>

                        <?php foreach (($lead->pendingtasks?:array()) as $task): ?>
                        <li class="task-item info" data-id="<?php echo $task->id; ?>">
                            <div class="task-handle">
                                <div class="checkbox-custom">
                                    <input type="checkbox" id="_task<?php echo $task->id; ?>" />
                                    <label for="_task<?php echo $task->id; ?>"></label>
                                </div>
                            </div>
                            <div class="task-desc"><?php echo $task->title; ?> <span><time class="cw-relative-date" datetime="<?php echo $task->datedue; ?>"></time></span></div>
                            <div class="task-menu"></div>
                            <div class="task-control">
                                <a class="ajax-modal"  href="/tasks/edit/<?php echo $task->id; ?>"><i class="fa fa-pencil"></i></a> &nbsp;
                                <a href="/tasks/delete/<?php echo $task->id; ?>" data-type="Task" class="delete"><i class="fa fa-trash"></i></a>
                            </div>
                        </li>
                        <?php endforeach; ?>

                    </ul>

                    <ul class="task-list task-completed">
                        <li class="task-label">Completed Tasks</li>

                        <?php foreach (($lead->completedtasks?:array()) as $task): ?>
                        <li class="task-item success item-checked" data-id="<?php echo $task->id; ?>">
                            <div class="task-handle">
                                <div class="checkbox-custom">
                                    <input type="checkbox" id="task<?php echo $task->id; ?>" />
                                    <label for="task<?php echo $task->id; ?>"></label>
                                </div>
                            </div>
                            <div class="task-desc"><?php echo $task->title; ?><span class="hidden"><time class="cw-relative-date" datetime="<?php echo $task->duedate; ?>"></time></span></div>
                            <div class="task-menu"></div>
                        </li>
                        <?php endforeach; ?>


                    </ul>
                </div>

            </div>


            <!-- Column Graph -->
            <div class="panel" id="p5">
                <div class="panel-heading cursor">
                    <span class="panel-icon">
                        <span class="glyphicons glyphicons-cup"></span>
                    </span>
                    <span class="panel-title">Opportunities (<?php echo $lead->opportunities?count($lead->opportunities):0; ?>)</span>
                    <span class="pull-right">
                        <a class="ajax-modal" href="/lead/<?php echo $lead->id; ?>/opportunities/add">
                            <i class="fa fa-plus"></i> Add
                        </a>
                    </span>
                </div>
            </div>

                <?php foreach (($lead->opportunities?:array()) as $opportunity): ?>
                    <div class="panel" id="p6">
                        <div class="panel-heading cursor">
                    <span class="panel-icon">
                        <span class="glyphicons glyphicons-cup"></span>
                    </span>
                            <span class="panel-title"><b><?php echo $opportunity->producttype->title; ?></b> : <b>Ksh <?php echo Currency::format($opportunity->amount); ?></b></span>
                            <span class="pull-right">
                                <?php if ($opportunity->sale): ?>
                                    
                                        <span class="badge badge-success">Approved</span>
                                    
                                    <?php else: ?>
                                        <a class="ajax-modal" href="/opportunities/edit/<?php echo $opportunity->id; ?>"><i class="fa fa-pencil"></i></a> &nbsp;
                                        <a href="/opportunities/delete/<?php echo $opportunity->id; ?>" data-type="Opportunity" class="delete"><i class="fa fa-trash"></i></a>
                                    
                                <?php endif; ?>

                            </span>
                        </div>
                <div class="panel-body pn" style="margin-bottom: 10px">
                    <table class="table mbn tc-med-last table-striped">
                        <tbody>
                        <tr>
                            <td valign="top">
                                <span class="fa fa-circle text-warning fs14 mr10"></span>
                                Contact: <?php echo $opportunity->contact->fullnames; ?><br/>
                                Assignee: <?php echo $opportunity->assignee->fullnames; ?>

                            </td>
                            <td valign="top">
                                <?php echo $opportunity->status->title; ?><br/>
                                <?php echo $opportunity->confidence; ?>%
                                <?php echo $opportunity->expecteddate; ?>

                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="background: #eee;">
                                Comments: <?php echo $opportunity->description; ?>
                                <br/>
                            </td>
                        </tr>
                        <tr>
                                <table class="table admin-form theme-warning tc-checkbox-1 fs13">
                                    <thead>
                                    <tr class="bg-light">
                                        <th class=""><?php echo Customlang::process('Product Title'); ?></th>
                                        <th class="">Unit</th>
                                        <th class="">Price</th>
                                        <th class="text-right">Status</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach (($opportunity->products?:array()) as $product): ?>
                                        <tr>
                                            <td class=""><?php echo $product->title; ?></td>
                                            <td class=""><?php echo $product->unit; ?></td>
                                            <td class=""><?php echo Currency::format($product->price); ?></td>
                                            <td class="text-right">
                                                <?php if ($opportunity->sale): ?>
                                                    
                                                        <span class="badge badge-system">Sold</span>
                                                    
                                                    <?php else: ?>
                                                        <div class="btn-group text-right">
                                                            <button type="button" class="btn btn-success br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Active
                                                                <span class="caret ml5"></span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu">
                                                                <li>
                                                                    <a href="/products/edit/<?php echo $product->id; ?>" class="ajax-modal">Edit</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    
                                                <?php endif; ?>

                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                        </tr>
                        <tr>
                                <div class="row">
                                    <div id="justified-button-bar" class="col-lg-12">
                                        <div class="btn-group btn-group-justified">
                                            <div class="btn-group">
                                                <?php if ($opportunity->products && !$opportunity->sale): ?>
                                                    
                                                        <a href="/opportunity/<?php echo $opportunity->id; ?>/quotations/add" title="Create <?php echo Customlang::process('Quotation'); ?>(s)" type="button" class="btn btn-default" data-toggle="tooltip">
                                                            <i class="fa fa-plus"></i></span>&nbsp; <?php echo Customlang::process('Quotation'); ?>
                                                        </a>
                                                    
                                                <?php endif; ?>
                                                <?php if (!$opportunity->products && !$opportunity->sale): ?>
                                                    
                                                            <a href="/opportunities/edit/<?php echo $opportunity->id; ?>" title="Add <?php echo Customlang::process('Product'); ?>{s) for <?php echo Customlang::process('Quotation'); ?>" type="button" class="btn btn-default ajax-modal" data-toggle="tooltip">
                                                                <i class="fa fa-edit"></i></span>&nbsp; Edit Opportunity
                                                            </a>
                                                    
                                                <?php endif; ?>

                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="panel listgroup-widget">
                                <div class="panel-body pn">

                                    <table class="table mbn tc-med-last">
                                        <tbody>
                                        <?php foreach (($opportunity->quotations?:array()) as $quotation): ?>
                                            <tr class="<?php echo $quotation->sale?'success':''; ?><?php echo !$quotation->sale && !$quotation->active?'danger':''; ?>">
                                                <td>
                                                    <b><i class="fa fa-shopping-cart"></i> <?php echo Customlang::process('Quotation'); ?> : <?php echo @$quotation->opportunity->contact->fullnames; ?>: <span class="badge"><?php echo $quotation->sale?'Approved':''; ?><?php echo !$quotation->sale && !$quotation->active?'Rejected':''; ?></span></b>
                                                    <br/>
                                                    <?php echo $quotation->datecreated; ?>
                                                    </br>Amount: <?php echo Currency::format($quotation->amount); ?> <br/>
                                                    <b>Payments <a data-toggle="collapse" href="#collapsePayment<?php echo $quotation->id; ?>" aria-expanded="false" aria-controls="#collapsePayment<?php echo $quotation->id; ?>"><i class="fa fa-eye"></i></a></b><br>
                                                    <div class="collapse" id="collapsePayment<?php echo $quotation->id; ?>">
                                                        <?php foreach (($quotation->paymentPlan?:array()) as $payment): ?>
                                                            <?php echo $payment->amount?'Payment: ':''; ?><?php echo Currency::format($payment->amount); ?><?php echo $payment->amount?'<br>':''; ?>
                                                        <?php endforeach; ?>
                                                    </div>

                                                </td>
                                                <td class="text-right">
                                                    <?php if (!$quotation->sale): ?>
                                                        
                                                            <a href="/opportunity/<?php echo $opportunity->id; ?>/quotations/approve/<?php echo $quotation->id; ?>"  data-toggle="tooltip" title="Approve <?php echo Customlang::process('Quotation'); ?>"><span class="fa fa-check"></span></a>&nbsp;
                                                        
                                                    <?php endif; ?>
                                                    <?php if ($quotation->active && !$quotation->sale): ?>
                                                        
                                                            <a href="/opportunity/<?php echo $opportunity->id; ?>/quotations/reject/<?php echo $quotation->id; ?>"  data-toggle="tooltip" title="Reject <?php echo Customlang::process('Quotation'); ?>"><span class="fa fa-times"></span></a>&nbsp;
                                                        
                                                    <?php endif; ?>
                                                    <a href="/opportunity/<?php echo $opportunity->id; ?>/quotations/view/<?php echo $quotation->id; ?>"  data-toggle="tooltip" title="View <?php echo Customlang::process('Quotation'); ?>"><span class="fa fa-bookmark"></span></a>&nbsp;
                                                    <?php if ($quotation->active && !$quotation->sale): ?>
                                                        
                                                            <a href="/opportunity/<?php echo $opportunity->id; ?>/quotations/edit/<?php echo $quotation->id; ?>"  data-toggle="tooltip" title="Edit <?php echo Customlang::process('Quotation'); ?>"><span class="fa fa-pencil"></span></a>&nbsp;
                                                            <a href="/opportunity/<?php echo $opportunity->id; ?>/quotations/delete/<?php echo $quotation->id; ?>" class="delete" data-type="<?php echo Customlang::process('Quotation'); ?>" data-toggle="tooltip" title="Delete <?php echo Customlang::process('Quotation'); ?>"><span class="fa fa-trash"></span></a>

                                                        
                                                    <?php endif; ?>

                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </tr>
                        </tbody>
                    </table>
                </div>
                    </div>

                <?php endforeach; ?>












        </div>
        <div class="col-md-7">

            <h2 class="mtn">

                <a class="btn btn-default btn-gradient dark ajax-modal" href="/lead/<?php echo $lead->id; ?>/notes/add">
                    <i class="glyphicons glyphicons-comments"></i> Note
                </a>
                <button type="button" class="btn btn-default btn-gradient dark" data-toggle="collapse" data-target="#emailCompose" aria-expanded="false" aria-controls="emailCompose">
                    <i class="glyphicons glyphicons-envelope"></i> Email
                </button>
                <a class="btn btn-default btn-gradient dark ajax-modal" id="load-callform"href="/lead/<?php echo $lead->id; ?>/calls/add">
                    <i class="glyphicons glyphicons-phone"></i> Log a Call
                </a>
                <div class="col-md-3 pull-right">
                    <select name="status" id="status" class="form-control" data-id="<?php echo $lead->id; ?>">
                    <?php foreach($statuses?$statuses:array() as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['status']) && $POST['status']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                </div>


            </h2>
            <br/>
            <div class="email-compose collapse" id="emailCompose">
                <div id="email-results"></div>

                <!-- Email -->
                <form class="form-horizontal" role="form" method="POST" action="/lead/<?php echo $lead->id; ?>/emails/add" id="leademail-form">
                    <div class="panel">
                        <div class="panel-heading">
                            <span class="panel-title">New Email</span>

                        </div>
                        <div class="panel-body bg-light pt20 pbn pl30">
                            <div class="pull-left col-md-8"><p><label>From:</label> <?php echo $USER->fullnames; ?> &lt;<?php echo $USER->email; ?>&gt;</p></div>
                        </div>
                        <div class="panel-body">

                            <div class="ph20">
                                <div class="form-group">
                                    <div class="bs-component">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                To:
                                            </span>
                                            <input name="email[to]" class="form-control" type="text" data-role="emails-input" value="<?php echo $lead->contacts['0']->email; ?>" />
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="bs-component">
                                        <a class="add-cc">Add CC</a>
                                        <a class="add-bcc">Add BCC</a>

                                    </div>
                                </div>
                                <div class="form-group" id="cc-input">
                                    <div class="bs-component">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                CC:
                                            </span>
                                            <input name="email[cc]" class="form-control" type="text" data-role="emails-input" value="<?php echo isset($POST['email']['cc'])?$POST['email']['cc']:''; ?>" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" id="bcc-input">
                                    <div class="bs-component">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                BCC:
                                            </span>
                                            <input name="email[bcc]" class="form-control" type="text" style="width: 100%;" data-role="emails-input" value="<?php echo isset($POST['email']['bcc'])?$POST['email']['bcc']:''; ?>" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="bs-component">
                                        <input name="email[subject]" class="form-control" type="text" placeholder="Subject" value="<?php echo isset($POST['email']['subject'])?$POST['email']['subject']:''; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="bs-component">
                                        <textarea class="summernote-email" name="email[description]"></textarea>
                                    </div>
                                </div>
                                <div class="form-group" id="foremailattachments">
                                    <div class="bs-component">
                                        <div id="emailattachments" class="dropzone"></div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="panel-footer clearfix">
                            <span class="pull-left" style="padding:5px">
                                <a href="#" id="toggleattachments"><span class="glyphicons glyphicons-paperclip fa-lg"></span></a>
                            </span>

                            <span class="pull-right">
                                <input class="btn btn-primary btn-sm ph15" type="submit" name="send" value="Send" />
                            </span>

                        </div>
                    </div>
                </form>
            </div>



            <div class="panel">
                <div class="panel-body <?php echo count($lead->allactivities)>7? 'panel-scroller scroller-pn pn':''; ?>">
                    <ol class="timeline-list">
                        <?php foreach (($lead->allactivities?:array()) as $activity): ?>
                        <li class="timeline-item">
                            <div class="timeline-icon bg-dark light">
                                <span class="<?php echo $activity->icon; ?>"></span>
                            </div>
                            <div class="timeline-desc">

                                <p><span class="text-darker"><?php echo $activity->description; ?></span>
                                    </br><time class="cw-relative-date" datetime="<?php echo $activity->datecreated; ?>"></time>
                                </p>

                            </div>
                            <div class="timeline-date rightn">
                                <?php echo $activity->creator->firstname; ?>
                                <img src="<?php echo $activity->creator->gravatar; ?>" alt="avatar" class="mw30 br4 ml10">

                            </div>
                        </li>

                        <?php endforeach; ?>

                    </ol>
                </div>
            </div>




        </div>
    </div>



    <script>

        function syncEmails(reload) {
            $.get('/emails/sync/<?php echo $lead->id; ?>',function(xhr){
                if (reload)
                    $.pjax.reload('#content');
            })
        }
        $(function(){
            setTimeout(syncEmails(), 1000 * 60 * 20);
            syncEmails(false);
        })
    </script>