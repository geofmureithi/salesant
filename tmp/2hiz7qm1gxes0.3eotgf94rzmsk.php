<div class="row">
    <form class="<?php echo $form['action']; ?>" method="get">
    <div class="panel ">
        <div class="panel-heading">
            <div class="panel-title hidden-xs">
                <span class="glyphicon glyphicon-tasks"></span><?php echo Customlang::process('Sort'); ?></div>
        </div>
        <div class="panel-body">
            <?php echo $this->render('/general/timespan.html',$this->mime,get_defined_vars()); ?>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="opportunity_type" class="control-label"><?php echo Customlang::process('Opportunity Type'); ?></label>
                    <div class="bs-component">
                        <select id="opportunity_type" name="productType" class="select2-single form-control">
                        <?php foreach($producttypes as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['productType']) && $POST['productType']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label"></label>
                    <div class="bs-component">
                        <button class="btn btn-warning pull-right col-md-12" type="submit">Process</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title hidden-xs">
                    <span class="glyphicon glyphicon-copy"></span><?php echo Customlang::process('Opportunities'); ?> Created</div>
            </div>
            <div class="panel-body pn" id="opportunityCreated" style="height: 200px">

            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title hidden-xs">
                    <span class="glyphicon glyphicon-copy"></span><?php echo Customlang::process('Quotations'); ?> Created</div>
            </div>
            <div class="panel-body pn" id="quotationsCreated" style="height: 200px">

            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title hidden-xs">
                    <span class="glyphicon glyphicon-copy"></span><?php echo Customlang::process('Quotations'); ?> Value</div>
            </div>
            <div class="panel-body pn" id="quotationsValue" style="height: 200px">

            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title hidden-xs">
                    <span class="glyphicon glyphicon-copy"></span><?php echo Customlang::process('Opportunities'); ?> Value</div>
            </div>
            <div class="panel-body pn" id="opportunitiesValue" style="height: 200px">

            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy4">
            <div class="panel-heading">
                <div class="panel-title hidden-xs">
                    <span class="glyphicon glyphicon-tasks"></span><?php echo Customlang::process('Opportunities'); ?> Report</div>
            </div>
            <div class="panel-menu"></div>
            <div class="panel-body pn">
                <table class="table table-striped table-hover display" id="datatable6" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Sales Person</th>
                        <th>Leads</th>
                        <th><?php echo Customlang::process('Quotations'); ?></th>
                        <th><?php echo Customlang::process('Quotations'); ?> Value</th>
                        <th><?php echo Customlang::process('Opportunities'); ?></th>
                        <th class="sorting_1"><?php echo Customlang::process('Opportunities'); ?> Value</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $totalleads='0'; ?>
                    <?php $totalquotations='0'; ?>
                    <?php $totalopportunities='0'; ?>
                    <?php $totalquotationvalues='0'; ?>
                    <?php $totalopportunitiesvalues='0'; ?>
                    <?php foreach (($salespersons?:array()) as $user): ?>
                        <tr>
                            <td><?php echo $user->fullnames; ?></td>
                            <td><?php echo $user->reportmyleads?count($user->reportmyleads):0; ?><?php $totalleads=$totaleads += $user->reportmyleads?count($user->reportmyleads):0; ?></td>
                            <td><?php echo $user->reportmyquotations?count($user->reportmyquotations):0; ?><set @totalquotations="<?php echo $totalquotations += $user->reportmyquotations?count($user->reportmyquotations):0; ?>"/></td>
                            <td><?php echo Currency::format($user->reportmyquotationsvalue); ?><?php $totalquotationvalues=$totalquotationvalues +=$user->reportmyquotationsvalue; ?></td>
                            <td><?php echo $user->reportmyopportunities?count($user->reportmyopportunities):0; ?><?php $totalopportunities=$totalopportunities += $user->reportmyopportunities?count($user->reportmyopportunities):0; ?></td>
                            <td><?php echo Currency::format($user->reportmyopportunitiesvalue); ?><?php $totalopportunitiesvalues=$totalopportunitiesvalues +=$user->reportmyopportunitiesvalue; ?></td>
                        </tr>
                    <?php endforeach; ?>


                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-2 text-center">
        <div class="panel panel-tile text-center">
            <div class="panel-body bg-info">
                <h2 class="fs25 mbn"><?php echo $totalleads; ?></h2>
                <h6 class="text-white">Total <?php echo Customlang::process('Leads'); ?>(s)</h6>
            </div>
            <div class="panel-footer br-n p12">
    <span class="fs11">
      <i class="fa fa-arrow-up text-info pr5"></i>
      <b></b>
    </span>
            </div>
        </div>
    </div>
    <div class="col-md-2 text-center">
        <div class="panel panel-tile text-center">
            <div class="panel-body bg-info">
                <h2 class="fs25 mbn"><?php echo $totalquotations; ?></h2>
                <h6 class="text-white">Total <?php echo Customlang::process('Quotation'); ?>(s)</h6>
            </div>
            <div class="panel-footer br-n p12">
    <span class="fs11">
      <i class="fa fa-arrow-up text-info pr5"></i>
      <b></b>
    </span>
            </div>
        </div>
    </div>
    <div class="col-md-2 text-center">
        <div class="panel panel-tile text-center">
            <div class="panel-body bg-info">
                <h2 class="fs25 mbn"><?php echo $totalopportunities; ?></h2>
                <h6 class="text-white">Total <?php echo Customlang::process('Opportunity'); ?>(s)</h6>
            </div>
            <div class="panel-footer br-n p12">
    <span class="fs11">
      <i class="fa fa-arrow-up text-info pr5"></i>
      <b></b>
    </span>
            </div>
        </div>
    </div>
    <div class="col-md-3 text-center">
        <div class="panel panel-tile text-center">
            <div class="panel-body bg-info">
                <h2 class="fs25 mbn"><?php echo Currency::format($totalopportunitiesvalues); ?></h2>
                <h6 class="text-white">Total <?php echo Customlang::process('Opportunity'); ?>(s) Value</h6>
            </div>
            <div class="panel-footer br-n p12">
    <span class="fs11">
      <i class="fa fa-arrow-up text-info pr5"></i>
      <b></b>
    </span>
            </div>
        </div>
    </div>
    <div class="col-md-3 text-center">
        <div class="panel panel-tile text-center">
            <div class="panel-body bg-info">
                <h2 class="fs25 mbn"><?php echo Currency::format($totalquotationvalues); ?></h2>
                <h6 class="text-white">Total <?php echo Customlang::process('Quotations'); ?>(s) Value</h6>
            </div>
            <div class="panel-footer br-n p12">
    <span class="fs11">
      <i class="fa fa-arrow-up text-info pr5"></i>
      <b></b>
    </span>
            </div>
        </div>
    </div>
</div>
<script>
    var opportunityCreated = [
            <?php foreach (($salespersons?:array()) as $user): ?>
        { label: "<?php echo $user->firstname; ?> : <?php echo $user->reportmyopportunities?count($user->reportmyopportunities):0; ?>",  data: <?php echo $user->reportmyopportunities?count($user->reportmyopportunities):0; ?>},
            <?php endforeach; ?>
    ];
    var quotationsCreated = [
            <?php foreach (($salespersons?:array()) as $user): ?>
        { label: "<?php echo $user->firstname; ?> : <?php echo $user->reportmyquotations?count($user->reportmyquotations):0; ?>",  data: <?php echo $user->reportmyquotations?count($user->reportmyquotations):0; ?>},
            <?php endforeach; ?>
    ];
    var quotationsValue = [
            <?php foreach (($salespersons?:array()) as $user): ?>
        { label: "<?php echo $user->firstname; ?> : <?php echo Currency::format($user->reportmyquotationsvalue); ?>",  data: <?php echo $user->reportmyquotationsvalue; ?>},
            <?php endforeach; ?>
    ];
    var opportunitiesValue = [
            <?php foreach (($salespersons?:array()) as $user): ?>
        { label: "<?php echo $user->firstname; ?> : <?php echo Currency::format($user->reportmyopportunitiesvalue); ?>",  data: <?php echo $user->reportmyopportunitiesvalue; ?>},
            <?php endforeach; ?>
    ];
</script>

<script>
    $(function(){

        $.plot('#opportunityCreated', opportunityCreated, {
            series: {
                pie: {
                    show: true,
                    radius: 1,
                }
            },
            legend: {
                show: true
            }
        });
        $.plot('#quotationsCreated', quotationsCreated, {
            series: {
                pie: {
                    show: true,
                    radius: 1,
                }
            },
            legend: {
                show: true
            }
        });
        $.plot('#quotationsValue', quotationsValue, {
            series: {
                pie: {
                    show: true,
                    radius: 1,
                }
            },
            legend: {
                show: true
            }
        });
        $.plot('#opportunitiesValue', opportunitiesValue, {
            series: {
                pie: {
                    show: true,
                    radius: 1,
                }
            },
            legend: {
                show: true
            }
        });
    })
</script>

