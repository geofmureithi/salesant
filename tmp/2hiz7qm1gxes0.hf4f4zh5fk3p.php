    <div class="row">
        <div class="col-md-5">
            <h2 class="mtn"><?php echo $lead->title; ?></h2>

            <br/>
            <!-- Task Widget -->
            <div class="panel panel-widget task-widget">
                <div class="panel-heading cursor">
                    <span class="panel-icon">
                        <i class="fa fa-cog"></i>
                    </span>
                    <span class="panel-title"> Tasks </span>

                    <span class="pull-right">
                        <a id="load-taskform" href="#task-form">
                            <i class="fa fa-plus"></i> Add
                        </a>
                    </span>
                </div>
                <div class="panel-body pn">

                    <ul class="task-list task-current">
                        <li class="task-label">Current Tasks</li>

                        <?php foreach (($lead->pendingtasks?:array()) as $task): ?>
                        <li class="task-item info" data-id="<?php echo $task->id; ?>">
                            <div class="task-handle">
                                <div class="checkbox-custom">
                                    <input type="checkbox" id="_task<?php echo $task->id; ?>" /><?php endforeach; ?>

                    </ul>

                    <ul class="task-list task-completed">
                        <li class="task-label">Completed Tasks</li>

                        <?php foreach (($lead->completedtasks?:array()) as $task): ?>
                        <li class="task-item success item-checked" data-id="<?php echo $task->id; ?>">
                            <div class="task-handle">
                                <div class="checkbox-custom">
                                    <input type="checkbox" id="task<?php echo $task->id; ?>" /><?php endforeach; ?>


                    </ul>
                </div>

            </div>


            <!-- Column Graph -->
            <div class="panel" id="p5">
                <div class="panel-heading cursor">
                    <span class="panel-icon">
                        <span class="glyphicons glyphicons-cup"></span>
                    </span>
                    <span class="panel-title">Opportunities</span>
                    <span class="pull-right">
                        <a class="ajax-modal" href="/opportunities/add">
                            <i class="fa fa-plus"></i> Add
                        </a>
                    </span>
                </div>
                <?php foreach (($lead->opportunities?:array()) as $opportunity): ?>
                <div class="panel-body pn" style="margin-bottom: 10px">
                    <table class="table mbn tc-med-last table-striped">
                        <tbody>
                        <tr>
                            <td valign="top">
                                <span class="fa fa-circle text-warning fs14 mr10"></span><b><?php echo $opportunity->type->title; ?></b> : <b>Ksh <?php echo ($opportunity->value); ?></b><br/>
                                <?php echo $opportunity->contact->fullnames; ?>
                                <?php echo $opportunity->assignee->fullnames; ?>

                            </td>
                            <td valign="top">
                                <?php echo $opportunity->status->title; ?><br/>
                                <?php echo $opportunity->confidence; ?>%
                                <?php echo $opportunity->expecteddate; ?>

                            </td>
                            <td width="60">
                                <a class="load-editopportunityform" data-opportunityid="<?php echo $opportunity->id; ?>" href="#opportunity-editform"><i class="fa fa-pencil"></i></a> &nbsp;
                                <a href="#" class="delete-opportunity" data-opportunityid="<?php echo $opportunity->id; ?>"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="background: #eee;">
                                Comments: <?php echo $opportunity->description; ?>
                                <br/>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <?php endforeach; ?>



            </div>



            <div class="panel listgroup-widget">
                <div class="panel-heading">
                    <span class="panel-icon">
                        <i class="fa fa-users"></i>
                    </span>
                    <span class="panel-title"> Contacts</span>
                    <span class="pull-right">
                        <a id="load-contactform" href="#contact-form">
                            <i class="fa fa-plus"></i> Add
                        </a>
                    </span>
                </div>
                <div class="panel-body pn">

                    <table class="table mbn tc-med-last">
                        <tbody>
                        <?php foreach (($lead->contacts?:array()) as $contact): ?>
                        <tr>
                            <td>
                                <b><?php echo $contact->fullnames; ?></b><br/>
                                <?php echo $contact->position; ?><?php echo $contact->position?'<br>':''; ?>
                                <?php echo $contact->phone; ?><?php echo $contact->phone?'<br>':''; ?>
                                <?php echo $contact->email; ?><?php echo $contact->email?'<br>':''; ?>
                            </td>
                            <td class="text-right">
                                <a href="#contact-editform" class="load-editcontactform" data-contactid="<?php echo $contact->id; ?>"><span class="fa fa-pencil"></span> Edit </a> &nbsp;&nbsp;
                                <a href="#" class="delete-contact" data-contactid="<?php echo $contact->id; ?>" ><span class="fa fa-trash"></span> Delete </a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
        <div class="col-md-7">

            <h2 class="mtn">

                <a class="btn btn-default btn-gradient dark" id="load-noteform" href="#note-form">
                    <i class="glyphicons glyphicons-comments"></i> Note
                </a>
                <button type="button" class="btn btn-default btn-gradient dark" data-toggle="collapse" data-target="#emailCompose" aria-expanded="false" aria-controls="emailCompose">
                    <i class="glyphicons glyphicons-envelope"></i> Email
                </button>
                <a class="btn btn-default btn-gradient dark" id="load-callform" href="#call-form">
                    <i class="glyphicons glyphicons-phone"></i> Log a Call
                </a>
                <div class="form-group col-sm-3 pull-right">
                    <select name="status" id="status" class="form-control">
                    <?php foreach($statuses?$statuses:array() as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['status']) && $POST['status']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                </div>


            </h2>
            <br/>
            <div class="email-compose collapse" id="emailCompose">
                <div id="email-results"></div>

                <!-- Email -->
                <form class="form-horizontal" role="form" method="POST" action="" id="leademail-form">
                    <div class="panel">
                        <div class="panel-heading">
                            <span class="panel-title">New Email</span>
                        </div>
                        <div class="panel-body bg-light pt20 pbn pl30">
                            <p><b>From:</b> <?php echo $USER->fullnames; ?> &lt;<?php echo $USER->email; ?>&gt;
                        </div>
                        <div class="panel-body">

                            <div class="ph20">
                                <div class="form-group">
                                    <div class="bs-component">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                To:
                                            </span>
                                            <input name="email_to" class="form-control" type="text" data-role="emails-input" value="<?php echo $lead->contact['0']->email; ?>" />
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="bs-component">
                                        <a class="add-cc">Add CC</a> &nbsp;&nbsp;
                                        <a class="add-bcc">Add BCC</a>

                                    </div>
                                </div>
                                <div class="form-group" id="cc-input">
                                    <div class="bs-component">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                CC:
                                            </span>
                                            <input name="email[cc]" class="form-control" type="text" data-role="emails-input" value="<?php echo isset($POST['email']['cc'])?$POST['email']['cc']:''; ?>" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" id="bcc-input">
                                    <div class="bs-component">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                BCC:
                                            </span>
                                            <input name="email[bcc]" class="form-control" type="text" data-role="emails-input" value="<?php echo isset($POST['email']['bcc'])?$POST['email']['bcc']:''; ?>" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="bs-component">
                                        <input name="email[subject]" class="form-control" type="text" placeholder="Subject" value="<?php echo isset($POST['email']['subject'])?$POST['email']['subject']:''; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="bs-component">
                                        <textarea class="summernote-email" name="email[body]"></textarea>
                                    </div>
                                </div>
                                <div class="form-group" id="foremailattachments">
                                    <div class="bs-component">
                                        <div id="emailattachments" class="dropzone"></div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="panel-footer clearfix">
                            <span class="pull-left" style="padding:5px">
                                <a href="#" id="toggleattachments"><span class="glyphicons glyphicons-paperclip fa-lg"></span></a>
                            </span>

                            <span class="pull-right">
                                <button class="btn btn-default btn-sm ph15 mr5" type="submit" value="draft">Draft</button>
                                <button class="btn btn-primary btn-sm ph15" type="submit" value="send">Send</button>
                            </span>

                        </div>
                    </div>
                </form>
            </div>



            <div class="panel">
                <div class="panel-body ptn pbn pl10 prn">
                    <ol class="timeline-list">
                        <?php foreach (($lead->activities?:array()) as $activity): ?>
                        <li class="timeline-item">
                            <div class="timeline-icon bg-dark light">
                                <span class="<?php echo $activity->icon; ?>"></span>
                            </div>
                            <div class="timeline-desc">

                                <p><span class="text-darker"><?php echo $activity->desciption; ?></span><?php echo $activity->datecreated; ?></p>

                            </div>
                            <div class="timeline-date rightn">
                                <?php echo $activity->creator->firstname; ?>
                                <img src="<?php echo $activity->creator->gravator; ?>" alt="avatar" class="mw30 br4 ml10">

                            </div>
                        </li>

                        <?php endforeach; ?>

                    </ol>
                </div>
            </div>




        </div>
    </div>

