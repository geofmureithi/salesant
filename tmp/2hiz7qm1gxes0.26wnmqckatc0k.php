<title><?php echo $page['title']; ?> | <?php echo $app['title']; ?></title>
<div class="col-lg-12" style="margin-top: 10px">
    <?php foreach ((\Flash::instance()->getMessages()?:array()) as $msg): ?>
        <div class="alert alert-<?php echo $msg['status']?$msg['status']:'info'; ?> alert-dismissable alert-dismiss">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->esc($msg['text']); ?>
        </div>
    <?php endforeach; ?>
</div>