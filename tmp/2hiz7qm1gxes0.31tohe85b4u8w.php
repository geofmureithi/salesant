
<div class="col-md-10">
    <div class="panel panel-visible" id="spy4">
        <div class="panel-heading">
            <div class="panel-title hidden-xs">
                <span class="glyphicon glyphicon-tasks"></span><?php echo Customlang::process('Products'); ?> Listing</div>
        </div>
        <div class="panel-menu"></div>
        <div class="panel-body pn">
            <table class="table table-striped table-hover display" id="datatable6" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Name</th>
                    <th><?php echo Customlang::process('Product Type'); ?></th>
                    <th>Price</th>
                    <th>Unit</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (($list?:array()) as $product): ?>
                    <tr>
                        <td><?php echo $product->title; ?></td>
                        <td><?php echo $product->type->title; ?></td>
                        <td><?php echo Currency::format($product->price); ?></td>
                        <td><?php echo $product->unit; ?></td>
                        <td><a href="/products/edit/<?php echo $product->id; ?>" class="ajax-modal" >Edit</a> </td>
                    </tr>
                <?php endforeach; ?>


                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="col-md-2">
    <a href="/products/add" class="btn btn-default btn-sm light fw600 ml10 ajax-modal">
        <span class="fa fa-plus pr5"></span> Add <?php echo Customlang::process('Product'); ?> </a>
</div>

