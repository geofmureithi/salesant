<!-- Admin Form Popup -->
<div id="modal-form" class="popup-basic popup-lg mfp-with-anim">
    <div class="panel">
        <div class="panel-heading">
            <span class="panel-title">
                <i class="fa fa-building"></i><?php echo $page['title']; ?>
            </span>
        </div>
        <!-- end .panel-heading section -->
        <div id="results"></div>
        <form method="post" action="<?php echo $form['action']; ?>">
            <div class="panel-body p25">

                <div class="section">
                    <div class="form-group col-md-6">
                        <label for="company" class="control-label">Company/Organization</label>
                        <div class="bs-component">
                            <input placeholder="Name of the Company if lead is a Company" type="text" name="company" id="company" class="form-control" title="Company name if lead is not a person" value="<?php echo isset($POST['company'])?$POST['company']:''; ?>" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="contact_name" class="control-label">Contact Name</label>
                        <div class="bs-component">
                            <input placeholder="Name of Chief Contact Names" type="text" name="contact[name]" id="contact_name" class="form-control" value="<?php echo isset($POST['contact']['name'])?$POST['contact']['name']:''; ?>" />
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="contact_email" class="control-label">Contact Email</label>
                        <div class="bs-component">
                            <input placeholder="Contacts Email" type="text" name="contact[email]" id="contact_email" class="form-control" value="<?php echo isset($POST['contact']['email'])?$POST['contact']['email']:''; ?>" />
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="contact_email" class="control-label">Contact Telephone</label>
                        <div class="bs-component">
                            <input placeholder="Contacts Telephone" type="text" name="contact[telephone]" id="contact_telephone" class="form-control" value="<?php echo isset($POST['contact']['telephone'])?$POST['contact']['telephone']:''; ?>" />
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="source" class="control-label">
                            Choose a Source
                            <?php if ($sources): ?>
                                <?php else: ?>
                                    <span class="label label-danger">Please Create Lead Sources for your organisation <a href="/organisations">HERE</a> </span>
                                
                            <?php endif; ?>
                        </label>
                        <div class="bs-component">
                            <select required name="lead[source]" id="source" class="select2-single form-control">
                            <?php foreach($sources as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['lead']['source']) && $POST['lead']['source']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                        </div>

                    </div>
                    <div class="form-group col-md-12">
                    <?php if ($templates): ?>
                        

                                <div class="checkbox-custom fill mb5">
                                    <input type="checkbox" checked id="sendWelcomeEmail" />
                                    <label for="sendWelcomeEmail">Send Welcome Email</label>
                                </div>
                                <div class="bs-component">
                                    <select name="welcometemplate" id="template" class="select2-single form-control">
                                    <?php foreach($templates as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['welcometemplate']) && $POST['welcometemplate']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                                </div>

                        
                        <?php else: ?>
                            <span class="label label-warning">Please Create A welcome template to be able to be sending welcome templates</span>
                        
                    <?php endif; ?>
                    </div>
                </div>
                <!-- end section -->

            </div>
            <!-- end .form-body section -->

            <div class="panel-footer">
                <button type="submit" class="btn btn-sm btn-primary">Submit</button>
            </div>
            <!-- end .form-footer section -->
        </form>
    </div>
    <!-- end: .panel -->
</div>
<!-- end: .admin-form -->
<script>
    $(function(){
        $('#sendWelcomeEmail').on('change', function(e){
            $('select#template').attr('disabled', !$(this).is(':checked'))
        })

    })
</script>