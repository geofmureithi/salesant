<div class="col-md-10">
    <div class="panel panel-visible" id="spy4">
        <div class="panel-heading">
            <div class="panel-title hidden-xs">
                <span class="glyphicon glyphicon-tasks"></span><?php echo $page['title']; ?></div>
        </div>
        <div class="panel-menu"></div>
        <div class="panel-body pn">
            <table class="table table-striped table-hover display" id="datatable6" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (($list?:array()) as $productType): ?>
                    <tr>
                        <td><?php echo $productType->title; ?></td>
                        <td><?php echo $productType->description; ?></td>
                        <td><a href="/producttypes/edit/<?php echo $productType->id; ?>" class="ajax-modal" >Edit</a> </td>
                    </tr>
                <?php endforeach; ?>


                </tbody>
            </table>
        </div>
    </div>
</div>

