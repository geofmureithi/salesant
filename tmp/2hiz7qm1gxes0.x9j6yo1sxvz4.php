<div class="col-md-10">
    <div class="panel panel-visible" id="spy4">
        <div class="panel-heading">
            <div class="panel-title hidden-xs">
                <span class="fa fa-table"></span><?php echo $page['title']; ?></div>
        </div>
        <div class="panel-menu"></div>
        <div class="panel-body pn">
            <table class="table table-striped table-hover display" id="datatable6" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Members</th>
                    <th>Leads</th>
                    <th>Opportunities</th>
                    <th>Created</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (($list?:array()) as $organisation): ?>
                    <tr>
                        <td><?php echo $organisation->name; ?></td>
                        <td><?php echo count($organisation->users); ?></td>
                        <td><?php echo count($organisation->leads); ?></td>
                        <td><?php echo count($organisation->opportunities); ?></td>
                        <td><time class="cw-relative-date" datetime="<?php echo $organisation->datecreated; ?>"></time></td>
                        <td>
                            <a href="/organisations/edit/<?php echo $organisation->id; ?>"><i class="fa fa-edit"></i></a>
                            <a href="/organisations/view/<?php echo $organisation->id; ?>"> <i class="fa fa-search-plus"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>


                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="col-md-2 text-center">
    <a href="/organisations/add" class="btn btn-default btn-sm light fw600 ml10 text-center">
        <span class="fa fa-plus pr5"></span> Add Organisation</a>
    <div class="clearfix"><br/></div>
    <div class="panel panel-tile text-center">
        <div class="panel-body bg-info">
            <h1 class="fs35 mbn"><?php echo $list?count($list):0; ?></h1>
            <h6 class="text-white">ORG(s)</h6>
        </div>
        <div class="panel-footer br-n p12">
    <span class="fs11">
      <i class="fa fa-arrow-up text-info pr5"></i>
      <b></b>
    </span>
        </div>
    </div>
</div>

