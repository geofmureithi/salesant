<!-- Contact Form Popup -->
<div class="clearfix"></div>
<div id="product-form">
    <div class="panel">
        <div class="panel-heading">
            <span class="panel-title">
                <i class="fa fa-building"></i><?php echo Customlang::process($page['title']); ?>
            </span>
        </div>
        <!-- end .panel-heading section -->
        <div id="contact-results"></div>
        <form method="post" action="<?php echo $form['action']; ?>">
            <div class="panel-body p25">

                <div class="section">
                    <div class="form-group col-md-12">
                        <label for="title" class="control-label"><?php echo Customlang::process('Product Name'); ?> *</label>
                        <div class="bs-component">
                            <input type="text" name="product[title]" id="title" placeholder="The Product's Name" class="form-control" required value="<?php echo isset($POST['product']['title'])?$POST['product']['title']:''; ?>" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="product_unit" class="control-label">SKU</label>
                        <div class="bs-component">
                            <input type="text" name="product[sku]" id="product_sku" placeholder="Store Keeping Unit (Optional)" class="form-control" value="<?php echo isset($POST['product']['sku'])?$POST['product']['sku']:''; ?>" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="product_type" class="control-label"><?php echo Customlang::process('Product Type'); ?>*</label>
                        <div class="bs-component">
                            <select required id="product_type" placeholder="Choose <?php echo Customlang::process('Product Type'); ?>" name="product[type]" class="select2-single form-control">
                                <option>Choose <?php echo Customlang::process('Product Type'); ?></option>
                            <?php foreach($types as $key => $val) {?><option value="<?php echo $key; ?>"<?php echo (isset($POST['product']['type']) && $POST['product']['type']==$key)?' selected="selected"':''; ?>><?php echo $val; ?></option><?php } ?></select>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="price" class="control-label">Price*</label>
                        <div class="bs-component">
                            <input type="text" name="product[price]" placeholder="Price without commas" id="price" class="form-control" required value="<?php echo isset($POST['product']['price'])?$POST['product']['price']:''; ?>" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="product_unit" class="control-label">Unit*</label>
                        <div class="bs-component">
                            <input type="text" name="product[unit]" placeholder="Unit of measurement eg Kg or per unit" id="product_unit" class="form-control" value="<?php echo isset($POST['product']['unit'])?$POST['product']['unit']:''; ?>" />
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="product_description" class="control-label">Description</label>
                        <div class="bs-component">
                            <textarea name="product[description]" placeholder="Please enter a description for the item" id="product_description" class="form-control"><?php echo $POST['product']['description']; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Status</label>
                        <div class="bs-component">
                            <?php foreach (($productStatuses?:array()) as $status): ?>
                                <span class="label label-<?php echo $status->badge; ?>"><input type="radio" name="product[status]" value="<?php echo $status->id; ?>" title="<?php echo $status->description; ?>" <?php echo (isset($POST['product']['status']) && $POST['product']['status']==$status->id)?'checked="checked"':''; ?> /> <?php echo $status->title; ?></span>
                            <?php endforeach; ?>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label">Custom Fields</label>
                        <div class="form-group customfields">

                        </div>

                    </div>
                    <div class="form-group col-md-12">
                        <label for="productphotos" class="control-label">Photos <small class="label label-alert">Upload <?php echo Customlang::process('Product'); ?> Photos</small></label>
                        <div id="productphotos" class="dropzone">

                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <hr/>
                        <label for="productphotos" class="control-label">Settings <small class="label label-warning">Custom <?php echo Customlang::process('Product'); ?> Settings</small></label>
                    <div class="checkbox-custom fill checkbox-primary mb5">
                        <input type="checkbox" name="product[isService]" id="service" <?php echo (isset($POST['product']['isService']) && $POST['product']['isService']=='on')?'checked="checked"':''; ?> />
                        <label for="service">Is a Service?</label>
                    </div>
                    <div class="checkbox-custom fill checkbox-primary mb5">
                        <input type="checkbox" name="product[isResale]" id="resale" <?php echo (isset($POST['product']['isResale']) && $POST['product']['isResale']=='on')?'checked="checked"':''; ?> />
                        <label for="resale">Can be Resold?</label>
                    </div>
                        </div>

                </div>


                <!-- end section -->

            </div>
            <!-- end .form-body section -->

            <div class="panel-footer">
                <?php if ($PARAMS['id']): ?>
                    
                        <input id="product_id" type="hidden" name="product[id]" value="<?php echo $PARAMS['id']; ?>" />
                <?php endif; ?>
                <button type="submit" class="btn btn-sm btn-primary">Submit</button>
            </div>
            <!-- end .form-footer section -->
        </form>
    </div>
    <!-- end: .panel -->
</div>
<!-- end: .contact-form -->
<script>
    $(function(){
        function loadFields(){
           var product = $('#product_id').val();
            if(product > 0){
                $.get('/producttypes/customfields/'+$('select#product_type').val()+'?product='+product,function(res){
                    var html = $.parseHTML(res.fields.template);
                    $(".customfields").html(html);
                });
            } else{
                $.get('/producttypes/customfields/'+$('select#product_type').val(),function(res){
                    var html = $.parseHTML(res.fields.template);
                    $(".customfields").html(html);
                });
            }
        }
        $('select#product_type').on('change',function(e){
            loadFields();
        });
        loadFields();
    });
</script>


