'use strict';
//  Author: AdminDesigns.com
// 
//  This file is reserved for changes made by the use. 
//  Always seperate your work from the theme. It makes
//  modifications, and future theme updates much easier 
// Place custom scripts here

var Custom = function (options) {

    // Custom Functions
    var runCustomElements = function (options) {


        // Init jQuery Multi-Select for navbar user dropdowns
            $('.multiselect').multiselect({
                buttonClass: 'btn btn-default btn-sm'

            });
            //$('select').selectize();
        // Init DateTimepicker - fields
            $('.datetime').datetimepicker({
                minDate: moment(),
                format:"YYYY-MM-DD HH:mm:ss"
            });
            $('.date-back').datetimepicker({
                maxDate: moment(),
                format:"YYYY-MM-DD"
            });
            $('.date-simple').datetimepicker({
                format:"YYYY-MM-DD"
            });

        // Init plugins for ".compose-widget"
        // plugins: Summernote
        //
        if ($(".summernote-email").length) {
            $('.summernote-email').summernote({
                height: 197, //set editable area's height
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', ]],
                    ['para', ['ul', 'ol', 'paragraph']],
                ]
            });
        }


        // Slider with Counter
        $("#confidence-count").slider({
            range: "min",
            min: 0,
            max: 100,
            value: 50,
            slide: function (event, ui) {
                $("#percentage").html(ui.value + "%");
                $("#opportunity_confidence").val(ui.value);
            }
        });
        $("#confidence-count2").slider({
            range: "min",
            min: 0,
            max: 100,
            value: 50,
            slide: function (event, ui) {
                $("#percentage2").html(ui.value + "%");
                $("#opportunity_confidence2").val(ui.value);
            }
        });



        // Init plugins for ".task-widget"
        // plugins: Custom Functions + jQuery Sortable
        //
        var taskWidget = $('div.task-widget');
        var taskItems = taskWidget.find('li.task-item');
        var currentItems = taskWidget.find('ul.task-current');
        var completedItems = taskWidget.find('ul.task-completed');


        // Init jQuery Sortable on Task Widget
        taskWidget.sortable({
            items: taskItems, // only init sortable on list items (not labels)
            axis: 'y',
            connectWith: ".task-list",
            update: function (event, ui) {
                var Item = ui.item;
                var ParentList = Item.parent();
                var Itemid = Item.attr('data-id');

                // If item is already checked move it to "current items list"
                if (ParentList.hasClass('task-current') && Itemid) {
                    Item.removeClass('item-checked').find('input[type="checkbox"]').prop('checked', false);
                    Item.find('.task-desc span').removeClass('hidden');
                    $.post("/tasks/edit/" + Itemid, {task: {status: 0}});
                }
                if (ParentList.hasClass('task-completed') && Itemid) {
                    Item.addClass('item-checked').find('input[type="checkbox"]').prop('checked', true);
                    Item.find('.task-desc span').addClass('hidden');
                    $.post("/tasks/edit/" + Itemid, {task: {status: 1}});
                }
                updateAllRelativeDates();

            }
        });

        // Custom Functions to handle/assign list filter behavior
        taskItems.on('click', '.task-handle', function (e) {
            e.preventDefault();
            var This = $(this).parent();

            if ($(e.target).hasClass('fa-remove')) {
                This.remove();
                return;
            }

            var Itemid = This.attr('data-id');


            // If item is already checked move it to "current items list"
            if (This.hasClass('item-checked')) {
                This.removeClass('item-checked').appendTo(currentItems).find('input[type="checkbox"]').prop('checked', false);
                This.find('.task-desc span').removeClass('hidden');
                $.post("/tasks/edit/" + Itemid, {task: {status: 0}});
            }
            // Otherwise move it to the "completed items list"
            else {
                This.addClass('item-checked').appendTo(completedItems).find('input[type="checkbox"]').prop('checked', true);
                This.find('.task-desc span').addClass('hidden');
                $.post( "/tasks/edit/" + Itemid, {task: {status: 1}});
            }

        });






        $(".email-compose").on('click', '.add-cc', function () {
            $("#cc-input").toggle();
            $(this).text(function (i, text) {
                return text === "Add CC" ? "Remove CC" : "Add CC";
            });
        });
        $(".email-compose").on('click', '.add-bcc', function () {
            $("#bcc-input").toggle();
            $(this).text(function (i, text) {
                return text === "Add BCC" ? "Remove BCC" : "Add BCC";
            });
        });


        // Dropzone
        //var myDropzone = new Dropzone("div#emailattachments", { url: "/uploads/attachments"});
        $("div#emailattachments").dropzone({
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 10, // Max file size - in MB
            addRemoveLinks: true, // Toggle remove links
            dictDefaultMessage: '<i class="fa fa-cloud-upload"></i> \
                <span class="main-text"><b>Drop Files</b> to attach</span> <br /> \
                <span class="sub-text">(or click)</span> \
               ',
            dictResponseError: 'Server not Configured',
            url: '/emails/upload',
            init: function () {
                this.on('success', function (file, res) {
                    if (res.success)
                        $('<input>').attr({
                            type: 'hidden',
                            data: file.name,
                            name: 'email[attachments][]',
                            value: res.fileName
                        }).appendTo('form#leademail-form');

                    return true;
                });

                this.on('removedfile', function (file) {
                    $('input[data="' + file.name + '"]').remove();

                });
            }
        });
        if($("div#importnewsletter").length)
        $("div#importnewsletter").dropzone({
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 10, // Max file size - in MB
            acceptedFiles: ".csv",
            maxFiles: 1,
            addRemoveLinks: true, // Toggle remove links
            dictDefaultMessage: '<i class="fa fa-cloud-upload"></i> \
                <span class="main-text"><b>Drop Csv</b> to attach</span> <br /> \
                <span class="sub-text">(or click)</span> \
               ',
            dictResponseError: 'Server not Configured',
            url: '/newsletters/upload',
            init: function () {
                this.on('success', function (file, res) {
                    if (res.success)
                        $('<input>').attr({
                            type: 'hidden',
                            data: file.name,
                            name: 'newsletter[attachment]',
                            value: res.fileName
                        }).appendTo('div#importnewsletter');

                    return true;
                });

                this.on('removedfile', function (file) {
                    $('input[data="' + file.name + '"]').remove();

                });
            }
        });
        if($("div#documents").length)
        $("div#documents").dropzone({
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 10, // Max file size - in MB
            addRemoveLinks: true, // Toggle remove links
            dictDefaultMessage: '<i class="fa fa-cloud-upload"></i> \
                <span class="main-text"><b>Drop File</b> to Upload</span> <br /> \
                <span class="sub-text">(or click)</span> \
               ',
            dictResponseError: 'Server not Configured',
            url: 'documents/upload',
            init: function () {
                this.on('success', function (file, res) {
                    if (res.success)
                        $('<input>').attr({
                            type: 'hidden',
                            data: file.name,
                            name: 'document[files][]',
                            value: res.fileName
                        }).appendTo('div#documents');

                    return true;
                });

                this.on('removedfile', function (file) {
                    $('input[data="' + file.name + '"]').remove();


                });
            }
        });
        if($("div#quotation").length){
            $("div#quotation").dropzone({
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 10, // Max file size - in MB
                acceptedFiles: ".htm,.html,.doc,.docx",
                maxFiles: 1,
                addRemoveLinks: true, // Toggle remove links
                dictDefaultMessage: '<i class="fa fa-cloud-upload"></i> \
                <span class="main-text"><b>Drop Quotation Template</b> to Upload</span> <br /> \
                <span class="sub-text">(or click)</span> \
               ',
                dictResponseError: 'Server not Configured',
                url: 'organisations/upload',
                init: function () {
                    this.on('success', function (file, res) {
                        if (res.success)
                            $('<input>').attr({
                                type: 'hidden',
                                data: file.name,
                                name: 'organisation[quoteTemplate]',
                                value: res.fileName
                            }).appendTo('div#quotation');

                        return true;
                    });

                    this.on('removedfile', function (file) {
                        this.options.maxFiles = this.options.maxFiles + 1;
                        $('input[data="' + file.name + '"]').remove();

                    });
                }
            });
        }

        if($("div#invoice").length)
        $("div#invoice").dropzone({
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 10, // Max file size - in MB
            maxFiles: 1,
            addRemoveLinks: true, // Toggle remove links
            acceptedFiles: ".htm,.html,.doc,.docx",
            dictDefaultMessage: '<i class="fa fa-cloud-upload"></i> \
                <span class="main-text"><b>Drop Invoice Template</b> to Upload</span> <br /> \
                <span class="sub-text">(or click)</span> \
               ',
            dictResponseError: 'Server not Configured',
            url: 'organisations/upload',
            init: function () {
                this.on('success', function (file, res) {
                    if (res.success)
                        $('<input>').attr({
                            type: 'hidden',
                            data: file.name,
                            name: 'organisation[invoiceTemplate]',
                            value: res.fileName
                        }).appendTo('div#invoice');

                    return true;
                });

                this.on('removedfile', function (file) {
                    $('input[data="' + file.name + '"]').remove();


                });
            }
        });
        if($("div#receipt").length)
        $("div#receipt").dropzone({
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 10, // Max file size - in MB
            acceptedFiles: ".htm,.html,.doc,.docx",
            maxFiles: 1,
            addRemoveLinks: true, // Toggle remove links
            dictDefaultMessage: '<i class="fa fa-cloud-upload"></i> \
                <span class="main-text"><b>Drop Invoice Template</b> to Upload</span> <br /> \
                <span class="sub-text">(or click)</span> \
               ',
            dictResponseError: 'Server not Configured',
            url: 'organisations/upload',
            init: function () {
                this.on('success', function (file, res) {
                    if (res.success)
                        $('<input>').attr({
                            type: 'hidden',
                            data: file.name,
                            name: 'organisation[receiptTemplate]',
                            value: res.fileName
                        }).appendTo('div#receipt');

                    return true;
                });

                this.on('removedfile', function (file) {
                    $('input[data="' + file.name + '"]').remove();

                });
            }
        });
        if($("div#organisationLogo").length)
        $("div#organisationLogo").dropzone({
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 10, // Max file size - in MB
            acceptedFiles: "image/*",
            maxFiles: 1,
            addRemoveLinks: true, // Toggle remove links
            dictDefaultMessage: '<i class="fa fa-cloud-upload"></i> \
                <span class="main-text"><b>Drop the Organisation Logo</b> to Upload</span> <br /> \
                <span class="sub-text">(or click)</span> \
               ',
            dictResponseError: 'Server not Configured',
            url: 'organisations/upload',
            init: function () {
                this.on('success', function (file, res) {
                    if (res.success)
                        $('<input>').attr({
                            type: 'hidden',
                            data: file.name,
                            name: 'organisation[logo]',
                            value: res.fileName
                        }).appendTo('div#organisationLogo');

                    return true;
                });

                this.on('removedfile', function (file) {
                    $('input[data="' + file.name + '"]').remove();

                });
            }
        });
        if($("div#productphotos").length)
        $("div#productphotos").dropzone({
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 10, // Max file size - in MB
            addRemoveLinks: true, // Toggle remove links
            dictDefaultMessage: '<i class="fa fa-cloud-upload"></i> \
                <span class="main-text"><b>Drop Images</b> to Upload</span> <br /> \
                <span class="sub-text">(or click)</span> \
               ',
            dictResponseError: 'Server not Configured',
            url: 'products/upload',
            init: function () {
                this.on('success', function (file, res) {
                    if (res.success)
                        $('<input>').attr({
                            type: 'hidden',
                            data: file.name,
                            name: 'product[photos][]',
                            value: res.fileName
                        }).appendTo('form#product-form');

                    return true;
                });

                this.on('removedfile', function (file) {
                    $('input[data="' + file.name + '"]').remove();

                });
            }
        });
        Dropzone.options.bulkupload = {
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 10, // Max file size - in MB
            addRemoveLinks: true, // Toggle remove links
            dictDefaultMessage: '<i class="fa fa-cloud-upload"></i> \
                <span class="main-text"><b>Drop Files</b> to Import</span> <br /> \
                <span class="sub-text">(or click)</span> \
               ',
            dictResponseError: 'Server not Configured',
            url:'uploads/imports',
            uploadMultiple: false,
            maxFiles: 1,
            acceptedFiles: '.xlsx,.xls,.csv',
            init: function () {
                this.on('success', function (file, res) {
                    if (res.success)
                        $('<input>').attr({
                            type: 'hidden',
                            data: file.name,
                            name: 'import',
                            value: res.name
                        }).appendTo('form#imports-form');

                    return true;
                });

                this.on('removedfile', function (file) {
                    $('input[data="' + file.name + '"]').remove();

                });
            }
        };
        $('#openBulkModal').click(function(e){
            e.preventDefault();
            $("#bulkModal").modal('show');
        });

        $("#toggleattachments").on('click', function (e) {
            e.preventDefault();
            $("#foremailattachments").toggle('slow');
        });

        var $emailInputs = $("input[data-role='emails-input']");
        if($emailInputs){
            var contacts = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('firstname'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                prefetch: {
                    url: '/contacts/fetch/?' + Date.now(),
                    filter: function(list) {
                        return $.map(list, function(contact) {
                            return { name: contact['firstname'] + contact['lastname'], email: contact['email'] }; });
                    }
                }
            });
            contacts.initialize();
            $emailInputs.tagsinput({
                tagClass: 'label label-primary',
                typeaheadjs: {
                    name: 'contacts',
                    displayKey: 'name',
                    valueKey: 'email',
                    source: contacts.ttAdapter()
                }
            });
            $emailInputs.on('beforeItemAdd', function (e) {
                if (!validateEmail(e.item))
                    e.cancel = true;
            });
        }

        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }



        $(".alert .alert-success").fadeTo(2000, 500).slideUp(500, function(){
            $(this).alert('close');
        });



    };



    return {
        init: function (options) {
            $('form').submit(function(){
                $(this).find('button[type=submit]').prop('disabled', true);
                $(this).find('input[type=submit]').prop('disabled', true);
            });
            $( 'form' ).garlic();

            $('a.delete').click(function (e) {
                var record = $(this).attr('data-type');
                var delete_url = $(this).attr('href');
                swal({
                        title: "Are you sure?",
                        text: "Your will not be able to recover this " + record +'? This cannot be undone.',
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function(){
                        $.get(delete_url, function(){
                            $.pjax.reload('#content');
                            swal("Deleted!", "Your "  + record + " has been deleted.", "success");
                        }).fail(function(data) {
                            swal.close();
                            $.pjax.reload('#content');

                        })

                    });
                e.preventDefault();
            })
            $('.summernote').summernote({
                height: 500, //set editable area's height
            });
            $('.smallnote').summernote({
                height: 100, //set editable area's height
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', ]]
                ]
            });
            $('form.ajaxable').submit(function (event) {
                var form = $(this);

                $.ajax({
                    type: 'post',
                    url: form.attr('action'),
                    data: form.serialize()
                }).done(function(data) {
                    $.pjax.reload('#content');
                    $.magnificPopup.close(); // Close popup that is currently opened (shorthand)
                }).fail(function(data) {
                    // Optionally alert the user of an error here...
                });
                event.preventDefault();
                return false;
            });
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                updateScrollers();
            });
            $('.add-payment-plan').click(function(e){

                var html = '<div class="row single-payable"><div class="form-group col-sm-4"><label class="control-label">Amount</label><div class="bs-component input-group"><span class="input-group-addon cursor remover"><i class="fa fa-close"></i></span><input name="paymentplan[amount][]" class="form-control amount" required /></div></div>' +
                    '<div class="form-group col-sm-4"><label for="dop" class="control-label">Date of Payment</label><div class="bs-component"><div class="input-group simpledate" id="dop"><span class="input-group-addon cursor"><i class="fa fa-calendar"></i></span><input type="text" name="paymentplan[datedue][]" class="form-control" style="background-color: #fff !important; cursor: text !important;"/></div></div></div><div class="form-group col-sm-4"><label class="control-label">Description</label><div class="bs-component"><input name="paymentplan[description][]" class="form-control"/></div></div></div>';
                $('div.payment-removable').append(html);
                updateScrollers();
                $('input.amount').on('keyup',function () {
                    calculateTotal();

                });
                $('input.amount').keyup(function(event) {

                    // skip for arrow keys
                    if(event.which >= 37 && event.which <= 40) return;

                    // format number
                    $(this).val(function(index, value) {
                        return value
                            .replace(/\D/g, "")
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                            ;
                    });
                });
                $('span.remover').on('click',function () {
                    $(this).closest('div.single-payable').remove();
                });

                e.preventDefault();
            })
            $('select#status').on('change', function(e){
                $.post('/leads/status/'+$(this).attr('data-id'),{status: $(this).val()}).done(function(res){
                    $.pjax.reload('#content');
                })
            })


            runCustomElements();
            updateAllRelativeDates();
            updateScrollers();
        }

    }


}();
var updateAllRelativeDates = function() {
    $('time').each(function (i, e) {
        if ($(e).attr("class") == 'cw-relative-date') {

            // Initialise momentjs
            var now = moment();
            moment.lang('en', {
                calendar : {
                    lastDay : '[Yesterday at] LT',
                    sameDay : '[Today at] LT',
                    nextDay : '[Tomorrow at] LT',
                    lastWeek : '[Last] dddd [at] LT',
                    nextWeek : 'dddd [at] LT',
                    sameElse : 'D MMM YYYY'
                }
            });

            // Grab the datetime for the element and compare to now
            var time = moment($(e).attr('datetime'));
            var diff = now.diff(time, 'days');

            // If less than one day ago/away use relative, else use calendar display
            if (diff <= 1 && diff >= -1) {
                $(e).html('<span>' + time.from(now) + '</span>');
            } else {
                $(e).html('<span>' + time.calendar() + '</span>');
            }
        }
    });
};
var updateScrollers = function(){
    $('.date').datetimepicker({
        format:"YYYY-MM-DD HH:mm:ss"
    });
    $('.simpledate').datetimepicker({
        format:"YYYY-MM-DD"
    });
    // If a panel element has the ".panel-scroller" class we init
    // custom fixed height content scroller. An optional delay data attr
    // may be set. This is useful when you expect the panels height to
    // change due to a plugin or other dynamic modification.
    var panelScroller = $('.panel-scroller');
    panelScroller.scroller();


    // Init smoothscroll on elements with set data attr
    // data value determines smoothscroll offset
    var SmoothScroll = $('[data-smoothscroll]');
    if (SmoothScroll.length) {
        SmoothScroll.each(function(i,e) {
            var This = $(e);
            var Offset = This.data('smoothscroll');
            var Links = This.find('a');

            // Init Smoothscroll with data stored offset
            Links.smoothScroll({
                offset: Offset
            });

        });
    }
};

