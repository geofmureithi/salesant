<?php

// Kickstart the framework
$f3=require('lib/base.php');
$f3->set('DEBUG',0);

if ((float)PCRE_VERSION<7.9)
    trigger_error('PCRE version is out of date');
$f3->config('config.ini');
//Load Db, Loaders and Routes
require('config/database.php');
require('config/loaders.php');
require('config/routes.php');


$f3->run();