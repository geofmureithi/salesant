<?php
define("ROOT", __DIR__);
$f3 = Base::instance();
if(!Subdomain::isRegistered())
    $f3->error(500, "Subdomain does not exist");

if(Subdomain::title() =='my')
    $f3->set('AccountDB', new \DB\SQL('mysql:host=localhost;port=3306;dbname='. $f3->get('mysql.db'), $f3->get('mysql.username'), $f3->get('mysql.password')));

if(Subdomain::isSub() && Subdomain::title() !='my'){
    $f3->set('AppDB1', new \DB\SQL('mysql:host=localhost;port=3306;dbname=sub_'. Subdomain::title(), $f3->get('mysql.username'), $f3->get('mysql.password')));
}

