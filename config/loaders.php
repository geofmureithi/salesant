<?php
/**
 * Created by PhpStorm.
 * User: geoff
 * Date: 4/8/16
 * Time: 11:39 AM
 */
define("ROOT", __DIR__);
\Template\FooForms::init();
\Template::instance()->filter('currency','Currency::format');
\Template::instance()->filter('customlang','Customlang::process');