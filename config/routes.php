<?php

if(Subdomain::isSub() && Subdomain::title() != 'my'){
    $f3->route(array('GET|POST /@controller'),'\Application\Controllers\@controller->index');
    $f3->route('GET|POST /@controller/@action/@id','\Application\Controllers\@controller->@action');
    $f3->route('GET|POST /@controller/@action','\Application\Controllers\@controller->@action');
    $f3->route('GET|POST /@controller','\Application\Controllers\@controller->'.$f3->get('index_action'));
    $f3->route('GET|POST /','\Application\Controllers\\' . $f3->get('index_controller').'->'.$f3->get('index_action'));
    $f3->route('GET|POST /@controller/@action/@id','\Application\Controllers\@controller->@action');
    $f3->route('GET|POST /reporting/@controller','\Application\Controllers\@controller->reporting');
    $f3->route('GET|POST /@mothercontroller/@motherid/@controller/@action','\Application\Controllers\@controller->@action');
    $f3->route('GET|POST /@mothercontroller/@motherid/@controller/@action/@id','\Application\Controllers\@controller->@action');
    $f3->route('GET|POST /auth/@action','\Application\Controllers\Auth->@action');
    $f3->route('POST /misc/file-details',function($f3){
        $myFS = new \FAL\LocalFS('files/');
        $details = new stdClass();
        $filename = $f3->get('POST.filename');
        if($myFS->exists($filename)){
            $details->name= basename($filename);
            $details->path = '/files/'.$filename;
            $details->size = $myFS->size($filename);
        }
        echo json_encode(array($details));
    });

    $f3->route('GET /models/setup',
        function($f3){
            $models = array('Activity','Call','Contact','CustomField', 'CustomValue', 'Email','Invoice', 'Lead','Leadsource','Leadstatus','Note', 'Opportunity','OpportunityStatus','Organisation','PaymentPlan','Product','ProductStatus','ProductType','Quotation','Receipt','Setting','Task','User','UserType','Document','Language', 'Sale', 'Payment', 'Transcript','NewsletterGroup', 'NewsletterTemplate','Notification','Permission');
            foreach($models as $model){
                $model = '\Application\Models\\' . $model;
                $x = (new $model);
                $x::install();
            }
        });
}

if(Subdomain::isSub() && Subdomain::title() == 'my'){
    $f3->route('GET|POST /auth/@action','\Account\Controllers\Auth->@action');
    $f3->route('GET|POST /','\Account\Controllers\\' . $f3->get('index_controller').'->'.$f3->get('index_action'));
    $f3->route(array('GET|POST /@controller'),'\Account\Controllers\@controller->index');
    $f3->route('GET /models/setup',
        function($f3){
            $models = array('Support', 'Bill', 'User', 'Usertype', 'Account');
            foreach($models as $model){
                $model = '\Account\Models\\' . $model;
                $x = (new $model);
                $x::install();
            }
        });
}
